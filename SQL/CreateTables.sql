CREATE TABLE `existingtravelers` (
  `travelerSN` int NOT NULL,
  `selectedPromptIndex` int DEFAULT NULL,
  `currentGroupIndex` int DEFAULT NULL,
  `scrollPosition` int DEFAULT NULL,
  `percentProgress` int DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `isLocked` tinyint(1) DEFAULT NULL,
  `isComplete` tinyint(1) DEFAULT NULL,
  `TravelerNotes` text,
  `lastDateSynced` datetime DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `isBusy` tinyint(1) DEFAULT '0',
  `busyDate` datetime DEFAULT NULL,
  `busyMachine` varchar(255) DEFAULT NULL
);
CREATE TABLE `activeusers` (
  `TravelerSN` int DEFAULT NULL,
  `userID` int DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
);
CREATE TABLE `assemblydetails` (
  `TravelerSN` int DEFAULT NULL,
  `sn` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `PN` varchar(255) DEFAULT NULL,
  `ADindex` int DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL
);
CREATE TABLE `completedtravelers` (
  `travelersn` int DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `html` mediumtext
);
CREATE TABLE `groupdata` (
  `operatorInitials` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `groupNumber` int DEFAULT NULL,
  `dateOfCompletion` datetime DEFAULT NULL,
  `pass` tinyint(1) DEFAULT NULL,
  `TravelerSN` int DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL
);
CREATE TABLE `historylog` (
  `previousValue` mediumtext,
  `newValue` mediumtext,
  `promptIndex` int DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `TravellerSN` int DEFAULT NULL,
  `ID` int NOT NULL AUTO_INCREMENT,
  `actionPerformed` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
);
CREATE TABLE `historylogusers` (
  `ID` int DEFAULT NULL,
  `userID` int DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
);
CREATE TABLE `promptlogic` (
  `targetPrompt` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `comparedPrompt` varchar(255) DEFAULT NULL,
  `offset` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `promptindex` int DEFAULT NULL,
  `comparedvalue` varchar(255) DEFAULT NULL
);
CREATE TABLE `prompts` (
  `type` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `TravelerSN` int DEFAULT NULL,
  `IsSet` tinyint(1) DEFAULT NULL,
  `groupLocked` tinyint(1) DEFAULT NULL,
  `selected` tinyint(1) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  `promptIndex` int DEFAULT NULL,
  `promptGroup` int DEFAULT NULL,
  `promptName` varchar(255) DEFAULT NULL,
  `currentValue` text,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL
);
CREATE TABLE `templates` (
  `templateName` varchar(255) DEFAULT NULL,
  `customerName` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL
);
CREATE TABLE `users` (
  `passwordHash` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `authority` int DEFAULT NULL,
  `userID` int NOT NULL AUTO_INCREMENT,
  `dateCreated` datetime DEFAULT NULL,
  `inactive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`userID`)
);
CREATE TABLE `userauthority` (
  `userid` int DEFAULT NULL,
  `authority` int DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL
);