﻿using System;
using System.Linq;

namespace TravellerAuto.PromptClasses
{
    public class TextBoxPrompt : UserPrompt
    {
        #region Prompt Functions
        public TextBoxPrompt()
        {
            PromptType = "TXT";
        }
        /// <summary>
        /// sets the value of the prompt to the input value
        /// </summary>
        public override void SetValue(object value)
        {
            string val = (string)value;
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue, val, "Assigned Value", Index);

            LogHistory.Add(log);
            CurrentValue = val;
            Valid = CheckValidity();

            IsSet = true;
        }
        public override void ValueCorrection(string value)
        {
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue.ToString(), value, "Admin Redaction", Index);

            LogHistory.Add(log);
            CurrentValue = value;
            Valid = CheckValidity();
        }
        /// <summary>
        /// determines whether the prompt is valid or not based on input logic provided by the template.
        /// </summary>
        public override int CheckValidity()
        {
            bool returnValue = true;
            if (Traveler.TravelerObject.PromptConstraints.ContainsKey(Index)) // if prompt logic is provided
            {
                foreach (PromptLogic constraint in Traveler.TravelerObject.PromptConstraints[Index])  // loops through all logic for this prompt
                {
                    constraint.Valid = 1;
                    if (constraint.Length > 2 && Traveler.TravelerObject.PromptConstraints[Index].ToList().IndexOf(constraint) == 0 && !constraint.Operator_.Contains("char"))  // if the first constraint is an operator, automatically check for type
                    {
                        if (decimal.TryParse(constraint.ComparedValue, out decimal output) && constraint.Operator_ != "=" && constraint.Operator_ != "!=" && constraint.Operator_ != "contains")  // if the compared value is a number
                        {
                            returnValue = constraint.CheckValid(this, "number");
                        }
                        else if (constraint.Operator_ != "=" && constraint.Operator_ != "!=" && constraint.Operator_ != "contains") // if the compared value is a string
                        {
                            returnValue = constraint.CheckValid(this, "text");
                        }
                        if (!returnValue) { constraint.Valid = 0; return 0; }  // if any validity checks fail based on prompt logic, hault all validity checking and declare prompt invalid
                    }
                    if (!constraint.CheckValid(this)) { constraint.Valid = 0; return 0; }
                }
            }
            return 1;
        }
        #endregion
        #region HTML Code
        /// <summary>
        /// determines HTML & CSS based on the prompts states
        /// </summary>
        public override string GetHtmlCode()
        {
            // Color Codes:
            // White: #FFFFFF 
            // Light Green: #7FFF6F (Selected)
            // Dark Green: #19DE00 (Valid Value)
            // Red: #F20707 (Invalid Value)
            // Orange: #FF9F25 (Group Locked)
            // light red :ff5252 (invalid selcted)
            string htmlCode;
            if (GroupLocked)
            {
                if (IsSet && Valid == 0)
                {
                    htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: fit-content; font-weight:bold; color :red;background-color:rgb(0,0,0,0); border:none;\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // FINISHED AND INVALID
                }
                else if (IsSet)
                {
                    htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: fit-content; font-weight:bold; color :black;background-color:rgb(0,0,0,0); border:none\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // FINISHED
                }
                else
                {
                    htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 120px; background-color:grey\" readonly=\"readonly\"/>";  // LOCKED TO FINISH
                }
            }
            else
            {
                if (Selected)
                {
                    if (Valid != 0 || CurrentValue.Equals(string.Empty))
                    {
                        htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 120px; background-color:#7FFF6F\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // SELECTED PROMPT VALID
                    }
                    else
                    {
                        htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 120px; background-color:#ff5252\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // SELECTED PROMPT INVALID
                    }
                }
                else
                {
                    if (!CurrentValue.Equals(string.Empty))
                    {
                        if (Valid == 1)
                        {
                            htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 120px; background-color:#19DE00\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";   // IF VALID NOT SELECTED
                        }
                        else
                        {
                            htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 120px; background-color:#F20707\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";  // IF INVALID NOT SELECTED
                        }
                    }
                    else
                    {
                        htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 120px; background-color:#FFFFFF\" readonly=\"readonly\"/>";   // NOT GROUP LOCKED, AND UNSET
                    }
                }
            }

            return htmlCode;
        }
        #endregion
    }
}
