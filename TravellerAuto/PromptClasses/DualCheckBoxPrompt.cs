﻿using System;
using System.Xml.Serialization;

namespace TravellerAuto.PromptClasses
{
    public class DualCheckBoxPrompt : UserPrompt
    {
        #region Attributes
        [XmlIgnore]
        public string FirstBoxName { get; set; } = "Yes";
        [XmlIgnore]
        public string SecondBoxName { get; set; } = "No";
        #endregion
        #region Prompt Functions
        public DualCheckBoxPrompt()
        {
            PromptType = "YN";
        }
        /// <summary>
        /// Sets CurrentValue based on the input value, can be Unset, Yes or No
        /// </summary>
        public override void SetValue(object value)
        {
            if (CurrentValue == string.Empty)
            {
                CurrentValue = "Unset";
            }

            string prev = CurrentValue;    // setting previous value before updating to the new value

            string newValue = (string)value;
            if (newValue == "FalseFalse")
            {
                CurrentValue = "Unset";
            }
            else if (newValue == "TrueFalse")
            {
                CurrentValue = "Yes";
            }
            else
            {
                CurrentValue = "No";
            }

            Valid = CheckValidity();

            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, prev, CurrentValue, "Assigned Value", Index);
            LogHistory.Add(log);

            IsSet = true;
        }
        public override void ValueCorrection(string value)
        {
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue.ToString(), value, "Admin Redaction", Index);
            LogHistory.Add(log);

            CurrentValue = value;
            Valid = CheckValidity();
        }
        /// <summary>
        /// determines whether the current value of the checkbox is valid or not based on the prompt logic template provided.
        /// </summary>
        public override int CheckValidity()
        {
            if (Traveler.TravelerObject.PromptConstraints.ContainsKey(Index)) // if there has been logic provided by the template
            {
                foreach (PromptLogic constraint in Traveler.TravelerObject.PromptConstraints[Index])  // loops through all logic for this prompt provided by the template
                {
                    constraint.Valid = 1;
                    if (!constraint.CheckValid(this)) { constraint.Valid = 0; return 0; }
                }
            }
            if (CurrentValue.ToLower() == "yes")
            {
                return 1; // checkboxes default to 'yes' being valid and 'no' being invalid if no logic is provided
            }
            else
            {
                return 0;
            }
        }
        #endregion
        #region HTML Code
        /// <summary>
        /// Determines the HTML & CSS style of the prompt based on its current state
        /// </summary>
        public override string GetHtmlCode()
        {
            // Color Codes:
            // White: #FFFFFF 
            // Light Green: #7FFF6F (Selected)
            // Dark Green: #19DE00 (Valid Value)
            // Red: #F20707 (Invalid Value)
            // Light Red: #ff5252

            // determines what the state of the checkboxes should be 
            //
            string innerHtml = $"<label for=\"{Index}1\">{FirstBoxName}</label>";

            if (CurrentValue == string.Empty || CurrentValue == "Unset")
            {
                innerHtml += $"<input type=\"checkbox\" id=\"{Index}1\" readonly=\"readonly\"/><label for=\"{Index}2\" readonly=\"readonly\">/ {SecondBoxName}</label><input type=\"checkbox\" id=\"{Index}2\" readonly=\"readonly\"/>";   // BOTH UNCHECKED
            }
            else if (CurrentValue.Equals("Yes"))
            {
                innerHtml += $"<input type=\"checkbox\" id=\"{Index}1\" readonly=\"readonly\" checked=\"true\"/><label for=\"{Index}2\" readonly=\"readonly\">/ {SecondBoxName}</label><input type=\"checkbox\" id=\"{Index}2\" readonly=\"readonly\"/>";  // YES CHECKED
            }
            else
            {
                innerHtml += $"<input type=\"checkbox\" id=\"{Index}1\" readonly=\"readonly\"/><label for=\"{Index}2\" readonly=\"readonly\">/ {SecondBoxName}</label><input type=\"checkbox\" id=\"{Index}2\" readonly=\"readonly\" checked=\"true\"/>";  // NO CHECKED
            }

            //determines the style of the checkboxes and labels
            //
            if (GroupLocked)
            {
                if (IsSet && Valid == 0)
                {
                    return $"<span id =\"{Index}\" style=\"color:red; display:-webkit-inline-box; width:fit-content;align-items:center;\">{innerHtml}</span>";  //FINISHED AND INVALID
                }

                if (IsSet)
                {
                    return $"<span id =\"{Index}\" style=\" display:-webkit-inline-box; width:fit-content;align-items:center;\">{innerHtml}</span>";  //FINISHED
                }
                else
                {
                    return $"<span id =\"{Index}\" style=\"border : 2px solid grey; display:-webkit-inline-box; align-items:center;\">{innerHtml}</span>";  //TO FINISH, GROUP LOCKED
                }
            }
            else
            {
                if (Selected)
                {
                    if (Valid != 0 || CurrentValue.Equals(string.Empty))
                    {
                        return $"<span  id =\"{Index}\" style=\"border : 2px solid #7FFF6F; display:-webkit-inline-box; align-items:center;\">{innerHtml}</span>"; // CURRENT PROMPT, SELECTED
                    }
                    else
                    {
                        return $"<span  id =\"{Index}\" style=\"border : 2px solid #ff5252; display:-webkit-inline-box; align-items:center;\">{innerHtml}</span>"; // CURRENT PROMPT, SELECTED INVALID
                    }
                }
                else
                {
                    if (!CurrentValue.Equals(string.Empty) && !CurrentValue.Equals("Unset"))
                    {
                        if (Valid == 1)
                        {
                            return $"<span  id =\"{Index}\" style=\"border : 2px solid #19DE00; display:-webkit-inline-box; align-items:center;\">{innerHtml}</span>";    // FINISHED AND VALID
                        }
                        else
                        {
                            return $"<span  id =\"{Index}\" style=\"border : 2px solid #F20707; display:-webkit-inline-box; align-items:center;\">{innerHtml}</span>";  //FINISHED AND INVALID
                        }
                    }
                    else
                    {
                        return $"<span  id =\"{Index}\" style=\"border : 2px solid black; display:-webkit-inline-box; align-items:center;\">{innerHtml}</span>";    //TO FINISH, GROUP UNLOCKED
                    }
                }
            }
        }
        #endregion
    }
}
