﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;


namespace TravellerAuto.PromptClasses
{
    public class UserPrompt
    {
        #region Attributes
        [XmlAttribute("Index")]
        public int Index { get; set; }
        [XmlAttribute("Type")]
        public string PromptType { get; set; } = string.Empty;

        [XmlAttribute("Name")]
        public string PromptName { get; set; } = string.Empty;

        [XmlAttribute("Group")]
        public int PromptGroup { get; set; } = 0;

        [XmlIgnore]
        public static int CurrentIndex { get; set; } = 0;

        [XmlAttribute("IsSet")]
        public bool IsSet { get; set; } = false;

        [XmlAttribute("GroupLocked")]
        public bool GroupLocked { get; set; } = true;

        [XmlAttribute("Selected")]
        public bool Selected { get; set; } = false;

        [XmlElement("CurrentValue")]
        public string CurrentValue { get; set; } = string.Empty;

        [XmlAttribute("Valid")]
        public int Valid { get; set; } = -1;

        [XmlArrayItem("LogHistory")]
        public List<HistoryLog> LogHistory { get; set; } = new List<HistoryLog>();
        #endregion
        #region Constructors
        public UserPrompt() { }
        /// <summary>
        /// creates a prompt based on the input tuple provided by TextProcessor <type, name, groupID></type>
        /// </summary>
        public static UserPrompt CreatePrompt(string promptType, string promptName, int promptGroup)
        {
            UserPrompt prompt;
            switch (promptType)
            {
                case "TXT":
                    prompt = new TextBoxPrompt();
                    break;
                case "YN":
                    prompt = new DualCheckBoxPrompt();
                    break;
                case "DT":
                    prompt = new DateTimePrompt("DT");  // date & time
                    break;
                case "D":
                    prompt = new DateTimePrompt("D");   // date only
                    break;
                case "T":
                    prompt = new DateTimePrompt("T");   // time only
                    break;
                case "CAM":
                    prompt = new CamRequestPrompt();
                    break;
                default:
                    MessageBox.Show($"Template Error '{promptType}_{promptName}_{promptGroup}'");
                    prompt = new TextBoxPrompt();
                    break;
            }
            prompt.PromptName = promptName;
            prompt.PromptGroup = promptGroup;
            prompt.Index = CurrentIndex++;      // sets index to the last index incremented
            return prompt;
        }
        #endregion
        #region Virtual Functions
        public virtual string GetHtmlCode() { return string.Empty; }
        public virtual void SetValue(object value) { }
        public virtual void ValueCorrection(string value) { }
        public virtual int CheckValidity() { return -1; }
        #endregion
    }
}
