﻿using System;

namespace TravellerAuto.PromptClasses
{
    public class CamRequestPrompt : UserPrompt
    {
        #region Prompt Functions
        public CamRequestPrompt()
        {
            PromptType = "CAM";
        }
        /// <summary>
        /// sets the value of the prompt to the input value
        /// </summary>
        public override void SetValue(object value)
        {
            //copy set value
            string val = (string)value;

            //create history log with the event of assigning the value
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue, val, "Assigned Value", Index);
            LogHistory.Add(log);

            //set the value and check if valid
            CurrentValue = val;
            Valid = CheckValidity();

            //disabled means that the prompt has been set
            IsSet = true;
        }
        public override void ValueCorrection(string value)
        {
            //add history log for the event of an admin redaction
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue.ToString(), value, "Admin Redaction", Index);
            LogHistory.Add(log);

            // set the valid and check if valid
            CurrentValue = value;
            Valid = CheckValidity();
        }
        public override int CheckValidity()
        {
            // cam request are always valid
            if (CurrentValue != null || CurrentValue != string.Empty)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        #endregion
        #region HTML Code
        public override string GetHtmlCode()
        {
            // Color Codes:
            // White: #FFFFFF 
            // Light Green: #7FFF6F (Selected)
            // Dark Green: #19DE00 (Valid Value)
            // Red: #F20707 (Invalid Value)
            // Light Red: #ff5252 (Invalid Value Selcted)

            string htmlCode;

            if (GroupLocked)
            {
                if (IsSet)
                {
                    htmlCode = $"<img src=\"{CurrentValue}\" id=\"{Index}\" style=\"border:none\"/>";    // FINISHED
                }
                else
                {
                    htmlCode = $"<img src=\"{CurrentValue}\" id=\"{Index}\" style=\"border :3px solid gray\"/>";  // LOCKED TO FINISH
                }
            }
            else
            {
                if (Selected)
                {
                    if (Valid != 0 || CurrentValue.Equals(string.Empty))
                    {
                        htmlCode = $"<img src=\"{CurrentValue}\" id=\"{Index}\" style=\"border: 3px solid #7FFF6F\"/>";    // SELECTED PROMPT
                    }
                    else
                    {
                        htmlCode = $"<img src=\"{CurrentValue}\" id=\"{Index}\" style=\"border: 3px solid #ff5252\"/>";    // SELECTED PROMPT AND INVALID
                    }
                }
                else
                {
                    if (!CurrentValue.Equals(string.Empty))
                    {
                        htmlCode = $"<img src=\"{CurrentValue}\" id=\"{Index}\" style=\"border :3px solid #19DE00\"/>";  // IF VALID NOT SELECTED
                    }
                    else
                    {
                        htmlCode = $"<img src=\"{CurrentValue}\" id=\"{Index}\" style=\"border :3px solid #FFFFFF\"/>";   // NOT GROUP LOCKED, AND UNSET
                    }
                }
            }
            return htmlCode;
        }
        #endregion
    }
}
