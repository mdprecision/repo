﻿using System;
using System.Xml.Serialization;

namespace TravellerAuto.PromptClasses
{
    public class DateTimePrompt : UserPrompt
    {
        #region Attributes
        [XmlAttribute("DateTimeType")]
        public string DT_Type { get; set; } // Can be DT(Date & Time), D(Date), or T(Time).
        #endregion
        #region Prompt Functions
        public DateTimePrompt(string type)
        {
            PromptType = "DateTime";
            DT_Type = type;
        }
        public DateTimePrompt()
        {
            PromptType = "DateTime";
            DT_Type = "DT";
        }
        /// <summary>
        /// sets the value of the prompt to the input value
        /// </summary>
        public override void SetValue(object value)
        {
            string DateAsString = (string)value;

            if (DT_Type == "DT")
            {
                DateAsString = DateAsString.Substring(0, 23);   // 0-22 is the Date&time
            }
            else if (DT_Type == "D")
            {
                DateAsString = DateAsString.Substring(0, 11);   //0-11 is only the date
            }
            else if (DT_Type == "T")
            {
                DateAsString = DateAsString.Substring(12, 11);   //12-23 is only the time
            }

            // add history log
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue, DateAsString, "Assigned Value", Index);
            LogHistory.Add(log);

            //set the value and check if valid
            CurrentValue = DateAsString;
            Valid = CheckValidity();

            // disabled means the value has been set
            IsSet = true;
        }
        public override void ValueCorrection(string value)
        {
            //add history log for redaction
            HistoryLog log = new HistoryLog(DateTime.Now.Ticks, CurrentValue.ToString(), value, "Admin Redaction", Index);
            LogHistory.Add(log);


            //set valud and check validity
            CurrentValue = value;
            Valid = CheckValidity();
        }
        /// <summary>
        /// Determines the HTML & CSS style of the prompt based on its current state
        /// </summary>

        public override int CheckValidity()
        {
            if (Traveler.TravelerObject.PromptConstraints.ContainsKey(Index)) // if prompt logic is provided
            {
                foreach (PromptLogic constraint in Traveler.TravelerObject.PromptConstraints[Index])  // loops through all logic for this prompt
                {
                    constraint.Valid = 1;
                    //if any logic is invalid, stop testing logic and set invalid
                    if (!constraint.CheckValid(this)) { constraint.Valid = 0; return 0; }
                }
            }
            return 1;
        }
        #endregion
        #region HTML Code
        public override string GetHtmlCode()
        {
            // Color Codes:
            // White: #FFFFFF 
            // Light Green: #7FFF6F (Selected)
            // Dark Green: #19DE00 (Valid Value)
            // Red: #F20707 (Invalid Value)
            // LIGHT RED : ff5252

            string htmlCode;
            if (GroupLocked)
            {
                if (IsSet && Valid == 0)
                {
                    htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width:fit-content; font-weight:bold; color :red;background-color:rgb(0,0,0,0); border:none;\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // FINISHED AND INVALID
                }
                else if (IsSet)
                {
                    htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width:fit-content; font-weight:bold; color :black;background-color:rgb(0,0,0,0); border:none\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // FINISHED
                }
                else
                {
                    htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 150px; background-color:grey\" readonly=\"readonly\"/>";  // LOCKED TO FINISH
                }
            }
            else
            {

                if (Selected)
                {
                    if (Valid != 0 || CurrentValue.Equals(string.Empty))
                    {
                        htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 150px; background-color:#7FFF6F\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // SELECTED PROMPT
                    }
                    else
                    {
                        htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 150px; background-color:#ff5252\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";    // SELECTED PROMPT INVALID
                    }
                }
                else
                {
                    if (!CurrentValue.Equals(string.Empty))
                    {
                        if (Valid == 1)
                        {
                            htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 150px; background-color:#19DE00\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";   // IF VALID NOT SELECTED
                        }
                        else
                        {
                            htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 150px; background-color:#F20707\" value=\"{CurrentValue}\" readonly=\"readonly\"/>";  // IF INVALID NOT SELECTED
                        }
                    }
                    else
                    {
                        htmlCode = $"<input type=\"text\" id=\"{Index}\" style=\"width: 150px; background-color:#FFFFFF\" readonly=\"readonly\"/>";   // NOT GROUP LOCKED, AND UNSET
                    }
                }
            }

            return htmlCode;
        }
        #endregion
    }
}
