﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using TravellerAuto.PromptClasses;

namespace TravellerAuto
{
    /// <summary>
    /// Prompt logic can be of length 2,3 or 4 Where index is :
    /// 1-target prompt
    /// 2-operator
    /// 3-compared prompt OR compared static value
    /// 4-offset value (for certain operators)
    /// </summary>
    public class PromptLogic
    {
        #region Attributes
        public string TargetPrompt { get; set; } = string.Empty;
        public string Operator_ { get; set; } = string.Empty;
        public string ComparedPrompt { get; set; } = string.Empty;
        public string ComparedValue { get; set; } = string.Empty;
        public string Offset { get; set; } = string.Empty;
        public int Valid { get; set; } = -1;
        public int Length { get; set; } = -1;
        #endregion
        #region Contructors
        public PromptLogic() { }
        public PromptLogic(string target, string type)
        {
            string[] supportedOperators = { "number", "text" };
            if (!supportedOperators.Contains(type))
            {
                Exception exception = new Exception($"Unknown operator '{type}'");
                throw exception;
            }

            string promptType = Traveler.TravelerObject.ListOfUserPrompts
                        .First(e => e.PromptName.ToLower().Equals(target)).PromptType;

            if (!promptType.Equals("TXT"))
            {
                Exception exception = new Exception($"Invalid Logic for type '{promptType}'");
                throw exception;
            }

            TargetPrompt = target;
            Operator_ = type;
            Length = 2;
        }
        public PromptLogic(string target, string op, string compared)
        {
            string[] supportedOperators = { "<", ">", "contains", "=", "!=", "<=", ">=", "char=", "char>", "char<", "char<=", "char>=", "startswith" };
            string[] numericOperators = { "<", ">", "<=", ">=" };
            if (!supportedOperators.Contains(op))
            {
                Exception exception = new Exception($"Unknown operator '{op}'");
                throw exception;
            }

            string promptType = Traveler.TravelerObject.ListOfUserPrompts
                        .First(e => e.PromptName.ToLower().Equals(target)).PromptType;
            if (promptType.Equals("CAM"))
            {
                Exception exception = new Exception($"Invalid Logic for type '{promptType}'");
                throw exception;
            }

            if (promptType.Equals("DateTime") && !numericOperators.Contains(op) && !op.Equals("="))
            {
                Exception exception = new Exception($"Invalid Operator '{op}' for type '{promptType}'");
                throw exception;
            }

            if (promptType.Equals("DateTime") && !compared.Equals("today") && !compared.Contains("step"))
            {
                Exception exception = new Exception($"Invalid Compared Value '{ComparedValue}' for type '{promptType}'");
                throw exception;
            }

            if (promptType.Equals("YN") && !op.Equals("=") && !op.Equals("!="))
            {
                Exception exception = new Exception($"Invalid Operator '{op}' for type '{promptType}'");
                throw exception;
            }

            TargetPrompt = target;
            Operator_ = op;

            if (compared.Contains("step"))
            {
                ComparedPrompt = compared;
                ComparedValue = "Unset";
            }
            else
            {
                ComparedValue = compared;
                ComparedPrompt = "NA";
                if (promptType.Equals("TXT") && numericOperators.Contains(Operator_) && !decimal.TryParse(ComparedValue, out decimal result))
                {
                    Exception exception = new Exception($"Invalid Compared Value '{ComparedValue}' for type '{promptType}' Must be Numeric");
                    throw exception;
                }
            }
            if (op.Contains("char") && !int.TryParse(compared, out int result1))
            {
                Exception exception = new Exception($"Invalid Compared Value '{ComparedValue}' for type '{promptType}' Must be integer");
                throw exception;
            }

            if (promptType.Equals("YN") && !ComparedValue.Equals("yes") && !ComparedValue.Equals("no"))
            {
                Exception exception = new Exception($"Invalid Compared Value '{ComparedValue}' for type '{promptType}'");
                throw exception;
            }

            Length = 3;
        }
        public PromptLogic(string target, string op, string compared, string offsetValue)
        {
            string[] supportedOperators = { "delta", "delta-" };
            if (!supportedOperators.Contains(op.ToLower()))
            {
                Exception exception = new Exception($"Unknown operator '{op}'");
                throw exception;
            }

            string promptType = Traveler.TravelerObject.ListOfUserPrompts
                        .First(e => e.PromptName.ToLower().Equals(target)).PromptType;
            if ((offsetValue.Contains("h") || offsetValue.Contains("d") || offsetValue.Contains("min")) && !promptType.Equals("DateTime"))
            {
                Exception exception = new Exception($"Invalid Offset '{offsetValue}' for type '{promptType}'");
                throw exception;
            }

            if (!decimal.TryParse(offsetValue.Replace("h", string.Empty).Replace("d", string.Empty).Replace("min", string.Empty), out decimal result))
            {
                Exception exception = new Exception($"Offset Value '{offsetValue}' must be numeric");
                throw exception;
            }

            TargetPrompt = target;
            Operator_ = op;
            if (compared.Contains("step"))
            {
                ComparedPrompt = compared;
                ComparedValue = "Unset";
            }
            else
            {
                ComparedValue = compared;
                ComparedPrompt = "NA";
            }
            if (offsetValue.Contains("h"))
            {
                Offset = (int.Parse(offsetValue.Replace("h", string.Empty)) * 36000000000).ToString();   //convert Hours to ticks
            }
            else if (offsetValue.Contains("d"))
            {
                Offset = (int.Parse(offsetValue.Replace("d", string.Empty)) * 864000000000).ToString();   //convert Days to ticks
            }
            else if (offsetValue.Contains("min"))
            {
                Offset = (int.Parse(offsetValue.Replace("min", string.Empty)) * 600000000).ToString();   //convert Minutes to ticks
            }
            else
            {
                Offset = offsetValue;
            }

            Length = 4;
        }
        #endregion
        #region PromptLogic Functions
        /// <summary>
        /// gets the value from the list of user prompts that is referenced in the logic
        /// </summary>
        public static string GetComparedValue(PromptLogic logic)
        {
            string value = Traveler.TravelerObject.ListOfUserPrompts
                    .First(e => e.PromptName.ToLower().Equals(logic.ComparedPrompt)).CurrentValue;
            logic.ComparedValue = value;
            return value;
        }
        /// <summary>
        /// Checks validity for the logic command. All exceptions thrown are not caught, and should never be thrown. any exceptions indicate an error catching-
        /// -invalid logic before being called by this function
        /// </summary>
        public bool CheckValid(UserPrompt prompt, string op = null)
        {
            //check if the compared value is another prompt
            // we get the compared values from the compared prompts when we check the validity, not when we load all the prompt logic on startup

            //exception to be thrown if we encounter an error. no exceptions should be thrown, all logic was checked for errors by the code already in LoadLogic
            Exception exception = new Exception($"Should not be thrown, Logic Check Programming Error. All exceptions should be caught when loading logic");
            if (ComparedValue.Equals("Unset"))
            {
                if (prompt.PromptType.Equals("DateTime"))
                {
                    string dt = Traveler.TravelerObject.ListOfUserPrompts
                            .First(e => e.PromptName.ToLower().Equals(ComparedPrompt)).CurrentValue;
                    ComparedValue = DateTime.Parse(dt).Ticks.ToString();
                }
                else
                {
                    ComparedValue = Traveler.TravelerObject.ListOfUserPrompts
                            .First(e => e.PromptName.ToLower().Equals(ComparedPrompt)).CurrentValue;
                }
            }
            else if (ComparedValue.ToLower().Contains("{[{doc"))
            {
                ComparedValue = DateTime.ParseExact(Traveler.TravelerObject.GroupDataList[int.Parse(ComparedValue.Split(':')[1].Replace("}]}", string.Empty)) - 1].DateComplete, "dd/MMM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).Ticks.ToString();
            }
            else if (ComparedValue.ToLower().Contains("{[{sn"))
            {
                ComparedValue = Traveler.TravelerObject.TravelerSN;
            }
            else if (ComparedValue.Equals("today"))
            {
                ComparedValue = DateTime.Now.Ticks.ToString();
            }
            if (op == null)
            {
                op = Operator_;
            }
            else
            {
                if (op.Equals("number") && !op.Equals("=") && !op.Equals("!="))
                {
                    return int.TryParse(prompt.CurrentValue.Replace(".", string.Empty), out int number);
                }
                else if (op.Equals("text") && !op.Equals("=") && !op.Equals("!="))
                {
                    return Regex.IsMatch(prompt.CurrentValue, @"^[a-zA-Z]+$");
                }
                else
                {
                    throw exception;
                }
            }
            switch (Length)
            {
                case 2:
                    if (prompt.PromptType.Equals("DateTime"))
                    {
                        throw exception;
                    }
                    else if (prompt.PromptType.Equals("YN"))
                    {
                        throw exception;
                    }
                    else if (prompt.PromptType.Equals("TXT"))
                    {
                        if (op.Equals("number"))
                        {
                            return int.TryParse(prompt.CurrentValue.Replace(".", string.Empty), out int number);
                        }
                        else if (op.Equals("text"))
                        {
                            return Regex.IsMatch(prompt.CurrentValue, @"^[a-zA-Z]+$");
                        }
                        else
                        {
                            throw exception;
                        }
                    }
                    else
                    {
                        throw exception;
                    }

                case 3:
                    if (prompt.PromptType.Equals("YN"))
                    {
                        if (op.Equals("="))
                        {
                            return prompt.CurrentValue.ToLower() == ComparedValue.ToLower();
                        }
                        else if (op.Equals("!="))
                        {
                            return prompt.CurrentValue.ToLower() != ComparedValue.ToLower();
                        }
                        else
                        {
                            throw exception;
                        }
                    }
                    else if (prompt.PromptType.Equals("TXT"))
                    {
                        if (op.Equals("<"))
                        {
                            return decimal.Parse(prompt.CurrentValue) < decimal.Parse(ComparedValue);
                        }

                        if (op.Equals("contains"))
                        {
                            return prompt.CurrentValue.ToLower().Contains(ComparedValue);
                        }
                        else if (op.Equals(">"))
                        {
                            return decimal.Parse(prompt.CurrentValue) > decimal.Parse(ComparedValue);
                        }
                        else if (op.Equals("="))
                        {
                            return prompt.CurrentValue.ToLower() == ComparedValue;
                        }
                        else if (op.Equals("!="))
                        {
                            return prompt.CurrentValue.ToLower() != ComparedValue;
                        }
                        else if (op.Equals(">="))
                        {
                            return decimal.Parse(prompt.CurrentValue) >= decimal.Parse(ComparedValue);
                        }
                        else if (op.Equals("<="))
                        {
                            return decimal.Parse(prompt.CurrentValue) <= decimal.Parse(ComparedValue);
                        }
                        else if (op.Equals("char="))
                        {
                            return prompt.CurrentValue.Length == int.Parse(ComparedValue);
                        }
                        else if (op.Equals("char<="))
                        {
                            return prompt.CurrentValue.Length <= int.Parse(ComparedValue);
                        }
                        else if (op.Equals("char>="))
                        {
                            return prompt.CurrentValue.Length >= int.Parse(ComparedValue);
                        }
                        else if (op.Equals("char<"))
                        {
                            return prompt.CurrentValue.Length < int.Parse(ComparedValue);
                        }
                        else if (op.Equals("char>"))
                        {
                            return prompt.CurrentValue.Length > int.Parse(ComparedValue);
                        }
                        else if (op.Equals("startswith"))
                        {
                            return prompt.CurrentValue.ToLower().StartsWith(ComparedValue);
                        }
                        else
                        {
                            throw exception;
                        }
                    }
                    else if (prompt.PromptType.Equals("DateTime"))
                    {
                        DateTimePrompt dt = (DateTimePrompt)prompt;
                        DateTime comparedDateTime = new DateTime(long.Parse(ComparedValue));
                        DateTime currentDateTime;
                        if (dt.DT_Type.Equals("D"))
                        {
                            currentDateTime = DateTime.ParseExact(dt.CurrentValue, "dd/MMM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            comparedDateTime = comparedDateTime.Date;
                        }
                        else if (dt.DT_Type.Equals("T"))
                        {
                            // we compare with todays date, and the inputted time
                            currentDateTime = DateTime.ParseExact(DateTime.Now.Date.ToString("dd / MMM / yyyy") + " " + dt.CurrentValue, " hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            currentDateTime = DateTime.Parse(prompt.CurrentValue);
                        }

                        if (op.Equals(">"))
                        {
                            return currentDateTime > comparedDateTime;
                        }
                        else if (op.Equals("<"))
                        {
                            return currentDateTime < comparedDateTime;
                        }
                        else if (op.Equals("="))
                        {
                            return currentDateTime == comparedDateTime;
                        }
                        else if (op.Equals(">="))
                        {
                            return currentDateTime >= comparedDateTime;
                        }
                        else if (op.Equals("<="))
                        {
                            return currentDateTime <= comparedDateTime;
                        }
                        else
                        {
                            throw exception;
                        }
                    }
                    else
                    {
                        throw exception;
                    }

                case 4:
                    if (prompt.PromptType.Equals("YN"))
                    {
                        throw exception;
                    }
                    else if (prompt.PromptType.Equals("TXT"))
                    {
                        if (op.Equals("delta"))
                        {
                            return Math.Abs(decimal.Parse(ComparedValue) - decimal.Parse(prompt.CurrentValue)) <= decimal.Parse(Offset);
                        }
                        else if (op.Equals("delta-"))
                        {
                            return Math.Abs(decimal.Parse(ComparedValue) - decimal.Parse(prompt.CurrentValue)) >= decimal.Parse(Offset);
                        }
                        else
                        {
                            throw exception;
                        }
                    }
                    else if (prompt.PromptType.Equals("DateTime"))
                    {
                        DateTimePrompt dt = (DateTimePrompt)prompt;
                        DateTime comparedDateTime = new DateTime(long.Parse(ComparedValue));
                        DateTime currentDateTime;
                        if (dt.DT_Type.Equals("D"))
                        {
                            currentDateTime = DateTime.ParseExact(dt.CurrentValue, "dd/MMM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            comparedDateTime = comparedDateTime.Date;
                        }
                        else if (dt.DT_Type.Equals("T"))
                        {
                            currentDateTime = DateTime.ParseExact(DateTime.Now.ToString("dd/MMM/yyyy") + " " + dt.CurrentValue, "dd-MMM-yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            currentDateTime = DateTime.ParseExact(prompt.CurrentValue, "dd/MMM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                        }

                        long currentDateTicks = currentDateTime.Ticks;
                        long comparedDateTicks = comparedDateTime.Ticks;

                        long difference = currentDateTicks - comparedDateTicks;

                        if (op.Equals("delta"))
                        {
                            return Math.Abs(difference) <= long.Parse(Offset);
                        }

                        if (op.Equals("delta-"))
                        {
                            return Math.Abs(difference) >= long.Parse(Offset);
                        }
                        else
                        {
                            throw exception;
                        }
                    }
                    else
                    {
                        throw exception;
                    }

                default: throw exception;
            }
        }
        #endregion
    }
}
