﻿namespace TravellerAuto
{
    partial class CompletedTravelersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompletedTravelersForm));
            this.webView21 = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.CompletedTravelersList = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.webView21)).BeginInit();
            this.SuspendLayout();
            // 
            // webView21
            // 
            this.webView21.AllowExternalDrop = true;
            this.webView21.CreationProperties = null;
            this.webView21.DefaultBackgroundColor = System.Drawing.Color.White;
            this.webView21.Dock = System.Windows.Forms.DockStyle.Right;
            this.webView21.Location = new System.Drawing.Point(189, 0);
            this.webView21.Name = "webView21";
            this.webView21.Size = new System.Drawing.Size(809, 764);
            this.webView21.TabIndex = 0;
            this.webView21.TabStop = false;
            this.webView21.ZoomFactor = 1D;
            this.webView21.NavigationCompleted += new System.EventHandler<Microsoft.Web.WebView2.Core.CoreWebView2NavigationCompletedEventArgs>(this.WebView21_NavigationCompleted);
            // 
            // CompletedTravelersList
            // 
            this.CompletedTravelersList.Dock = System.Windows.Forms.DockStyle.Left;
            this.CompletedTravelersList.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CompletedTravelersList.FormattingEnabled = true;
            this.CompletedTravelersList.ItemHeight = 38;
            this.CompletedTravelersList.Location = new System.Drawing.Point(0, 0);
            this.CompletedTravelersList.Name = "CompletedTravelersList";
            this.CompletedTravelersList.Size = new System.Drawing.Size(183, 764);
            this.CompletedTravelersList.TabIndex = 1;
            this.CompletedTravelersList.SelectedIndexChanged += new System.EventHandler(this.CompletedTravelersList_SelectedIndexChanged);
            // 
            // CompletedTravelersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 764);
            this.Controls.Add(this.CompletedTravelersList);
            this.Controls.Add(this.webView21);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CompletedTravelersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Completed Travelers";
            this.Load += new System.EventHandler(this.CompletedTravelersForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.webView21)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Web.WebView2.WinForms.WebView2 webView21;
        private System.Windows.Forms.ListBox CompletedTravelersList;
    }
}