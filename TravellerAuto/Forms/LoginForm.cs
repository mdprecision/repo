﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class LoginForm : Form
    {
        #region Attributes
        private bool loginSuccess { get; set; } = false;
        #endregion
        #region FormEvents
        public LoginForm()
        {
            InitializeComponent();
            if (Traveler.LoggedUsers.Count == 0)
            {
                LoginStatusLbl.ForeColor = Color.Blue;
                LoginStatusLbl.Text = "Login Required";
            }
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            Login();
        }
        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // login is required to use the program
            if (Traveler.LoggedUsers.Count == 0 && !loginSuccess && !Traveler.QuitAfterLoginDialog)
            {
                DialogResult res = MessageBox.Show("You must login to continue, do you want to quit?", "Required Login", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    Traveler.QuitAfterLoginDialog = true;
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            if (Application.OpenForms["MainForm"] is MainForm mainform && !Traveler.QuitAfterLoginDialog)
            {
                mainform.RefreshUsers();
            }
        }
        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; Login(); }
        }
        private void UsernameBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; Login(); }
        }
        #endregion
        #region LoginFunctions
        private void Login()
        {
            int userExistsResult = User.UserExists(UsernameBox.Text);
            if (userExistsResult == 1)
            {
                int loginResult = User.Login(UsernameBox.Text, PasswordBox.Text);
                if (loginResult == 1)
                {
                    LoginStatusLbl.ForeColor = Color.Green;
                    LoginStatusLbl.Text = "Logged in successfully";
                    loginSuccess = true;
                    Close();
                }
                else if (loginResult == -1) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "User already logged in"; UsernameBox.Focus(); }
                else if (loginResult == -2) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "Incorrect password, try again"; UsernameBox.Focus(); }
            }
            else if (userExistsResult == 0) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "Invalid username, try again"; UsernameBox.Focus(); }
            else if (userExistsResult == -1) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "No Internet Connection"; UsernameBox.Focus(); }
            UsernameBox.Text = PasswordBox.Text = string.Empty;
        }
        #endregion
    }
}
