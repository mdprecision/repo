﻿namespace TravellerAuto
{
    partial class NewTravelerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CustomerCombBx = new System.Windows.Forms.ComboBox();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.open_btn = new System.Windows.Forms.Button();
            this.SN_TxtBx = new System.Windows.Forms.TextBox();
            this.TemplatesListBox = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::TravellerAuto.Properties.Resources.sonelelogo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(448, 40);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(9, 72);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Customer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(10, 152);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Template:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(26, 243);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "SN:";
            // 
            // CustomerCombBx
            // 
            this.CustomerCombBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustomerCombBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.CustomerCombBx.FormattingEnabled = true;
            this.CustomerCombBx.Location = new System.Drawing.Point(78, 58);
            this.CustomerCombBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CustomerCombBx.Name = "CustomerCombBx";
            this.CustomerCombBx.Size = new System.Drawing.Size(360, 39);
            this.CustomerCombBx.TabIndex = 4;
            this.CustomerCombBx.SelectedIndexChanged += new System.EventHandler(this.CustomerCombBx_SelectedIndexChanged);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancel_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.cancel_btn.Location = new System.Drawing.Point(9, 268);
            this.cancel_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(116, 41);
            this.cancel_btn.TabIndex = 6;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // open_btn
            // 
            this.open_btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.open_btn.Enabled = false;
            this.open_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.open_btn.Location = new System.Drawing.Point(325, 268);
            this.open_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.open_btn.Name = "open_btn";
            this.open_btn.Size = new System.Drawing.Size(112, 41);
            this.open_btn.TabIndex = 7;
            this.open_btn.Text = "Open";
            this.open_btn.UseVisualStyleBackColor = true;
            this.open_btn.Click += new System.EventHandler(this.open_btn_Click);
            // 
            // SN_TxtBx
            // 
            this.SN_TxtBx.Enabled = false;
            this.SN_TxtBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SN_TxtBx.Location = new System.Drawing.Point(78, 228);
            this.SN_TxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SN_TxtBx.Name = "SN_TxtBx";
            this.SN_TxtBx.Size = new System.Drawing.Size(360, 38);
            this.SN_TxtBx.TabIndex = 8;
            this.SN_TxtBx.TextChanged += new System.EventHandler(this.SN_TxtBx_TextChanged);
            this.SN_TxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SN_TxtBx_KeyPress);
            // 
            // TemplatesListBox
            // 
            this.TemplatesListBox.Enabled = false;
            this.TemplatesListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TemplatesListBox.FormattingEnabled = true;
            this.TemplatesListBox.ItemHeight = 31;
            this.TemplatesListBox.Location = new System.Drawing.Point(78, 97);
            this.TemplatesListBox.Name = "TemplatesListBox";
            this.TemplatesListBox.Size = new System.Drawing.Size(360, 128);
            this.TemplatesListBox.TabIndex = 9;
            this.TemplatesListBox.SelectedIndexChanged += new System.EventHandler(this.TemplatesListBox_SelectedIndexChanged);
            // 
            // NewTravelerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 313);
            this.Controls.Add(this.TemplatesListBox);
            this.Controls.Add(this.SN_TxtBx);
            this.Controls.Add(this.open_btn);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.CustomerCombBx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewTravelerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Start New Traveler";
            this.Load += new System.EventHandler(this.NewTravelerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CustomerCombBx;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Button open_btn;
        private System.Windows.Forms.TextBox SN_TxtBx;
        private System.Windows.Forms.ListBox TemplatesListBox;
    }
}