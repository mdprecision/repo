﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public partial class LogoutForm : Form
    {
        #region FormEvents
        public LogoutForm()
        {
            InitializeComponent();
            Logoutstatus.Text = string.Empty;
        }
        private void LogoutForm_Load(object sender, EventArgs e)
        {
            if (Traveler.LoggedUsers.Count == 0)
            {
                LogoutUsers.Text = "No users logged in";
            }
            else
            {
                // adding current logged in users to the logoust list
                foreach (User user in Traveler.LoggedUsers)
                {
                    LogoutUsers.Items.Add(user.Username);
                }
            }
        }
        private void LogoutBtn_Click(object sender, EventArgs e)
        {
            // if we are offline, disallow logout
            if (!TravelerDatabase.UseDatabase || !TravelerDatabase.PingDB())
            {
                Logoutstatus.ForeColor = Color.Red;
                Logoutstatus.Text = "No Connection";
                return;
            }

            //logout selected user and remove from the list, then refresh users
            if (LogoutUsers.SelectedItems != null)
            {
                if (LogoutUsers.SelectedItem == null)
                {
                    MessageBox.Show("Please choose a user to logout.", "Logout User Required", MessageBoxButtons.OK);
                    return;
                }
                User.Logout(LogoutUsers.SelectedItem.ToString());
            }
            LogoutUsers.Items.Remove(LogoutUsers.SelectedItem);

            //saving to traveler data to update who is logged into traveler
            if (PathMgr.XmlPath != null)
            {
                Traveler.TravelerObject.SaveTravelerData();
            }

            // checking if the user logged out all users


            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.RefreshUsers();
            }
            Close();
        }
        #endregion
    }
}
