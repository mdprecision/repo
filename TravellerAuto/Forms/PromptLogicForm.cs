﻿using System;
using System.Linq;
using System.Windows.Forms;
using TravellerAuto.PromptClasses;

namespace TravellerAuto
{
    public partial class PromptLogicForm : Form
    {
        #region Attributes
        UserPrompt selectedPrompt { get; set; } = null;
        #endregion
        #region FormEvents
        public PromptLogicForm()
        {
            InitializeComponent();
        }
        private void PromptLogicForm_Load(object sender, EventArgs e)
        {
            LoadPrompts();
        }
        private void PromptsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedPrompt = Traveler.TravelerObject.ListOfUserPrompts[PromptsComboBox.SelectedIndex];
            LoadTable();
        }
        #endregion
        #region PromptLogic Functions
        private void LoadPrompts()
        {
            PromptsComboBox.Items.Clear();

            //strings to convert integer validity to a string
            string[] validity = { "Unset", "Invalid", "Valid" };

            //loading each prompt in the combo box 
            foreach (UserPrompt prompt in Traveler.TravelerObject.ListOfUserPrompts)
            {
                PromptsComboBox.Items.Add($"{prompt.PromptName} | Current Value : {prompt.CurrentValue} | Status : {validity[prompt.Valid + 1]}");
            }
        }
        private void LoadTable()
        {
            PromptsLogicTable.Rows.Clear();

            //strings to convert integer validity to a string
            string[] validStrings = { "Unset", "Invalid", "Valid" };

            // if there is prompt logic for the selected prompt, we loop through the logic for that prompt and list it
            if (Traveler.TravelerObject.PromptConstraints.ContainsKey(selectedPrompt.Index))
            {
                for (int i = 0; i < Traveler.TravelerObject.PromptConstraints[selectedPrompt.Index].Count; ++i)
                {
                    PromptLogic logic = Traveler.TravelerObject.PromptConstraints[selectedPrompt.Index][i];
                    if (logic.Length == 2)
                    {
                        PromptsLogicTable.Rows.Add(logic.TargetPrompt, logic.Operator_, string.Empty, string.Empty, validStrings[logic.Valid + 1]);
                    }
                    else
                    {
                        string compared;
                        string offset = string.Empty;
                        if (logic.ComparedPrompt != null)
                        {
                            string type = Traveler.TravelerObject.ListOfUserPrompts
                                .First(e => e.PromptName.ToLower().Equals(logic.TargetPrompt)).PromptType;
                            if (logic.ComparedValue.ToLower().Contains("{[{doc"))
                            {
                                if (Traveler.TravelerObject.GroupDataList[int.Parse(logic.ComparedValue.Split(':')[1].Replace("}]}", string.Empty))].DateComplete == string.Empty)
                                {
                                    compared = $"Date of completion (Group {logic.ComparedValue.Split(':')[1].Replace("}]}", string.Empty)}) (Unset)";
                                }
                                else
                                {
                                    compared = $"Date of completion (Group {logic.ComparedValue.Split(':')[1].Replace("}]}", string.Empty)}) (" +
                                        $"{Traveler.TravelerObject.GroupDataList[int.Parse(logic.ComparedValue.Split(':')[1].Replace("}]}", string.Empty))].DateComplete})";
                                }
                                offset = $"{TimeSpan.FromTicks(long.Parse(logic.Offset)).TotalHours} Hours";
                            }
                            else if (logic.ComparedValue.ToLower().Contains("{[{sn"))
                            {
                                compared = $"SN Number ({Traveler.TravelerObject.TravelerSN})";
                            }
                            else if (type == "DateTime")
                            {
                                compared = $"{new DateTime(long.Parse(logic.ComparedValue))}";
                                offset = $"{TimeSpan.FromTicks(long.Parse(logic.Offset)).TotalHours} Hours";
                            }
                            else if (logic.ComparedPrompt == "NA")
                            {
                                compared = $"{logic.ComparedPrompt} ({logic.ComparedValue})";
                            }
                            else if (PromptLogic.GetComparedValue(logic) == string.Empty)
                            {
                                compared = $"{logic.ComparedPrompt} (Unset)";
                            }
                            else
                            {
                                compared = $"{logic.ComparedPrompt} ({PromptLogic.GetComparedValue(logic)})";
                            }
                        }
                        else
                        {
                            compared = logic.ComparedValue;
                            offset = logic.Offset;
                        }

                        if (logic.Length == 3)
                        {
                            PromptsLogicTable.Rows.Add(logic.TargetPrompt, logic.Operator_, compared, string.Empty, validStrings[logic.Valid + 1]);
                        }
                        else if (logic.Length == 4)
                        {
                            PromptsLogicTable.Rows.Add(logic.TargetPrompt, logic.Operator_, compared, offset, validStrings[logic.Valid + 1]);
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    PromptsLogicTable.Rows[i].ReadOnly = true;
                }
            }
        }
        #endregion
    }
}
