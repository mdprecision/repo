﻿using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public partial class CameraCaptureForm : Form
    {
        #region Attributes
        private static FilterInfoCollection VideoDevices { get; set; }// for list of video devices
        private static VideoCaptureDevice VideoSource { get; set; }  //actual selected video device
        private static bool Started { get; set; } = false;
        #endregion
        #region FormEvents
        public CameraCaptureForm()
        {
            InitializeComponent();
        }
        private void CameraCaptureForm_Load(object sender, EventArgs e)
        {
            //gets list of video input devices
            VideoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (VideoDevices.Count == 0)
            {
                MessageBox.Show("No Camera Devices Found");
                Close();
            }

            // adds the cameras to the dropdown list of available devices
            CameraDropDown.DisplayMember = "Name";
            foreach (FilterInfo device in VideoDevices)
            {
                CameraDropDown.Items.Add(device);
            }
            //defaults to the first item in the list
            CameraDropDown.SelectedIndex = 0;

        }
        private void ActivateCamBtn_Click_1(object sender, EventArgs e)
        {
            if (pictureBox1 != null)
            {
                using (Bitmap snapshot = new Bitmap(pictureBox1.Image))
                {
                    snapshot.Save(Path.Combine($@"{PathMgr.GetImagePth(Traveler.TravelerObject.CustomerName, Traveler.TravelerObject.TemplateName, Traveler.TravelerObject.TravelerSN)}", $"{Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex].PromptName}.png"), ImageFormat.Png);
                }
            }
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.Submit();
            }
            Close();
        }
        private void CameraCaptureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (VideoSource.IsRunning)
            {
                VideoSource.Stop();
            }
        }
        private void CameraDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Started)
            {
                VideoSource.Stop();
            }
            // resets camera capture whnen a new device is selcted
            if (VideoSource != null || !Started)
            {
                VideoSource = new VideoCaptureDevice(((FilterInfo)CameraDropDown.SelectedItem).MonikerString);
                VideoSource.NewFrame += new NewFrameEventHandler(VideoSource_NewFrame);

                //this is used to set max resolution and framerate for the selected camera
                VideoCapabilities[] videocap = VideoSource.VideoCapabilities;
                VideoSource.VideoResolution = videocap[0];

                Height = VideoSource.VideoResolution.FrameSize.Height;
                Width = VideoSource.VideoResolution.FrameSize.Width;

                VideoSource.Start();
                Started = true;
            }
        }
        #endregion
        #region CameraFunctions
        private void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            //gets called (framerate) times per second to update the image on scren
            pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
        }
        #endregion
    }
}
