﻿using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using System;
using System.IO;
using System.Windows.Forms;
using TravellerAuto.Managers;
using Spire.Doc;
using System.Xml.Linq;
using System.Linq;

namespace TravellerAuto
{
    public partial class HelpForm : Form
    {
        #region FormEvents
        public HelpForm()
        {
            InitializeComponent();
        }
        private void HelpForm_Load(object sender, EventArgs e)
        {
            // path ot help document path NETWORK DRIVE
            string docPath = Path.Combine(PathMgr.HelpPath, $"{PathMgr.HelpDocumentName}.docx");

            // path to cached help html file to be read by the webbrowser LOCALLY STORED
            string htmlPath = Path.Combine(PathMgr.HtmlCachePth, $"{PathMgr.HelpDocumentName}.html");

            //convert word doc to html
            if (!File.Exists(htmlPath))
            {
                try
                {
                    // converting the help document to html
                    Document document = new Document();
                    document.HtmlExportOptions.ImageEmbedded = true;
                    document.LoadFromFile(docPath);
                    document.SaveToFile(htmlPath, FileFormat.Html);

                    //removing watermark
                    string html = File.ReadAllText(htmlPath);
                    File.WriteAllText(htmlPath, html.Replace("Evaluation Warning: The document was created with Spire.Doc for .NET.", ""));
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("No help Document Found");
                    Close();
                }
            }
            webBrowser1.Url = new Uri(htmlPath);
        }
        #endregion
    }
}
