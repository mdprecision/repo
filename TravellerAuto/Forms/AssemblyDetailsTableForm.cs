﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public partial class AssemblyDetailsTableForm : Form
    {
        #region FormEvents
        public AssemblyDetailsTableForm()
        {
            InitializeComponent();

            // two first columns of assembly details should not be editable
            AssemblyDetailsGridView.Columns[0].ReadOnly = true;
            AssemblyDetailsGridView.Columns[1].ReadOnly = true;
        }
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            Close();    // cancel button does not update table with entered values
        }
        /// <summary>
        /// Updates the Excel Template with the data entered into the datatable. 
        /// </summary>
        private void update_btn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Traveler.TravelerObject.NumberOfAssemblies; ++i)
            {
                Traveler.TravelerObject.AssemblyDetailsList[i].SN = AssemblyDetailsGridView.Rows[i].Cells[2].Value.ToString();
                Traveler.TravelerObject.AssemblyDetailsList[i].AssemblyNotes = AssemblyDetailsGridView.Rows[i].Cells[3].Value.ToString();
            }
            using (Traveler.WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
            {
                HtmlMgr.ReloadAssemblyDetails();
            }
            Close();
        }
        #endregion
        #region AssemblyDetailsFunctions
        /// <summary>
        /// Loads the data from the spreadsheet template into the assembly details Datatable
        /// </summary>
        /// <param name="PN">A list containing the product numbers of each row in the excel spreadsheet template</param>
        /// <param name="Desc">A list containing the product descriptions of each row in the excel spreadsheet</param>
        public void LoadAssemblyDetails()
        {
            List<PromptClasses.AssemblyDetails> ad = Traveler.TravelerObject.AssemblyDetailsList;
            for (int i = 0; i < Traveler.TravelerObject.NumberOfAssemblies; ++i)
            {
                AssemblyDetailsGridView.Rows.Add(ad[i].PN, ad[i].AssemblyDesc, ad[i].SN, ad[i].AssemblyNotes); //populates datatable with the values from the spreadsheet
            }
        }
        #endregion
    }
}
