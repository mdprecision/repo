﻿using OpenXmlPowerTools;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravellerAuto.Managers;
using TravellerAuto.PromptClasses;

namespace TravellerAuto
{
    public partial class MainForm : Form
    {
        #region Attributes
        public static bool SentByPrompt { get; set; } = false;// passed to certain functions so we know if it was sent by a prompt
        public static bool SkipSnBox { get; set; } = false;// SN box handled is so that we can proccess the combo box in multiple ways, without evaluating multiple events 
        public static bool SkipSavingUsers { get; set; } = false;

        #endregion
        #region Main FormEvents
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // program doesnt work at all if there is no connection to the network drive, check for this first and exit if we cant find it
            if (!PathMgr.CheckNetworkDrive())
            {
                MessageBox.Show("ERROR : Cannot Connect to Sonele Network Drive. Exiting");
                Application.Exit();
            }
            else
            {
                // create directories if they dont exist and load the values into the pathmgr class
                PathMgr.LoadDirectories();

                //create the config file if it doesnt exist, otherwise load the config file
                if (!File.Exists(PathMgr.ConfigPath))
                {
                    Traveler.ConfigObject.SaveConfig();
                }
                else
                {
                    Traveler.ConfigObject = ConfigManager.LoadConfig();
                }

                //Hardcoding an admin user for testing while offline and unable to check database users
                List<User> adminuser = new List<User>
                {
                    new User()
                    {
                        Username = "admin",
                        Email = "admin@mdprecision.com",
                        Salt = Encoding.UTF8.GetBytes("aM"),
                        PasswordHash = User.Hash("admin", Encoding.UTF8.GetBytes("aM")),
                        DefaultAuthority = 2
                    }
                };
                User.HardcodedUsers = adminuser;

                //setting datetime to now
                DateTimeLbl.Text = DateTime.Now.ToString("dd MMMM, yyyy | hh:mm tt");

                //disable traveler controls until a traveler is loaded
                EnableTravelerControls(false);

                // disable all tabs until a traveler is loaded
                foreach (TabPage tab in PromptsTabControl.TabPages)
                {
                    tab.Enabled = false;
                }

                // get customer folders for the combo box to load travelers
                foreach (string customer in PathMgr.GetCustomerDirNames())
                {
                    customersComboBx.Items.Add(customer);
                }
                User.CheckIfLoginRequired();
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB() && Traveler.IsOpen)
            {
                TravelerDatabase.SaveTravelerDB(Traveler.TravelerObject);
                TravelerDatabase.ReleaseTravelerDB(Traveler.TravelerObject);
            }
            Traveler.ConfigObject.SaveConfig();
        }
        private void AssemblyDetailsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            using (AssemblyDetailsTableForm form = new AssemblyDetailsTableForm())
            {
                form.LoadAssemblyDetails();
                form.ShowDialog();
            }
        }
        private void TravelerNotesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (Traveler.TravelerObject.TravelerNotes.Equals("-1"))
            {
                MessageBox.Show("Traveler does not have notes section");
            }
            else
            {
                using (TravelerNotesForm form = new TravelerNotesForm())
                {
                    form.ShowDialog();
                }
            }
        }
        private void SyncTravelerToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB() && Traveler.IsOpen)
            {
                TravelerDatabase.SaveTravelerDB(Traveler.TravelerObject);
            }
        }
        private void CompletedTravelersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (CompletedTravelersForm form = new CompletedTravelersForm())
            {
                form.ShowDialog();
            }
        }
        private void CurrentTravelerToolStripMenuItem_MouseHover(object sender, EventArgs e)
        {
            if (!TravelerDatabase.UseDatabase)
            {
                SaveTravelerToolStripMenuItem.Enabled = false;
            }
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            DateTimeLbl.Text = DateTime.Now.ToString("dd MMMM, yyyy | hh:mm tt");
        }
        private void WebView_NavigationCompleted(object sender, Microsoft.Web.WebView2.Core.CoreWebView2NavigationCompletedEventArgs e)
        {
            if (Traveler.IsOpen)
            {
                if (Traveler.TravelerObject.SelectedPromptIndex != -1)
                {
                    Traveler.TravelerObject.CheckGroupValid();
                }
                _ = Scroll();
            }
        }
        private void StartNewTravelerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // warning if the user tries to create a new traveler with something in the input field
            if (!TextBox.Text.Equals(string.Empty))
            {
                DialogResult warningResult = MessageBox.Show("Warning : Unsubmitted data Still in input field, Would you like to Continue?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (warningResult == DialogResult.No)
                {
                    return;
                }
            }

            //disabling controls temporarily while the new traveler is created
            EnableTravelerControls(false);

            // disable all input tabs so we can enable the correct one later
            foreach (TabPage tab in PromptsTabControl.TabPages)
            {
                tab.Enabled = false;
            }

            // creating new traveler form
            string sn, customer, travelerName;
            using (NewTravelerForm form = new NewTravelerForm())
            {
                DialogResult result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    // if we are leaving a traveler, release it from being 'busy' on the database
                    if (Traveler.IsOpen && TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
                    {
                        TravelerDatabase.ReleaseTravelerDB(Traveler.TravelerObject);
                    }

                    //getting the new traveler base info
                    sn = form.SN;
                    customer = form.Customer;
                    travelerName = form.TravelerName;

                    // setting the load traveler dropdowns
                    customersComboBx.Text = customer;
                    travelersComboBx.Text = travelerName;
                    SNComboBx.Text = sn;

                    // setting the selected indexes correctly
                    int custIndex = customersComboBx.FindString(customer);
                    int trvlrIndex = travelersComboBx.FindString(travelerName);
                    customersComboBx.SelectedIndex = custIndex;
                    travelersComboBx.SelectedIndex = trvlrIndex;

                    // Main function for loading the traveler data
                    Traveler.CreateNewTraveler(customer, travelerName, sn);

                    // switch to first prompt type
                    TabSwitch(Traveler.TravelerObject.ListOfUserPrompts[0]);

                    // set the traveler busy so no other tablet can open it
                    if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
                    {
                        TravelerDatabase.SetTravelerBusyDB(Traveler.TravelerObject);
                    }

                    // new traveler done, re-enable controls and reload the html
                    EnableTravelerControls(true);
                    SaveTravelerToolStripMenuItem.Enabled = true;

                    ReloadBrowser();
                }
            }

        }
        private void LoginUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (LoginForm loginForm = new LoginForm())
            {
                loginForm.ShowDialog();
            }
        }
        private void LogoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (LogoutForm logoutform = new LogoutForm())
            {
                logoutform.ShowDialog();
            }
            User.CheckIfLoginRequired();
        }
        private void RemoveFromTravelerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (RemoveUserForm form = new RemoveUserForm())
            {
                form.ShowDialog();
                if (form.RemovedSuccess)
                {
                    int nextSN = TravelerDatabase.GetNextTraveler(Traveler.TravelerObject.CustomerName, Traveler.TravelerObject.TemplateName);
                    if (nextSN != -1)
                    {
                        // sn box event calls load function
                        SkipSavingUsers = true;
                        SNComboBx.Text = nextSN.ToString().PadLeft(4, '0');
                    }
                    else
                    {
                        // no more travelers next to load
                        if (Traveler.IsOpen && Application.OpenForms["MainForm"] is MainForm mainform)
                        {
                            mainform.CloseTraveler();
                        }
                    }
                }
            }
        }
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (DualCheckBox_ChkBxA.Checked)
            {
                ChkBx_Yes.Focus();
                DualCheckBox_ChkBxB.Checked = false;
            }
        }
        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (DualCheckBox_ChkBxB.Checked)
            {
                ChkBx_No.Focus();
                DualCheckBox_ChkBxA.Checked = false;
            }
        }
        private void ChkBx_Yes_Click(object sender, EventArgs e)
        {
            DualCheckBox_ChkBxA.Checked = true;
            if (!(e is System.Windows.Forms.MouseEventArgs))
            {
                Submit();
            }
        }
        private void ActivateCamBtn_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["CameraCaptureForm"] == null)
            {
                using (CameraCaptureForm form = new CameraCaptureForm())
                {
                    form.ShowDialog();
                }
            }
        }
        private void ChkBx_No_Click(object sender, EventArgs e)
        {
            DualCheckBox_ChkBxB.Checked = true;
            if (!(e is System.Windows.Forms.MouseEventArgs))
            {
                Submit();
            }
        }
        private void Time_NowBtn_Click(object sender, EventArgs e)
        {
            int hour = int.Parse(DateTime.Now.ToString("hh"));
            int min = int.Parse(DateTime.Now.ToString("mm"));
            string ampm = DateTime.Now.ToString("tt");

            DateTime_TimeHourSelector.Text = hour.ToString().PadLeft(2, '0');
            DateTime_TimeMinSelector.Text = min.ToString().PadLeft(2, '0');
            if (ampm.Equals("AM"))
            {
                DateTime_TimeAMPMSelector.SelectedIndex = 0; // AM
            }
            else
            {
                DateTime_TimeAMPMSelector.SelectedIndex = 1; // PM
            }

        }
        private void Date_NowBtn_Click_1(object sender, EventArgs e)
        {
            DateTime_DaySelector.Text = DateTime.Now.ToString("dd");
            DateTime_MonthSelector.Text = DateTime.Now.ToString("MMM");
            DateTime_YearSelector.Text = DateTime.Now.ToString("yyyy");
        }
        private void NextBtn_Click(object sender, EventArgs e)
        {
            Next();
        }
        private void BackBtn_Click_1(object sender, EventArgs e)
        {
            Back();
        }
        private void SubmitPromptBtn_Click(object sender, EventArgs e)
        {
            Submit();
        }
        private void CustomersComboBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            travelersComboBx.Items.Clear();
            SNComboBx.Items.Clear();
            if (customersComboBx.Text != null || customersComboBx.Text != string.Empty)
            {
                foreach (string trvlrName in PathMgr.GetCustomerTrvlrNames(customersComboBx.Text))
                {
                    travelersComboBx.Items.Add(trvlrName);
                }
            }
        }
        private void PromptsTabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (!e.TabPage.Enabled)
            {
                e.Cancel = true;
            }
        }
        private void HelpButton_Click(object sender, EventArgs e)
        {
            using (HelpForm form = new HelpForm())
            {
                form.ShowDialog();
            }
        }
        private void PromptRedactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (User.GetAuthority() < 2)
            {
                MessageBox.Show("No Active Users with Administration permissions");
                return;
            }
            else if (Traveler.TravelerObject.IsComplete)
            {
                MessageBox.Show("Traveler Complete, Cannot perform Redaction");
                return;
            }
            using (PromptRedactionForm form = new PromptRedactionForm())
            {
                form.ShowDialog();
            }
        }
        private void CloseTravelerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseTraveler();
        }
        private void PromptLogicToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            using (PromptLogicForm form = new PromptLogicForm())
            {
                form.ShowDialog();
            }
        }
        private void UsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (User.GetAuthority() < 2)
            {
                MessageBox.Show("No Active Users with Administration permissions");
                return;
            }
            using (AdminUsersForm form = new AdminUsersForm())
            {
                form.ShowDialog();
            }
        }
        #endregion
        #region TravelerFunctions
        private void LoadTraveler()
        {
            // check if the 3 load traveler comboboxes are not empty, and there is a valid SN number in the SN combo box
            if ((customersComboBx.SelectedIndex != -1)
                && (travelersComboBx.SelectedIndex != -1)
                && ((SNComboBx.SelectedIndex != -1) || SNComboBx.Items.Contains(SNComboBx.Text)))
            {
                // check if the traveler is open on another tablet running the program
                if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB()
                && TravelerDatabase.CheckTravelerBusyDB(SNComboBx.Text, customersComboBx.Text, travelersComboBx.Text))
                {
                    MessageBox.Show($"Traveler '{SNComboBx.Text}' already open on another machine");
                    SNComboBx.SelectedIndex = -1;
                    SNComboBx.Text = string.Empty;
                    return;
                }

                // warning if the user has something in the input field before loading another traveler
                if (TextBox.Text != string.Empty)
                {
                    DialogResult warningResult = MessageBox.Show("Warning : Unsubmitted data Still in input field, Would you like to Continue?", "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (warningResult == DialogResult.No)
                    {
                        return;
                    }
                }

                //temporarily disable prompt controls while loading process takes place
                EnableTravelerControls(false);
                SaveTravelerToolStripMenuItem.Enabled = true;

                // if we are not loading the first traveler in the runtime of the program
                if (Traveler.IsOpen)
                {
                    // release current traveler from busy status
                    if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
                    {
                        TravelerDatabase.ReleaseTravelerDB(Traveler.TravelerObject);
                    }

                    // save scroll position on webbrowser
                    _ = SaveScroll();

                    // save travelerdata
                    if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
                    {
                        TravelerDatabase.SaveTravelerDB(Traveler.TravelerObject, SkipSavingUsers);
                    }
                }

                // main function for loading travelerdata
                Traveler.LoadExistingTraveler(customersComboBx.Text, travelersComboBx.Text, SNComboBx.Text);

                //enable correct tab for new traveler loaded
                if (Traveler.TravelerObject.SelectedPromptIndex == -1)
                {
                    TabSwitch(new TextBoxPrompt());
                }
                else if (Traveler.TravelerObject.TravellerIsLocked)
                {
                    PromptsTabControl.SelectedTab.Enabled = false;
                    ErrorPage.Enabled = true;
                    PromptsTabControl.SelectedTab = ErrorPage;
                }
                else
                {
                    TabSwitch();
                }

                //set NEW traveler to busy
                if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
                {
                    TravelerDatabase.SetTravelerBusyDB(Traveler.TravelerObject);
                }
            }
            else
            {
                //incorrect traveler SN to load
                if (Traveler.TravelerObject.TravelerSN != null)
                {
                    // reset sn combo box to the current traveler SN number
                    SNComboBx.SelectedIndex = SNComboBx.Items.IndexOf(Traveler.TravelerObject.TravelerSN);
                    SNComboBx.Text = Traveler.TravelerObject.TravelerSN;
                }
                MessageBox.Show("Traveler Not Found");
            }
            SkipSavingUsers = false;
        }
        /// <summary>
        /// enables/disables all control buttons based on the input boolean
        /// </summary>
        public void EnableTravelerControls(bool travelerLoaded)
        {
            BackBtn.Enabled = travelerLoaded;
            assemblyDetailsToolStripMenuItem.Enabled = travelerLoaded;
            travelerNotesToolStripMenuItem.Enabled = travelerLoaded;
            SaveTravelerToolStripMenuItem.Enabled = travelerLoaded;
            promptLogicToolStripMenuItem.Enabled = travelerLoaded;
            currentTravelerToolStripMenuItem.Enabled = travelerLoaded;
            closeTravelerToolStripMenuItem.Enabled = travelerLoaded;
            completedTravelersToolStripMenuItem.Enabled = travelerLoaded;
            SubmitPromptBtn.Enabled = travelerLoaded;
            NextBtn.Enabled = travelerLoaded;

            // special case where a new traveler does not have submit and back buttons enabled until the traveler has been started
            if (travelerLoaded && Traveler.TravelerObject.SelectedPromptIndex == -1)
            {
                SubmitPromptBtn.Enabled = false;
                BackBtn.Enabled = false;
                foreach (TabPage tab in PromptsTabControl.TabPages)
                {
                    tab.Enabled = false;
                }
            }
        }
        /// <summary>
        /// put into a function to allow submitting through other events, activates submit logic 
        /// </summary>
        public void Submit()
        {
            //warning if the users is submitting blank input, overwriting something currently in the prompt current value
            if (TextBox.Text.Equals(string.Empty) && !Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex].CurrentValue.Equals(string.Empty) && PromptsTabControl.SelectedTab == TxtBxPage)
            {
                DialogResult warningResult = MessageBox.Show("Warning : Submitting blank input, current input will be overwritten. Would you like to Continue?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (warningResult == DialogResult.No)
                {
                    return;
                }
            }
            //temporarily disable prompt controls while submitting value
            EnableTravelerControls(false);

            //setting the selected prompts current value to the current input. also increments selected prompt
            UserPrompt currentPrompt = Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex];
            GetCurrentPromptInput(currentPrompt);

            //switching the control tab to the correct one based on next prompt
            UserPrompt currentPrompt2 = Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex];
            if (!Traveler.TravelerObject.TravellerIsLocked)
            {
                TabSwitch(currentPrompt2);
            }
            ClearAllInputFields();

            //update progress bar after completing prompt
            int completedPrompts = Traveler.TravelerObject.GetUserPromptsWithValues().Count + 1;
            int prog = Convert.ToInt32(completedPrompts / (double)Traveler.TravelerObject.ListOfUserPrompts.Count * 100.0);
            Traveler.TravelerObject.PercentProgress = prog;
            RefreshProgressBar();

            //scroll to next prompt (current since already incremented)
            _ = Scroll(Traveler.TravelerObject.SelectedPromptIndex);

            //re-enable prompt controls if the traveler was not locked from the last prompts input
            EnableTravelerControls(!Traveler.TravelerObject.TravellerIsLocked);
        }
        private void Next()
        {
            // functionality same as Submit() function above without setting hte prompts value
            if (Traveler.IsOpen && Traveler.TravelerObject.SelectedPromptIndex != -1 && TextBox.Text != string.Empty && (TextBox.Text != Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex].CurrentValue))
            {
                DialogResult warningResult = MessageBox.Show("Warning : Unsubmitted data Still in input field, Would you like to Continue?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (warningResult == DialogResult.No)
                {
                    return;
                }
            }
            EnableTravelerControls(false);

            Traveler.SelectNextPrompt();
            ClearAllInputFields();
            UserPrompt currentPrompt = Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex];
            TabSwitch(currentPrompt);
            LoadPreviousFieldValue(currentPrompt);
            _ = Scroll(Traveler.TravelerObject.SelectedPromptIndex);

            EnableTravelerControls(true);
        }
        private void Back()
        {
            //same functionality as Next() function above with the only exception being going backwards when changing prompts
            if (TextBox.Text != string.Empty && (TextBox.Text != Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex].CurrentValue))
            {
                DialogResult warningResult = MessageBox.Show("Warning : Unsubmitted data Still in input field, Would you like to Continue?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (warningResult == DialogResult.No)
                {
                    return;
                }
            }
            EnableTravelerControls(false);

            Traveler.SelectPreviousPrompt();
            SentByPrompt = true;
            ClearAllInputFields();
            UserPrompt currentPrompt = Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex];
            TabSwitch(currentPrompt);
            LoadPreviousFieldValue(currentPrompt);
            _ = Scroll(Traveler.TravelerObject.SelectedPromptIndex);

            EnableTravelerControls(true);
        }
        /// <summary>
        /// updates the html with javascript to avoid having to reload the html.
        /// </summary>
        /// <param name="index">points to the index of prompt to reload</param>
        public void UpdateNode(int index, int group = -1)
        {
            // special index -1 indicates refreshing ASSEMBLY DETAILS
            if (index == -1)
            {
                for (int i = 0; i < Traveler.TravelerObject.NumberOfAssemblies; i++)
                {
                    webView.ExecuteScriptAsync($"document.getElementById('AD:{i}').innerHTML = '{Traveler.TravelerObject.AssemblyDetailsList[i].SN}';");
                    webView.ExecuteScriptAsync($"document.getElementById('ADN:{i}').innerHTML = '{Traveler.TravelerObject.AssemblyDetailsList[i].AssemblyNotes}';");
                }
            }
            // special index -2 indicates refreshing GROUP DATA
            else if (index == -2)
            {
                webView.ExecuteScriptAsync($"document.getElementById('OI:{group}').innerHTML = '{Traveler.TravelerObject.GroupDataList[group].OperatorInitials}';");
                webView.ExecuteScriptAsync($"document.getElementById('ST:{group}').innerHTML = '{Traveler.TravelerObject.GroupDataList[group].PassFail}';");
                webView.ExecuteScriptAsync($"document.getElementById('DoC:{group}').innerHTML = '{Traveler.TravelerObject.GroupDataList[group].DateComplete}';");
            }
            // special index -3 indicates refreshing TRAVELER NOTES
            else if (index == -3)
            {
                webView.ExecuteScriptAsync($"document.getElementById('TravelerNotes').innerHTML = '{Traveler.TravelerObject.TravelerNotes.Replace("\r\n", "</br>")}';");
            }
            //refreshing prompt given by index
            else
            {
                if (Traveler.TravelerObject.ListOfUserPrompts[index].PromptType.Equals("CAM"))
                {
                    webView.ExecuteScriptAsync($"document.getElementById('{index}').outerHTML = '{Traveler.TravelerObject.ListOfUserPrompts[index].GetHtmlCode().Replace(@"\", "/")}';");
                }
                else
                {
                    webView.ExecuteScriptAsync($"document.getElementById('{index}').outerHTML = '{Traveler.TravelerObject.ListOfUserPrompts[index].GetHtmlCode()}';");
                }
            }
        }
        /// <summary>
        /// loads the given prompts value into the text field for input
        /// </summary>
        public void LoadPreviousFieldValue(UserPrompt prompt)
        {
            if (!prompt.IsSet)
            {
                return;
            }

            // txt prompts are easy to load the previous value, just copying string
            if (PromptsTabControl.SelectedTab == TxtBxPage)
            {
                TextBox.Text = prompt.CurrentValue;
            }
            else if (PromptsTabControl.SelectedTab == DateTimePage)
            {
                // cast prompt to datetime to be able to use DT_TYPE member
                DateTimePrompt dPrompt = (DateTimePrompt)prompt;
                if (dPrompt.DT_Type.Equals("DT"))
                {
                    DateTime dt = DateTime.Parse(prompt.CurrentValue);

                    // setting date and time inputs in the datetime tab
                    DateTime_DaySelector.Text = dt.Day.ToString().PadLeft(2, '0');
                    DateTime_MonthSelector.Text = dt.ToString("MMM", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime_YearSelector.Text = dt.Year.ToString();
                    DateTime_TimeHourSelector.Text = dt.Hour.ToString().PadLeft(2, '0');
                    DateTime_TimeMinSelector.Text = dt.Minute.ToString().PadLeft(2, '0');
                    if (dt.ToString("tt").Equals("AM"))
                    {
                        DateTime_TimeAMPMSelector.SelectedIndex = 0;
                    }
                    else
                    {
                        DateTime_TimeAMPMSelector.SelectedIndex = 1;
                    }
                }
                else if (dPrompt.DT_Type.Equals("T"))
                {
                    DateTime dt = DateTime.ParseExact(prompt.CurrentValue, "hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

                    // setting time inputs in datetime tab
                    DateTime_TimeHourSelector.Text = dt.Hour.ToString().PadLeft(2, '0');
                    DateTime_TimeMinSelector.Text = dt.Minute.ToString().PadLeft(2, '0');
                    if (dt.ToString("tt").Equals("AM"))
                    {
                        DateTime_TimeAMPMSelector.SelectedIndex = 0;
                    }
                    else
                    {
                        DateTime_TimeAMPMSelector.SelectedIndex = 1;
                    }
                }
                else // type is date
                {
                    DateTime dt = DateTime.ParseExact(prompt.CurrentValue, "dd/MMM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    // setting only date inputs in datetime tab
                    DateTime_DaySelector.Text = dt.Day.ToString().PadLeft(2, '0');
                    DateTime_MonthSelector.Text = dt.ToString("MMM", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime_YearSelector.Text = dt.Year.ToString();
                }
            }
            else if (PromptsTabControl.SelectedTab == DualChkBxPage)
            {
                // reloading checkbox values, also simple
                if (prompt.CurrentValue.Equals("Yes"))
                {
                    DualCheckBox_ChkBxA.Checked = true;
                    DualCheckBox_ChkBxB.Checked = false;
                }
                else if (prompt.CurrentValue.Equals("No"))
                {
                    DualCheckBox_ChkBxA.Checked = false;
                    DualCheckBox_ChkBxB.Checked = true;
                }
                else
                {
                    DualCheckBox_ChkBxA.Checked = false;
                    DualCheckBox_ChkBxB.Checked = false;
                }
            }
        }
        /// <summary>
        /// Clears all input tabs in the control panel
        /// </summary>
        public void ClearAllInputFields()
        {
            // functionality used to skip clearing the input fields when the next or submit function is called
            if (Traveler.SkipClear) { Traveler.SkipClear = false; return; }

            // TextBox Fields
            TextBox.Text = string.Empty;

            // DualCheckBox Fields
            DualCheckBox_ChkBxA.Checked = false;
            DualCheckBox_ChkBxB.Checked = false;

            // DateTime Fields
            DateTime_DaySelector.Text = DateTime.Now.ToString("dd");
            DateTime_MonthSelector.Text = DateTime.Now.ToString("MMM");
            DateTime_YearSelector.Text = DateTime.Now.ToString("yyyy");
            DateTime_TimeHourSelector.Text = "12";
            DateTime_TimeMinSelector.Text = "00";
            DateTime_TimeAMPMSelector.SelectedIndex = 0;
        }
        private void GetCurrentPromptInput(UserPrompt prompt)
        {
            string promptType = prompt.PromptType;
            switch (promptType)
            {
                case "TXT":
                    Traveler.SetValueToSelectedPrompt(TextBox.Text);
                    return;
                case "YN":
                    Traveler.SetValueToSelectedPrompt(DualCheckBox_ChkBxA.Checked.ToString() + DualCheckBox_ChkBxB.Checked.ToString());
                    return;
                case "DateTime":
                    Traveler.SetValueToSelectedPrompt(GetDateTimeInput());
                    return;
                case "CAM":
                    Traveler.SetValueToSelectedPrompt((Path.Combine(PathMgr.GetImagePth(Traveler.TravelerObject.CustomerName, Traveler.TravelerObject.TemplateName, Traveler.TravelerObject.TravelerSN), $"{prompt.PromptName}.png")).Replace(@"\",@"/"));
                    return;
                default:
                    ErrorMsgLbl.Text += $" Failed to load prompt controls. Prompt Type: {promptType} is unknown.";
                    PromptsTabControl.SelectedTab = ErrorPage;
                    return;
            }
        }
        /// <summary>
        /// prints the current webview2 HTML to PDF
        /// </summary>
        public void PrintToPdf(string path)
        {
            FileSystemWatcher fw = new FileSystemWatcher(PathMgr.CompletedTravelersPath);
            fw.Created += fileSystemWatcher_Created;
            fw.EnableRaisingEvents = true;

            webView.CoreWebView2.PrintToPdfAsync(path);

            void fileSystemWatcher_Created(object sender, FileSystemEventArgs e)
            {
                if (e.Name.Equals($"{Traveler.TravelerObject.TravelerSN.PadLeft(4,'0')}.pdf"))
                {
                    string signOffPagePath = PathMgr.GetSignOffPagePath(Traveler.TravelerObject.CustomerName, Traveler.TravelerObject.TemplateName);
                    if (File.Exists(signOffPagePath))
                    {
                        using (PdfDocument signOffPage = PdfReader.Open(signOffPagePath, PdfDocumentOpenMode.Import))
                        using (PdfDocument travelerPage = PdfReader.Open(path, PdfDocumentOpenMode.Import))
                        using (PdfDocument outPdf = new PdfDocument())
                        {
                            CopyPages(signOffPage, outPdf);
                            CopyPages(travelerPage, outPdf);

                            outPdf.Save(path);
                        }
                        void CopyPages(PdfDocument from, PdfDocument to)
                        {
                            for (int i = 0; i < from.PageCount; i++)
                            {
                                to.AddPage(from.Pages[i]);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("No header sign off sheet found");
                    }
                }
            }
        }
        /// <summary>
        /// sets the current selected tab and focus to the prompt given by the parameter
        /// </summary>
        public void TabSwitch(UserPrompt prompt = null)
        {
            if (prompt == null)
            {
                prompt = Traveler.TravelerObject.ListOfUserPrompts[Traveler.TravelerObject.SelectedPromptIndex];
            }
            string promptType = prompt.PromptType;
            PromptsTabControl.SelectedTab.Enabled = false;
            switch (promptType)
            {
                case "TXT":
                    TxtBxPage.Enabled = true;
                    PromptsTabControl.SelectedTab = TxtBxPage;
                    TextBox.Focus();
                    break;
                case "YN":
                    DualChkBxPage.Enabled = true;
                    PromptsTabControl.SelectedTab = DualChkBxPage;
                    ChkBx_Yes.Focus();
                    break;
                case "DateTime":
                    DateTime_DaySelector.Enabled = true;
                    DateTime_MonthSelector.Enabled = true;
                    DateTime_YearSelector.Enabled = true;
                    DateTime_TimeHourSelector.Enabled = true;
                    DateTime_TimeMinSelector.Enabled = true;
                    DateTime_TimeAMPMSelector.Enabled = true;
                    Date_NowBtn.Enabled = true;
                    Time_NowBtn.Enabled = true;
                    DateTimePage.Enabled = true;
                    PromptsTabControl.SelectedTab = DateTimePage;
                    DateTimePrompt dt = (DateTimePrompt)prompt;
                    string dateType = dt.DT_Type;
                    if (dateType.Equals("D"))
                    {
                        DateTime_DaySelector.Focus();
                        DateTime_TimeHourSelector.Enabled = false;
                        DateTime_TimeMinSelector.Enabled = false;
                        DateTime_TimeAMPMSelector.Enabled = false;
                        Time_NowBtn.Enabled = false;
                    }
                    else if (dateType.Equals("T"))
                    {
                        DateTime_TimeHourSelector.Focus();
                        DateTime_DaySelector.Enabled = false;
                        DateTime_MonthSelector.Enabled = false;
                        DateTime_YearSelector.Enabled = false;
                        Date_NowBtn.Enabled = false;
                    }
                    else
                    {
                        DateTime_DaySelector.Focus();
                    }
                    break;
                case "CAM":
                    CamPage.Enabled = true;
                    PromptsTabControl.SelectedTab = CamPage;
                    break;
                default:
                    ErrorPage.Enabled = true;
                    ErrorMsgLbl.Text += $" Failed to load prompt controls. Prompt Type: {promptType} is unknown.";
                    PromptsTabControl.SelectedTab = ErrorPage;
                    break;
            }
        }
        /// <summary>
        /// gets the current input in the datetime control tab and formats it to a string "dd/MMM/yyy hh:mm:00 tt"
        /// </summary>
        private string GetDateTimeInput()
        {
            string ans = $"{DateTime_DaySelector.Text}/{DateTime_MonthSelector.Text}/{DateTime_YearSelector.Text} ";
            ans += DateTime_TimeHourSelector.Text
                + ":" + DateTime_TimeMinSelector.Text
                + ":" + "00"
                + " " + DateTime_TimeAMPMSelector.Text;
            return ans;
        }
        /// <summary>
        /// locks/unlocks traveller by disabling controls and showing overlay based on the input boolean
        /// </summary>
        public void LockTraveller(bool locked)
        {
            Traveler.TravelerObject.TravellerIsLocked = locked;
            EnableTravelerControls(!locked);
            if (locked && Traveler.TravelerObject.IsComplete)
            {
                ErrorMessage("Traveler Complete", true);
            }
            else if (locked)
            {
                ErrorMessage("Traveler Locked");
            }
            else if (Traveler.TravelerObject.SelectedPromptIndex != -1)
            {
                TabSwitch();    // if disabling the lock, switch to the selected prompt
            }
            else
            {
                TabSwitch(Traveler.TravelerObject.ListOfUserPrompts[0]);
            }
        }
        /// <summary>
        /// reassigns the URL of the webbrowser
        /// </summary>
        public void ReloadBrowser()
        {
            webView.Source = Traveler.HtmlUri;
        }
        /// <summary>
        /// switches to the error message tab and displays the parameter message. optional good boolean to change color to green
        /// </summary>
        public void ErrorMessage(string message, bool good = false)
        {
            if (good)
            {
                ErrorMsgLbl.BackColor = System.Drawing.Color.LimeGreen;
            }
            else
            {
                ErrorMsgLbl.BackColor = System.Drawing.Color.Brown;
            }

            PromptsTabControl.SelectedTab.Enabled = false;
            ErrorPage.Enabled = true;
            // if there are no error messages
            if (ErrorMsgLbl.Text.Equals(string.Empty))
            {
                ErrorMsgLbl.Text = $"{message}";
            }
            // if there is already a message with the same error, append the text instead
            else if (!ErrorMsgLbl.Text.Contains(message))
            {
                ErrorMsgLbl.Text += $" | {message}";
            }

            PromptsTabControl.SelectedTab = ErrorPage;
        }
        // <summary>
        /// sets progress bar value and refreshes it
        /// </summary>
        public void RefreshProgressBar()
        {
            if (Traveler.TravelerObject.PercentProgress > 100)
            {
                Traveler.TravelerObject.PercentProgress = 100;
            }
            TravelerProgressBar.Value = Traveler.TravelerObject.PercentProgress;
            TravelerProgressBar.Refresh();
        }
        /// <summary>
        /// gets the current scroll position and saves it to XML
        /// </summary>
        public async Task SaveScroll(bool save = true)
        {
            // get the scroll position and save it
            string result = await webView.ExecuteScriptAsync("window.scrollY;");
            Traveler.TravelerObject.ScrollPosition = Convert.ToInt32(result);
            if (save)
            {
                Traveler.TravelerObject.SaveTravelerData();
            }
        }
        /// <summary>
        /// scrolls to the prompt by parameter index, and saves it
        /// </summary>
        public new async Task Scroll(int index = -1)
        {
            // if no prompt is provided, scroll to the travelers scroll position
            if (index == -1)
            {
                await webView.ExecuteScriptAsync($"window.scrollTo({{top: {Traveler.TravelerObject.ScrollPosition}, behavior:'smooth'}});");
            }
            else
            {
                // get scroll position of indexed prompt (index on html documents correspond with indexes in this program)
                string result = await webView.ExecuteScriptAsync($"document.getElementById('{index}').getBoundingClientRect().top + window.scrollY - 180;");
                if (result != null)
                {
                    Traveler.TravelerObject.ScrollPosition = Convert.ToInt32(Math.Floor(Convert.ToDouble(result)));
                }

                //scroll function
                await webView.ExecuteScriptAsync($"window.scrollTo({{top: {Traveler.TravelerObject.ScrollPosition}, behavior:'smooth'}});");
            }

            //save the new scroll position after scrolling
            _ = SaveScroll();
        }
        /// <summary>
        /// Refreshes Active User GUI with current active users
        /// </summary>
        public void RefreshUsers()
        {
            List<string> ActiveUsernames = User.GetActiveUsers(Traveler.TravelerObject.ActiveUsers);
            foreach (User user in Traveler.LoggedUsers)
            {
                if (!ActiveUsernames.Contains(user.Username))
                {
                    Traveler.TravelerObject.ActiveUsers.Add(user);
                }
            }
            List<string> listOfActiveUsers = User.GetActiveUsers(Traveler.LoggedUsers);
            if (listOfActiveUsers.Count != 0)
            {
                activeUsrsLbl.Text = listOfActiveUsers.StrCat(", ");
            }
            else
            {
                activeUsrsLbl.Text = "N/A";
            }
            if (Traveler.IsOpen)
            {
                TravelerDatabase.UpdateUsersDB(Traveler.TravelerObject);
            }
            if (PathMgr.XmlPath != null)
            {
                Traveler.TravelerObject.SaveTravelerData();
            }
        }
        /// <summary>
        /// Resets the Program after logging out last user
        /// </summary>
        public  void CloseTraveler()
        {
            EnableTravelerControls(false);
            Traveler.IsOpen = false;
            Traveler.TravelerObject = new TravelerData();
            currentTravelerToolStripMenuItem.Enabled = false;
            SNComboBx.Text = string.Empty;
            SNComboBx.SelectedItem = null;
            webView.NavigateToString(string.Empty);
            TravelerProgressBar.Value = 0;
        }
        /// <summary>
        /// gets the selected customer and template from the Mainform GUI. returns null if either are empty
        /// </summary>
        public Tuple<string,string> GetCustomerAndTemplate()
        {
            if (customersComboBx.Text.Equals(string.Empty) || travelersComboBx.Text.Equals(string.Empty))
            {
                return null;
            }
            else
            {
                return new Tuple<string,string> (customersComboBx.Text, travelersComboBx.Text);
            }
        }
        #endregion
        #region SNCombo Box Events
        private void SNComboBx_Validated(object sender, EventArgs e)
        {
            if (!SkipSnBox && SNComboBx.Text != string.Empty)
            {
                // SN box handled is so that we can proccess the combo box in multiple ways, without evaluating multiple events 
                SkipSnBox = true;

                //main function to load traveler
                LoadTraveler();

                //setting selection length to zero so it doesnt appear highlited
                SNComboBx.SelectionLength = 0;

                //switch the input tab to the current selected prompts type
                if (Traveler.TravelerObject.SelectedPromptIndex != -1 && !Traveler.TravelerObject.TravellerIsLocked)
                {
                    TabSwitch();
                }
                // set focus to the correct tab
                ActiveControl = PromptsTabControl.SelectedTab;

                // setting the handle back to false (other events have *likely* proccessed by now: UNSAFE, TO CHANGE)
                SkipSnBox = false;
            }
        }
        private void SNComboBx_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 && !SkipSnBox)
            {
                // SN box handled is so that we can proccess the combo box in multiple ways, without evaluating multiple events 
                SkipSnBox = true;
                e.Handled = true;

                //main function to load traveler
                LoadTraveler();

                //setting selection length to zero so it doesnt appear highlited
                SNComboBx.SelectionLength = 0;

                //switch the input tab to the current selected prompts type
                if (Traveler.TravelerObject.SelectedPromptIndex != -1 && !Traveler.TravelerObject.TravellerIsLocked)
                {
                    TabSwitch();
                }
                // set focus to the correct tab
                ActiveControl = PromptsTabControl.SelectedTab;

                // setting the handle back to false (other SNbox events have *likely* proccessed by now: UNSAFE, TO CHANGE)
                SkipSnBox = false;
            }
        }
        private void SNComboBx_DropDown(object sender, EventArgs e)
        {
            SNComboBx.Items.Clear();
            if ((customersComboBx.SelectedIndex != -1)
                && (travelersComboBx.SelectedIndex != -1))
            {
                if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
                {
                    // get the existing travelers and add them to the combo box
                    List<int> existingSNs = TravelerDatabase.GetTravelersDB(customersComboBx.Text, travelersComboBx.Text);
                    foreach (int sn in existingSNs)
                    {
                        SNComboBx.Items.Add(sn.ToString().PadLeft(4, '0'));
                    }
                }
                else
                {
                    foreach(string sn in PathMgr.GetExistingTravelerSNs(customersComboBx.Text, travelersComboBx.Text))
                    {
                        SNComboBx.Items.Add(sn.PadLeft(4,'0'));
                    }
                }

            }
        }
        private void SNComboBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!SkipSnBox && SNComboBx.Text != string.Empty)
            {
                // SN box handled is so that we can proccess the combo box in multiple ways, without evaluating multiple events 
                SkipSnBox = true;

                //main function to load traveler
                LoadTraveler();

                //setting selection length to zero so it doesnt appear highlited
                SNComboBx.SelectionLength = 0;

                //switch the input tab to the current selected prompts type
                if (Traveler.TravelerObject.SelectedPromptIndex != -1 && !Traveler.TravelerObject.TravellerIsLocked)
                {
                    TabSwitch();
                }
                // set focus to the correct tab
                ActiveControl = PromptsTabControl.SelectedTab;

                // setting the handle back to false (other events have *likely* proccessed by now: UNSAFE, TO CHANGE)
                SkipSnBox = false;
            }
        }
        #endregion
        #region KeyBind FormEvents
        //keycodes below
        // 13 = ENTER
        // 50 = number 2
        // 49 = number 1
        // below events contain functionality for changing prompts and submitting promts using arrows and enter keys
        private void ChkBx_Yes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)50) { DualCheckBox_ChkBxB.Checked = true; ChkBx_No.Focus(); }
            else if (e.KeyChar == (char)49) { DualCheckBox_ChkBxA.Checked = true; ChkBx_Yes.Focus(); }
        }
        private void ChkBx_No_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)49) { DualCheckBox_ChkBxA.Checked = true; ChkBx_Yes.Focus(); }
            else if (e.KeyChar == (char)50)
            {
                DualCheckBox_ChkBxB.Checked = true; ChkBx_No.Focus();
            }
        }
        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; Submit(); }
        }
        private void DateTime_TimeAMPMSelector_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (!DateTime_TimeAMPMSelector.Text.Equals("AM") && !DateTime_TimeAMPMSelector.Text.Equals("PM"))
                {
                    DateTime_TimeAMPMSelector.Text = "AM";
                    DateTime_TimeAMPMSelector.SelectedIndex = 0;
                }
                else
                {
                    e.Handled = true; Submit();
                }
            }
        }
        private void DateTime_YearSelector_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (!int.TryParse(DateTime_YearSelector.Text, out int output) || DateTime_YearSelector.Text.Length > 4 || DateTime_YearSelector.Text.Length == 0 || output < 1)
                {
                    DateTime_YearSelector.Text = "2022";
                }
                else
                {
                    e.Handled = true; Submit();
                }
            }
        }
        private void DateTime_MonthSelector_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                string[] months = { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };
                if (!months.ToList().Contains(DateTime_MonthSelector.Text) && DateTime_MonthSelector.Text.Length != 3)
                {
                    DateTime_MonthSelector.Text = "Jan";
                    return;
                }
                else
                {
                    DateTime_MonthSelector.Text = char.ToUpper(DateTime_MonthSelector.Text.ToString()[0]).ToString() + char.ToLower(DateTime_MonthSelector.Text.ToString()[1]) + char.ToLower(DateTime_MonthSelector.Text.ToString()[2]);
                }
                int[] monthDays = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
                if (!int.TryParse(DateTime_DaySelector.Text, out int output2) || DateTime_DaySelector.Text.Length > 2 || DateTime_DaySelector.Text.Length == 0 || output2 < 1 || output2 > monthDays[months.ToList().IndexOf(DateTime_MonthSelector.Text.ToLower())])
                {
                    DateTime_DaySelector.Text = "01";
                    return;
                }
                DateTime_DaySelector.Text = DateTime_DaySelector.Text.PadLeft(2, '0');
                e.Handled = true; Submit();
            }
        }
        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_DaySelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_MonthSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_YearSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_TimeHourSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_TimeMinSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_TimeAMPMSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void ChkBx_Yes_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void ChkBx_No_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                Back();
            }
            else if (e.KeyCode == Keys.Down)
            {
                Next();
            }
        }
        private void DateTime_DaySelector_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                string[] months = { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };
                int[] monthDays = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
                if (!int.TryParse(DateTime_DaySelector.Text, out int output) || DateTime_DaySelector.Text.Length > 2 || DateTime_DaySelector.Text.Length == 0 || output < 1 || output > monthDays[months.ToList().IndexOf(DateTime_MonthSelector.Text.ToLower())])
                {
                    DateTime_DaySelector.Text = "01";
                }
                else
                {
                    e.Handled = true; Submit();
                }
            }
        }
        private void DateTime_TimeHourSelector_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (!int.TryParse(DateTime_TimeHourSelector.Text, out int output) || DateTime_TimeHourSelector.Text.Length > 2 || DateTime_TimeHourSelector.Text.Length == 0 || output < 0 || output > 12)
                {
                    DateTime_TimeHourSelector.Text = "12";
                }
                else
                {
                    e.Handled = true; Submit();
                }
            }
        }
        private void DateTime_TimeMinSelector_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (!int.TryParse(DateTime_TimeMinSelector.Text, out int output) || DateTime_TimeMinSelector.Text.Length > 2 || DateTime_TimeMinSelector.Text.Length == 0 || output < 0 || output > 59)
                {
                    DateTime_TimeMinSelector.Text = "00";
                }
                else
                {
                    e.Handled = true; Submit();
                }
            }
        }
        #endregion
        #region DateTime Tab Events
        private void DateTime_TimeMinSelector_Validated(object sender, EventArgs e)
        {
            if (!int.TryParse(DateTime_TimeMinSelector.Text, out int output) || DateTime_TimeMinSelector.Text.Length > 2 || DateTime_TimeMinSelector.Text.Length == 0 || output < 0 || output > 59)
            {
                DateTime_TimeMinSelector.Text = "00";
            }
            DateTime_TimeMinSelector.Text = DateTime_TimeMinSelector.Text.PadLeft(2, '0');
        }
        private void DateTime_TimeHourSelector_Validated(object sender, EventArgs e)
        {
            if (!int.TryParse(DateTime_TimeHourSelector.Text, out int output) || DateTime_TimeHourSelector.Text.Length > 2 || DateTime_TimeHourSelector.Text.Length == 0 || output < 0 || output > 12)
            {
                DateTime_TimeHourSelector.Text = "12";
            }
            DateTime_TimeHourSelector.Text = DateTime_TimeHourSelector.Text.PadLeft(2, '0');
        }
        private void DateTime_MonthSelector_Validated(object sender, EventArgs e)
        {
            string[] months = { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };
            if (!months.ToList().Contains(DateTime_MonthSelector.Text) && DateTime_MonthSelector.Text.Length != 3)
            {
                DateTime_MonthSelector.Text = "Jan";
            }
            else
            {
                DateTime_MonthSelector.Text = char.ToUpper(DateTime_MonthSelector.Text.ToString()[0]).ToString() + char.ToLower(DateTime_MonthSelector.Text.ToString()[1]) + char.ToLower(DateTime_MonthSelector.Text.ToString()[2]);
            }

            // monthsdays are for checking if the input days are greater than days within each month
            int[] monthDays = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            if (!int.TryParse(DateTime_DaySelector.Text, out int output2) || DateTime_DaySelector.Text.Length > 2 || DateTime_DaySelector.Text.Length == 0 || output2 < 1 || output2 > monthDays[months.ToList().IndexOf(DateTime_MonthSelector.Text.ToLower())])
            {
                DateTime_DaySelector.Text = "01";
            }
            DateTime_DaySelector.Text = DateTime_DaySelector.Text.PadLeft(2, '0');
        }
        private void DateTime_DaySelector_Validated(object sender, EventArgs e)
        {
            int[] monthDays = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            string[] months = { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };

            if (!int.TryParse(DateTime_DaySelector.Text, out int output) || DateTime_DaySelector.Text.Length > 2 || DateTime_DaySelector.Text.Length == 0 || output < 1 || output > monthDays[months.ToList().IndexOf(DateTime_MonthSelector.Text.ToLower())])
            {
                DateTime_DaySelector.Text = "01";
            }
            DateTime_DaySelector.Text = DateTime_DaySelector.Text.PadLeft(2, '0');
        }
        private void DateTime_YearSelector_Validated(object sender, EventArgs e)
        {
            if (!int.TryParse(DateTime_YearSelector.Text, out int output) || DateTime_YearSelector.Text.Length > 4 || DateTime_YearSelector.Text.Length == 0 || output < 1)
            {
                DateTime_YearSelector.Text = "2022";
            }
        }
        private void DateTime_TimeAMPMSelector_Validated(object sender, EventArgs e)
        {
            if (!DateTime_TimeAMPMSelector.Text.Equals("AM") || !DateTime_TimeAMPMSelector.Text.Equals("PM"))
            {
                DateTime_TimeAMPMSelector.Text = "AM";
                DateTime_TimeAMPMSelector.SelectedIndex = 0;
            }
        }
        #endregion


    }
}
