﻿
namespace TravellerAuto
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startNewTravelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentTravelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promptLogicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assemblyDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.travelerNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveTravelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.completedTravelersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeTravelerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promptControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promptRedactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ActiveUsersLbl = new System.Windows.Forms.Label();
            this.DateTimeLbl = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.activeUsrsLbl = new System.Windows.Forms.Label();
            this.TextBoxPanel = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.PromptControlGroupBox = new System.Windows.Forms.GroupBox();
            this.BackBtn = new System.Windows.Forms.Button();
            this.PromptsTabControl = new System.Windows.Forms.TabControl();
            this.TxtBxPage = new System.Windows.Forms.TabPage();
            this.TextBox = new System.Windows.Forms.TextBox();
            this.DualChkBxPage = new System.Windows.Forms.TabPage();
            this.DualCheckBox_ChkBxB = new System.Windows.Forms.CheckBox();
            this.DualCheckBox_ChkBxA = new System.Windows.Forms.CheckBox();
            this.ChkBx_No = new System.Windows.Forms.Button();
            this.ChkBx_Yes = new System.Windows.Forms.Button();
            this.DateTimePage = new System.Windows.Forms.TabPage();
            this.yearLabel = new System.Windows.Forms.Label();
            this.monthLabel = new System.Windows.Forms.Label();
            this.dayLabel = new System.Windows.Forms.Label();
            this.DateTime_YearSelector = new System.Windows.Forms.TextBox();
            this.DateTime_MonthSelector = new System.Windows.Forms.TextBox();
            this.DateTime_DaySelector = new System.Windows.Forms.TextBox();
            this.DateTime_TimeMinSelector = new System.Windows.Forms.TextBox();
            this.DateTime_TimeHourSelector = new System.Windows.Forms.TextBox();
            this.TimeSemicolon_Lbl = new System.Windows.Forms.Label();
            this.Date_NowBtn = new System.Windows.Forms.Button();
            this.DateTime_TimeAMPMSelector = new System.Windows.Forms.DomainUpDown();
            this.Time_NowBtn = new System.Windows.Forms.Button();
            this.CamPage = new System.Windows.Forms.TabPage();
            this.ActivateCamBtn = new System.Windows.Forms.Button();
            this.ErrorPage = new System.Windows.Forms.TabPage();
            this.ErrorMsgLbl = new System.Windows.Forms.Label();
            this.SubmitPromptBtn = new System.Windows.Forms.Button();
            this.NextBtn = new System.Windows.Forms.Button();
            this.TravelerProgressBar = new System.Windows.Forms.ProgressBar();
            this.customersComboBx = new System.Windows.Forms.ComboBox();
            this.travelersComboBx = new System.Windows.Forms.ComboBox();
            this.SN_Lbl = new System.Windows.Forms.Label();
            this.SNComboBx = new System.Windows.Forms.ComboBox();
            this.HelpDocument = new System.Windows.Forms.Button();
            this.webView = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.menuStrip1.SuspendLayout();
            this.TextBoxPanel.SuspendLayout();
            this.PromptControlGroupBox.SuspendLayout();
            this.PromptsTabControl.SuspendLayout();
            this.TxtBxPage.SuspendLayout();
            this.DualChkBxPage.SuspendLayout();
            this.DateTimePage.SuspendLayout();
            this.CamPage.SuspendLayout();
            this.ErrorPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.webView)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.userToolStripMenuItem,
            this.promptControlToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1069, 54);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startNewTravelerToolStripMenuItem,
            this.currentTravelerToolStripMenuItem,
            this.completedTravelersToolStripMenuItem,
            this.closeTravelerToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(150, 50);
            this.fileToolStripMenuItem.Text = "Traveler";
            // 
            // startNewTravelerToolStripMenuItem
            // 
            this.startNewTravelerToolStripMenuItem.Name = "startNewTravelerToolStripMenuItem";
            this.startNewTravelerToolStripMenuItem.Size = new System.Drawing.Size(419, 50);
            this.startNewTravelerToolStripMenuItem.Text = "Start New Traveler";
            this.startNewTravelerToolStripMenuItem.Click += new System.EventHandler(this.StartNewTravelerToolStripMenuItem_Click);
            // 
            // currentTravelerToolStripMenuItem
            // 
            this.currentTravelerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.promptLogicToolStripMenuItem,
            this.assemblyDetailsToolStripMenuItem,
            this.travelerNotesToolStripMenuItem,
            this.SaveTravelerToolStripMenuItem});
            this.currentTravelerToolStripMenuItem.Name = "currentTravelerToolStripMenuItem";
            this.currentTravelerToolStripMenuItem.Size = new System.Drawing.Size(419, 50);
            this.currentTravelerToolStripMenuItem.Text = "Current Traveler";
            this.currentTravelerToolStripMenuItem.MouseHover += new System.EventHandler(this.CurrentTravelerToolStripMenuItem_MouseHover);
            // 
            // promptLogicToolStripMenuItem
            // 
            this.promptLogicToolStripMenuItem.Name = "promptLogicToolStripMenuItem";
            this.promptLogicToolStripMenuItem.Size = new System.Drawing.Size(365, 50);
            this.promptLogicToolStripMenuItem.Text = "Prompt Logic";
            this.promptLogicToolStripMenuItem.Click += new System.EventHandler(this.PromptLogicToolStripMenuItem_Click_1);
            // 
            // assemblyDetailsToolStripMenuItem
            // 
            this.assemblyDetailsToolStripMenuItem.Name = "assemblyDetailsToolStripMenuItem";
            this.assemblyDetailsToolStripMenuItem.Size = new System.Drawing.Size(365, 50);
            this.assemblyDetailsToolStripMenuItem.Text = "Assembly Details";
            this.assemblyDetailsToolStripMenuItem.Click += new System.EventHandler(this.AssemblyDetailsToolStripMenuItem_Click_1);
            // 
            // travelerNotesToolStripMenuItem
            // 
            this.travelerNotesToolStripMenuItem.Name = "travelerNotesToolStripMenuItem";
            this.travelerNotesToolStripMenuItem.Size = new System.Drawing.Size(365, 50);
            this.travelerNotesToolStripMenuItem.Text = "Notes";
            this.travelerNotesToolStripMenuItem.Click += new System.EventHandler(this.TravelerNotesToolStripMenuItem_Click_1);
            // 
            // SaveTravelerToolStripMenuItem
            // 
            this.SaveTravelerToolStripMenuItem.Enabled = false;
            this.SaveTravelerToolStripMenuItem.Name = "SaveTravelerToolStripMenuItem";
            this.SaveTravelerToolStripMenuItem.Size = new System.Drawing.Size(365, 50);
            this.SaveTravelerToolStripMenuItem.Text = "Save Traveler";
            this.SaveTravelerToolStripMenuItem.Click += new System.EventHandler(this.SyncTravelerToolStripMenuItem_Click_1);
            // 
            // completedTravelersToolStripMenuItem
            // 
            this.completedTravelersToolStripMenuItem.Name = "completedTravelersToolStripMenuItem";
            this.completedTravelersToolStripMenuItem.Size = new System.Drawing.Size(419, 50);
            this.completedTravelersToolStripMenuItem.Text = "Completed Travelers";
            this.completedTravelersToolStripMenuItem.Click += new System.EventHandler(this.CompletedTravelersToolStripMenuItem_Click);
            // 
            // closeTravelerToolStripMenuItem
            // 
            this.closeTravelerToolStripMenuItem.Name = "closeTravelerToolStripMenuItem";
            this.closeTravelerToolStripMenuItem.Size = new System.Drawing.Size(419, 50);
            this.closeTravelerToolStripMenuItem.Text = "Close Traveler";
            this.closeTravelerToolStripMenuItem.Click += new System.EventHandler(this.CloseTravelerToolStripMenuItem_Click);
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginUserToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.userToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(101, 50);
            this.userToolStripMenuItem.Text = "User";
            // 
            // loginUserToolStripMenuItem
            // 
            this.loginUserToolStripMenuItem.Name = "loginUserToolStripMenuItem";
            this.loginUserToolStripMenuItem.Size = new System.Drawing.Size(221, 50);
            this.loginUserToolStripMenuItem.Text = "Login";
            this.loginUserToolStripMenuItem.Click += new System.EventHandler(this.LoginUserToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(221, 50);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.LogoutToolStripMenuItem_Click);
            // 
            // promptControlToolStripMenuItem
            // 
            this.promptControlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.promptRedactionToolStripMenuItem,
            this.usersToolStripMenuItem});
            this.promptControlToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.promptControlToolStripMenuItem.Name = "promptControlToolStripMenuItem";
            this.promptControlToolStripMenuItem.Size = new System.Drawing.Size(132, 50);
            this.promptControlToolStripMenuItem.Text = "Admin";
            // 
            // promptRedactionToolStripMenuItem
            // 
            this.promptRedactionToolStripMenuItem.Name = "promptRedactionToolStripMenuItem";
            this.promptRedactionToolStripMenuItem.Size = new System.Drawing.Size(384, 50);
            this.promptRedactionToolStripMenuItem.Text = "Prompt Redaction";
            this.promptRedactionToolStripMenuItem.Click += new System.EventHandler(this.PromptRedactionToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(384, 50);
            this.usersToolStripMenuItem.Text = "Users";
            this.usersToolStripMenuItem.Click += new System.EventHandler(this.UsersToolStripMenuItem_Click);
            // 
            // ActiveUsersLbl
            // 
            this.ActiveUsersLbl.AutoSize = true;
            this.ActiveUsersLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ActiveUsersLbl.Location = new System.Drawing.Point(680, 2);
            this.ActiveUsersLbl.Name = "ActiveUsersLbl";
            this.ActiveUsersLbl.Size = new System.Drawing.Size(122, 20);
            this.ActiveUsersLbl.TabIndex = 2;
            this.ActiveUsersLbl.Text = "Active User(s):";
            // 
            // DateTimeLbl
            // 
            this.DateTimeLbl.AutoSize = true;
            this.DateTimeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.DateTimeLbl.Location = new System.Drawing.Point(680, 28);
            this.DateTimeLbl.Name = "DateTimeLbl";
            this.DateTimeLbl.Size = new System.Drawing.Size(32, 20);
            this.DateTimeLbl.TabIndex = 3;
            this.DateTimeLbl.Text = "DT";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // activeUsrsLbl
            // 
            this.activeUsrsLbl.AutoSize = true;
            this.activeUsrsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.activeUsrsLbl.Location = new System.Drawing.Point(808, 2);
            this.activeUsrsLbl.Name = "activeUsrsLbl";
            this.activeUsrsLbl.Size = new System.Drawing.Size(37, 20);
            this.activeUsrsLbl.TabIndex = 9;
            this.activeUsrsLbl.Text = "N/A";
            // 
            // TextBoxPanel
            // 
            this.TextBoxPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxPanel.Controls.Add(this.textBox1);
            this.TextBoxPanel.Controls.Add(this.label2);
            this.TextBoxPanel.Location = new System.Drawing.Point(1, 3793);
            this.TextBoxPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TextBoxPanel.Name = "TextBoxPanel";
            this.TextBoxPanel.Size = new System.Drawing.Size(3513, 231);
            this.TextBoxPanel.TabIndex = 10;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(181, 25);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1027, 22);
            this.textBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(7, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Text Box Control Panel:";
            // 
            // PromptControlGroupBox
            // 
            this.PromptControlGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PromptControlGroupBox.Controls.Add(this.BackBtn);
            this.PromptControlGroupBox.Controls.Add(this.PromptsTabControl);
            this.PromptControlGroupBox.Controls.Add(this.SubmitPromptBtn);
            this.PromptControlGroupBox.Controls.Add(this.NextBtn);
            this.PromptControlGroupBox.Controls.Add(this.TravelerProgressBar);
            this.PromptControlGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PromptControlGroupBox.Location = new System.Drawing.Point(5, 556);
            this.PromptControlGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PromptControlGroupBox.Name = "PromptControlGroupBox";
            this.PromptControlGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PromptControlGroupBox.Size = new System.Drawing.Size(1060, 172);
            this.PromptControlGroupBox.TabIndex = 16;
            this.PromptControlGroupBox.TabStop = false;
            // 
            // BackBtn
            // 
            this.BackBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BackBtn.BackgroundImage = global::TravellerAuto.Properties.Resources.previousprmptbtn;
            this.BackBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BackBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BackBtn.Location = new System.Drawing.Point(736, 30);
            this.BackBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(100, 100);
            this.BackBtn.TabIndex = 18;
            this.BackBtn.TabStop = false;
            this.BackBtn.UseVisualStyleBackColor = true;
            this.BackBtn.Click += new System.EventHandler(this.BackBtn_Click_1);
            // 
            // PromptsTabControl
            // 
            this.PromptsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PromptsTabControl.Controls.Add(this.TxtBxPage);
            this.PromptsTabControl.Controls.Add(this.DualChkBxPage);
            this.PromptsTabControl.Controls.Add(this.DateTimePage);
            this.PromptsTabControl.Controls.Add(this.CamPage);
            this.PromptsTabControl.Controls.Add(this.ErrorPage);
            this.PromptsTabControl.Location = new System.Drawing.Point(7, 22);
            this.PromptsTabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PromptsTabControl.Name = "PromptsTabControl";
            this.PromptsTabControl.SelectedIndex = 0;
            this.PromptsTabControl.Size = new System.Drawing.Size(675, 117);
            this.PromptsTabControl.TabIndex = 0;
            this.PromptsTabControl.TabStop = false;
            this.PromptsTabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.PromptsTabControl_Selecting);
            // 
            // TxtBxPage
            // 
            this.TxtBxPage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.TxtBxPage.Controls.Add(this.TextBox);
            this.TxtBxPage.Location = new System.Drawing.Point(4, 27);
            this.TxtBxPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TxtBxPage.Name = "TxtBxPage";
            this.TxtBxPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TxtBxPage.Size = new System.Drawing.Size(667, 86);
            this.TxtBxPage.TabIndex = 0;
            this.TxtBxPage.Text = "Text Box";
            // 
            // TextBox
            // 
            this.TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F);
            this.TextBox.Location = new System.Drawing.Point(3, 2);
            this.TextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TextBox.Name = "TextBox";
            this.TextBox.Size = new System.Drawing.Size(661, 79);
            this.TextBox.TabIndex = 1;
            this.TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.TextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyUp);
            // 
            // DualChkBxPage
            // 
            this.DualChkBxPage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DualChkBxPage.Controls.Add(this.DualCheckBox_ChkBxB);
            this.DualChkBxPage.Controls.Add(this.DualCheckBox_ChkBxA);
            this.DualChkBxPage.Controls.Add(this.ChkBx_No);
            this.DualChkBxPage.Controls.Add(this.ChkBx_Yes);
            this.DualChkBxPage.Location = new System.Drawing.Point(4, 27);
            this.DualChkBxPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DualChkBxPage.Name = "DualChkBxPage";
            this.DualChkBxPage.Size = new System.Drawing.Size(667, 86);
            this.DualChkBxPage.TabIndex = 4;
            this.DualChkBxPage.Text = "Dual Check Box";
            // 
            // DualCheckBox_ChkBxB
            // 
            this.DualCheckBox_ChkBxB.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DualCheckBox_ChkBxB.AutoSize = true;
            this.DualCheckBox_ChkBxB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.DualCheckBox_ChkBxB.Location = new System.Drawing.Point(537, 31);
            this.DualCheckBox_ChkBxB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DualCheckBox_ChkBxB.Name = "DualCheckBox_ChkBxB";
            this.DualCheckBox_ChkBxB.Size = new System.Drawing.Size(18, 17);
            this.DualCheckBox_ChkBxB.TabIndex = 13;
            this.DualCheckBox_ChkBxB.TabStop = false;
            this.DualCheckBox_ChkBxB.UseVisualStyleBackColor = true;
            this.DualCheckBox_ChkBxB.CheckedChanged += new System.EventHandler(this.CheckBox2_CheckedChanged);
            // 
            // DualCheckBox_ChkBxA
            // 
            this.DualCheckBox_ChkBxA.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DualCheckBox_ChkBxA.AutoSize = true;
            this.DualCheckBox_ChkBxA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.DualCheckBox_ChkBxA.Location = new System.Drawing.Point(216, 31);
            this.DualCheckBox_ChkBxA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DualCheckBox_ChkBxA.Name = "DualCheckBox_ChkBxA";
            this.DualCheckBox_ChkBxA.Size = new System.Drawing.Size(18, 17);
            this.DualCheckBox_ChkBxA.TabIndex = 12;
            this.DualCheckBox_ChkBxA.TabStop = false;
            this.DualCheckBox_ChkBxA.UseVisualStyleBackColor = true;
            this.DualCheckBox_ChkBxA.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // ChkBx_No
            // 
            this.ChkBx_No.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ChkBx_No.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ChkBx_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.ChkBx_No.Location = new System.Drawing.Point(333, 4);
            this.ChkBx_No.Margin = new System.Windows.Forms.Padding(4);
            this.ChkBx_No.Name = "ChkBx_No";
            this.ChkBx_No.Size = new System.Drawing.Size(328, 78);
            this.ChkBx_No.TabIndex = 11;
            this.ChkBx_No.Text = "No";
            this.ChkBx_No.UseVisualStyleBackColor = true;
            this.ChkBx_No.Click += new System.EventHandler(this.ChkBx_No_Click);
            this.ChkBx_No.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ChkBx_No_KeyPress);
            this.ChkBx_No.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChkBx_No_KeyUp);
            // 
            // ChkBx_Yes
            // 
            this.ChkBx_Yes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ChkBx_Yes.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ChkBx_Yes.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.ChkBx_Yes.Location = new System.Drawing.Point(5, 4);
            this.ChkBx_Yes.Margin = new System.Windows.Forms.Padding(4);
            this.ChkBx_Yes.Name = "ChkBx_Yes";
            this.ChkBx_Yes.Size = new System.Drawing.Size(323, 78);
            this.ChkBx_Yes.TabIndex = 10;
            this.ChkBx_Yes.Text = "Yes";
            this.ChkBx_Yes.UseVisualStyleBackColor = true;
            this.ChkBx_Yes.Click += new System.EventHandler(this.ChkBx_Yes_Click);
            this.ChkBx_Yes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ChkBx_Yes_KeyPress);
            this.ChkBx_Yes.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChkBx_Yes_KeyUp);
            // 
            // DateTimePage
            // 
            this.DateTimePage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DateTimePage.Controls.Add(this.yearLabel);
            this.DateTimePage.Controls.Add(this.monthLabel);
            this.DateTimePage.Controls.Add(this.dayLabel);
            this.DateTimePage.Controls.Add(this.DateTime_YearSelector);
            this.DateTimePage.Controls.Add(this.DateTime_MonthSelector);
            this.DateTimePage.Controls.Add(this.DateTime_DaySelector);
            this.DateTimePage.Controls.Add(this.DateTime_TimeMinSelector);
            this.DateTimePage.Controls.Add(this.DateTime_TimeHourSelector);
            this.DateTimePage.Controls.Add(this.TimeSemicolon_Lbl);
            this.DateTimePage.Controls.Add(this.Date_NowBtn);
            this.DateTimePage.Controls.Add(this.DateTime_TimeAMPMSelector);
            this.DateTimePage.Controls.Add(this.Time_NowBtn);
            this.DateTimePage.Location = new System.Drawing.Point(4, 27);
            this.DateTimePage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTimePage.Name = "DateTimePage";
            this.DateTimePage.Size = new System.Drawing.Size(667, 86);
            this.DateTimePage.TabIndex = 5;
            this.DateTimePage.Text = "DateTime";
            // 
            // yearLabel
            // 
            this.yearLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.yearLabel.AutoSize = true;
            this.yearLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.yearLabel.Location = new System.Drawing.Point(199, 66);
            this.yearLabel.Name = "yearLabel";
            this.yearLabel.Size = new System.Drawing.Size(35, 15);
            this.yearLabel.TabIndex = 33;
            this.yearLabel.Text = "YYYY";
            // 
            // monthLabel
            // 
            this.monthLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.monthLabel.AutoSize = true;
            this.monthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.monthLabel.Location = new System.Drawing.Point(83, 66);
            this.monthLabel.Name = "monthLabel";
            this.monthLabel.Size = new System.Drawing.Size(40, 15);
            this.monthLabel.TabIndex = 32;
            this.monthLabel.Text = "MMM";
            // 
            // dayLabel
            // 
            this.dayLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dayLabel.AutoSize = true;
            this.dayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.dayLabel.Location = new System.Drawing.Point(3, 66);
            this.dayLabel.Name = "dayLabel";
            this.dayLabel.Size = new System.Drawing.Size(25, 15);
            this.dayLabel.TabIndex = 31;
            this.dayLabel.Text = "DD";
            // 
            // DateTime_YearSelector
            // 
            this.DateTime_YearSelector.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DateTime_YearSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DateTime_YearSelector.Location = new System.Drawing.Point(187, 7);
            this.DateTime_YearSelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTime_YearSelector.Name = "DateTime_YearSelector";
            this.DateTime_YearSelector.Size = new System.Drawing.Size(113, 55);
            this.DateTime_YearSelector.TabIndex = 27;
            this.DateTime_YearSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DateTime_YearSelector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime_YearSelector_KeyPress);
            this.DateTime_YearSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DateTime_YearSelector_KeyUp);
            this.DateTime_YearSelector.Validated += new System.EventHandler(this.DateTime_YearSelector_Validated);
            // 
            // DateTime_MonthSelector
            // 
            this.DateTime_MonthSelector.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DateTime_MonthSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DateTime_MonthSelector.Location = new System.Drawing.Point(81, 7);
            this.DateTime_MonthSelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTime_MonthSelector.Name = "DateTime_MonthSelector";
            this.DateTime_MonthSelector.Size = new System.Drawing.Size(105, 55);
            this.DateTime_MonthSelector.TabIndex = 26;
            this.DateTime_MonthSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DateTime_MonthSelector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime_MonthSelector_KeyPress);
            this.DateTime_MonthSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DateTime_MonthSelector_KeyUp);
            this.DateTime_MonthSelector.Validated += new System.EventHandler(this.DateTime_MonthSelector_Validated);
            // 
            // DateTime_DaySelector
            // 
            this.DateTime_DaySelector.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DateTime_DaySelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DateTime_DaySelector.Location = new System.Drawing.Point(3, 7);
            this.DateTime_DaySelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTime_DaySelector.Name = "DateTime_DaySelector";
            this.DateTime_DaySelector.Size = new System.Drawing.Size(79, 55);
            this.DateTime_DaySelector.TabIndex = 25;
            this.DateTime_DaySelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DateTime_DaySelector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime_DaySelector_KeyPress);
            this.DateTime_DaySelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DateTime_DaySelector_KeyUp);
            this.DateTime_DaySelector.Validated += new System.EventHandler(this.DateTime_DaySelector_Validated);
            // 
            // DateTime_TimeMinSelector
            // 
            this.DateTime_TimeMinSelector.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DateTime_TimeMinSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DateTime_TimeMinSelector.Location = new System.Drawing.Point(441, 7);
            this.DateTime_TimeMinSelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTime_TimeMinSelector.Name = "DateTime_TimeMinSelector";
            this.DateTime_TimeMinSelector.Size = new System.Drawing.Size(59, 55);
            this.DateTime_TimeMinSelector.TabIndex = 29;
            this.DateTime_TimeMinSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DateTime_TimeMinSelector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime_TimeMinSelector_KeyPress);
            this.DateTime_TimeMinSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DateTime_TimeMinSelector_KeyUp);
            this.DateTime_TimeMinSelector.Validated += new System.EventHandler(this.DateTime_TimeMinSelector_Validated);
            // 
            // DateTime_TimeHourSelector
            // 
            this.DateTime_TimeHourSelector.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DateTime_TimeHourSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DateTime_TimeHourSelector.Location = new System.Drawing.Point(375, 7);
            this.DateTime_TimeHourSelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTime_TimeHourSelector.Name = "DateTime_TimeHourSelector";
            this.DateTime_TimeHourSelector.Size = new System.Drawing.Size(59, 55);
            this.DateTime_TimeHourSelector.TabIndex = 28;
            this.DateTime_TimeHourSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DateTime_TimeHourSelector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime_TimeHourSelector_KeyPress);
            this.DateTime_TimeHourSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DateTime_TimeHourSelector_KeyUp);
            this.DateTime_TimeHourSelector.Validated += new System.EventHandler(this.DateTime_TimeHourSelector_Validated);
            // 
            // TimeSemicolon_Lbl
            // 
            this.TimeSemicolon_Lbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.TimeSemicolon_Lbl.AutoSize = true;
            this.TimeSemicolon_Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.TimeSemicolon_Lbl.Location = new System.Drawing.Point(423, 11);
            this.TimeSemicolon_Lbl.Name = "TimeSemicolon_Lbl";
            this.TimeSemicolon_Lbl.Size = new System.Drawing.Size(32, 48);
            this.TimeSemicolon_Lbl.TabIndex = 22;
            this.TimeSemicolon_Lbl.Text = ":";
            // 
            // Date_NowBtn
            // 
            this.Date_NowBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Date_NowBtn.AutoSize = true;
            this.Date_NowBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Date_NowBtn.Location = new System.Drawing.Point(300, 7);
            this.Date_NowBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Date_NowBtn.Name = "Date_NowBtn";
            this.Date_NowBtn.Size = new System.Drawing.Size(88, 57);
            this.Date_NowBtn.TabIndex = 19;
            this.Date_NowBtn.TabStop = false;
            this.Date_NowBtn.Text = "Now";
            this.Date_NowBtn.UseVisualStyleBackColor = true;
            this.Date_NowBtn.Click += new System.EventHandler(this.Date_NowBtn_Click_1);
            // 
            // DateTime_TimeAMPMSelector
            // 
            this.DateTime_TimeAMPMSelector.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DateTime_TimeAMPMSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DateTime_TimeAMPMSelector.Items.Add("AM");
            this.DateTime_TimeAMPMSelector.Items.Add("PM");
            this.DateTime_TimeAMPMSelector.Location = new System.Drawing.Point(500, 7);
            this.DateTime_TimeAMPMSelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateTime_TimeAMPMSelector.Name = "DateTime_TimeAMPMSelector";
            this.DateTime_TimeAMPMSelector.Size = new System.Drawing.Size(93, 55);
            this.DateTime_TimeAMPMSelector.TabIndex = 30;
            this.DateTime_TimeAMPMSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DateTime_TimeAMPMSelector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTime_TimeAMPMSelector_KeyPress);
            this.DateTime_TimeAMPMSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DateTime_TimeAMPMSelector_KeyUp);
            this.DateTime_TimeAMPMSelector.Validated += new System.EventHandler(this.DateTime_TimeAMPMSelector_Validated);
            // 
            // Time_NowBtn
            // 
            this.Time_NowBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Time_NowBtn.AutoSize = true;
            this.Time_NowBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Time_NowBtn.Location = new System.Drawing.Point(595, 7);
            this.Time_NowBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Time_NowBtn.Name = "Time_NowBtn";
            this.Time_NowBtn.Size = new System.Drawing.Size(88, 57);
            this.Time_NowBtn.TabIndex = 13;
            this.Time_NowBtn.TabStop = false;
            this.Time_NowBtn.Text = "Now";
            this.Time_NowBtn.UseVisualStyleBackColor = true;
            this.Time_NowBtn.Click += new System.EventHandler(this.Time_NowBtn_Click);
            // 
            // CamPage
            // 
            this.CamPage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.CamPage.Controls.Add(this.ActivateCamBtn);
            this.CamPage.Location = new System.Drawing.Point(4, 27);
            this.CamPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CamPage.Name = "CamPage";
            this.CamPage.Size = new System.Drawing.Size(667, 86);
            this.CamPage.TabIndex = 6;
            this.CamPage.Text = "Camera Request";
            // 
            // ActivateCamBtn
            // 
            this.ActivateCamBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ActivateCamBtn.BackColor = System.Drawing.SystemColors.Control;
            this.ActivateCamBtn.BackgroundImage = global::TravellerAuto.Properties.Resources.CamBtnIcon11;
            this.ActivateCamBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ActivateCamBtn.Location = new System.Drawing.Point(371, 6);
            this.ActivateCamBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ActivateCamBtn.Name = "ActivateCamBtn";
            this.ActivateCamBtn.Size = new System.Drawing.Size(96, 69);
            this.ActivateCamBtn.TabIndex = 7;
            this.ActivateCamBtn.UseVisualStyleBackColor = false;
            this.ActivateCamBtn.Click += new System.EventHandler(this.ActivateCamBtn_Click);
            // 
            // ErrorPage
            // 
            this.ErrorPage.BackColor = System.Drawing.Color.Brown;
            this.ErrorPage.Controls.Add(this.ErrorMsgLbl);
            this.ErrorPage.Location = new System.Drawing.Point(4, 27);
            this.ErrorPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ErrorPage.Name = "ErrorPage";
            this.ErrorPage.Size = new System.Drawing.Size(667, 86);
            this.ErrorPage.TabIndex = 7;
            this.ErrorPage.Text = "Error Page";
            // 
            // ErrorMsgLbl
            // 
            this.ErrorMsgLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorMsgLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.ErrorMsgLbl.Location = new System.Drawing.Point(0, 0);
            this.ErrorMsgLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.ErrorMsgLbl.Name = "ErrorMsgLbl";
            this.ErrorMsgLbl.Padding = new System.Windows.Forms.Padding(5);
            this.ErrorMsgLbl.Size = new System.Drawing.Size(667, 86);
            this.ErrorMsgLbl.TabIndex = 0;
            this.ErrorMsgLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SubmitPromptBtn
            // 
            this.SubmitPromptBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubmitPromptBtn.BackgroundImage = global::TravellerAuto.Properties.Resources.submit;
            this.SubmitPromptBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.SubmitPromptBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.SubmitPromptBtn.Location = new System.Drawing.Point(948, 30);
            this.SubmitPromptBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SubmitPromptBtn.Name = "SubmitPromptBtn";
            this.SubmitPromptBtn.Size = new System.Drawing.Size(100, 100);
            this.SubmitPromptBtn.TabIndex = 2;
            this.SubmitPromptBtn.TabStop = false;
            this.SubmitPromptBtn.UseVisualStyleBackColor = true;
            this.SubmitPromptBtn.Click += new System.EventHandler(this.SubmitPromptBtn_Click);
            // 
            // NextBtn
            // 
            this.NextBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NextBtn.BackgroundImage = global::TravellerAuto.Properties.Resources.nxtprmptbtn;
            this.NextBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.NextBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.NextBtn.Location = new System.Drawing.Point(843, 30);
            this.NextBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Size = new System.Drawing.Size(100, 100);
            this.NextBtn.TabIndex = 5;
            this.NextBtn.TabStop = false;
            this.NextBtn.UseVisualStyleBackColor = true;
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // TravelerProgressBar
            // 
            this.TravelerProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TravelerProgressBar.Location = new System.Drawing.Point(5, 0);
            this.TravelerProgressBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TravelerProgressBar.Name = "TravelerProgressBar";
            this.TravelerProgressBar.Size = new System.Drawing.Size(1053, 14);
            this.TravelerProgressBar.TabIndex = 17;
            // 
            // customersComboBx
            // 
            this.customersComboBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customersComboBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customersComboBx.FormattingEnabled = true;
            this.customersComboBx.Location = new System.Drawing.Point(11, 58);
            this.customersComboBx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.customersComboBx.Name = "customersComboBx";
            this.customersComboBx.Size = new System.Drawing.Size(296, 56);
            this.customersComboBx.TabIndex = 18;
            this.customersComboBx.TabStop = false;
            this.customersComboBx.SelectedIndexChanged += new System.EventHandler(this.CustomersComboBx_SelectedIndexChanged);
            // 
            // travelersComboBx
            // 
            this.travelersComboBx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.travelersComboBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.travelersComboBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.travelersComboBx.FormattingEnabled = true;
            this.travelersComboBx.Location = new System.Drawing.Point(312, 58);
            this.travelersComboBx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.travelersComboBx.Name = "travelersComboBx";
            this.travelersComboBx.Size = new System.Drawing.Size(460, 56);
            this.travelersComboBx.TabIndex = 19;
            this.travelersComboBx.TabStop = false;
            // 
            // SN_Lbl
            // 
            this.SN_Lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SN_Lbl.AutoSize = true;
            this.SN_Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.SN_Lbl.Location = new System.Drawing.Point(779, 75);
            this.SN_Lbl.Name = "SN_Lbl";
            this.SN_Lbl.Size = new System.Drawing.Size(37, 20);
            this.SN_Lbl.TabIndex = 21;
            this.SN_Lbl.Text = "SN:";
            // 
            // SNComboBx
            // 
            this.SNComboBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SNComboBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SNComboBx.FormattingEnabled = true;
            this.SNComboBx.Location = new System.Drawing.Point(821, 58);
            this.SNComboBx.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SNComboBx.Name = "SNComboBx";
            this.SNComboBx.Size = new System.Drawing.Size(232, 56);
            this.SNComboBx.TabIndex = 23;
            this.SNComboBx.TabStop = false;
            this.SNComboBx.DropDown += new System.EventHandler(this.SNComboBx_DropDown);
            this.SNComboBx.SelectedIndexChanged += new System.EventHandler(this.SNComboBx_SelectedIndexChanged);
            this.SNComboBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SNComboBx_KeyPress);
            this.SNComboBx.Validated += new System.EventHandler(this.SNComboBx_Validated);
            // 
            // HelpDocument
            // 
            this.HelpDocument.Location = new System.Drawing.Point(621, 10);
            this.HelpDocument.Margin = new System.Windows.Forms.Padding(4);
            this.HelpDocument.Name = "HelpDocument";
            this.HelpDocument.Size = new System.Drawing.Size(51, 38);
            this.HelpDocument.TabIndex = 8;
            this.HelpDocument.TabStop = false;
            this.HelpDocument.Text = "?";
            this.HelpDocument.UseVisualStyleBackColor = true;
            this.HelpDocument.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // webView
            // 
            this.webView.AllowExternalDrop = true;
            this.webView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webView.CreationProperties = null;
            this.webView.DefaultBackgroundColor = System.Drawing.Color.White;
            this.webView.Location = new System.Drawing.Point(0, 118);
            this.webView.Margin = new System.Windows.Forms.Padding(4);
            this.webView.Name = "webView";
            this.webView.Size = new System.Drawing.Size(1069, 436);
            this.webView.TabIndex = 25;
            this.webView.TabStop = false;
            this.webView.ZoomFactor = 1D;
            this.webView.NavigationCompleted += new System.EventHandler<Microsoft.Web.WebView2.Core.CoreWebView2NavigationCompletedEventArgs>(this.WebView_NavigationCompleted);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 703);
            this.Controls.Add(this.webView);
            this.Controls.Add(this.HelpDocument);
            this.Controls.Add(this.SNComboBx);
            this.Controls.Add(this.SN_Lbl);
            this.Controls.Add(this.travelersComboBx);
            this.Controls.Add(this.customersComboBx);
            this.Controls.Add(this.PromptControlGroupBox);
            this.Controls.Add(this.TextBoxPanel);
            this.Controls.Add(this.activeUsrsLbl);
            this.Controls.Add(this.DateTimeLbl);
            this.Controls.Add(this.ActiveUsersLbl);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.Text = "TravelerAuto";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.TextBoxPanel.ResumeLayout(false);
            this.TextBoxPanel.PerformLayout();
            this.PromptControlGroupBox.ResumeLayout(false);
            this.PromptsTabControl.ResumeLayout(false);
            this.TxtBxPage.ResumeLayout(false);
            this.TxtBxPage.PerformLayout();
            this.DualChkBxPage.ResumeLayout(false);
            this.DualChkBxPage.PerformLayout();
            this.DateTimePage.ResumeLayout(false);
            this.DateTimePage.PerformLayout();
            this.CamPage.ResumeLayout(false);
            this.ErrorPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.webView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startNewTravelerToolStripMenuItem;
        private System.Windows.Forms.Label ActiveUsersLbl;
        private System.Windows.Forms.Label DateTimeLbl;
        private System.Windows.Forms.Button NextBtn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginUserToolStripMenuItem;
        private System.Windows.Forms.Label activeUsrsLbl;
        private System.Windows.Forms.Panel TextBoxPanel;
        private System.Windows.Forms.Button SubmitPromptBtn;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem promptControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promptRedactionToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox PromptControlGroupBox;
        private System.Windows.Forms.ProgressBar TravelerProgressBar;
        private System.Windows.Forms.ComboBox customersComboBx;
        private System.Windows.Forms.ComboBox travelersComboBx;
        private System.Windows.Forms.Label SN_Lbl;
        private System.Windows.Forms.ComboBox SNComboBx;
        private System.Windows.Forms.Button HelpDocument;
        private System.Windows.Forms.TabControl PromptsTabControl;
        private System.Windows.Forms.TabPage TxtBxPage;
        private System.Windows.Forms.TextBox TextBox;
        private System.Windows.Forms.TabPage DualChkBxPage;
        private System.Windows.Forms.TabPage DateTimePage;
        private System.Windows.Forms.Label TimeSemicolon_Lbl;
        private System.Windows.Forms.Button Date_NowBtn;
        private System.Windows.Forms.DomainUpDown DateTime_TimeAMPMSelector;
        private System.Windows.Forms.Button Time_NowBtn;
        private System.Windows.Forms.TabPage CamPage;
        private System.Windows.Forms.Button ActivateCamBtn;
        private System.Windows.Forms.TabPage ErrorPage;
        private System.Windows.Forms.TextBox DateTime_TimeMinSelector;
        private System.Windows.Forms.TextBox DateTime_TimeHourSelector;
        private System.Windows.Forms.Label ErrorMsgLbl;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.TextBox DateTime_YearSelector;
        private System.Windows.Forms.TextBox DateTime_MonthSelector;
        private System.Windows.Forms.TextBox DateTime_DaySelector;
        private System.Windows.Forms.Button ChkBx_No;
        private System.Windows.Forms.Button ChkBx_Yes;
        private System.Windows.Forms.CheckBox DualCheckBox_ChkBxA;
        private System.Windows.Forms.CheckBox DualCheckBox_ChkBxB;
        private Microsoft.Web.WebView2.WinForms.WebView2 webView;
        private System.Windows.Forms.Button BackBtn;
        private System.Windows.Forms.Label yearLabel;
        private System.Windows.Forms.Label monthLabel;
        private System.Windows.Forms.Label dayLabel;
        private System.Windows.Forms.ToolStripMenuItem currentTravelerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assemblyDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem travelerNotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveTravelerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem completedTravelersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeTravelerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promptLogicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
    }
}

