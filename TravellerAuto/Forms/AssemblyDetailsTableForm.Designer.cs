﻿namespace TravellerAuto
{
    partial class AssemblyDetailsTableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AssemblyDetailsGridView = new System.Windows.Forms.DataGridView();
            this.ProductNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNLotNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.update_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.AssemblyDetailsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // AssemblyDetailsGridView
            // 
            this.AssemblyDetailsGridView.AllowUserToAddRows = false;
            this.AssemblyDetailsGridView.AllowUserToDeleteRows = false;
            this.AssemblyDetailsGridView.AllowUserToResizeRows = false;
            this.AssemblyDetailsGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AssemblyDetailsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AssemblyDetailsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AssemblyDetailsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductNumber,
            this.Description,
            this.SNLotNum,
            this.Notes});
            this.AssemblyDetailsGridView.Location = new System.Drawing.Point(0, 0);
            this.AssemblyDetailsGridView.Margin = new System.Windows.Forms.Padding(2);
            this.AssemblyDetailsGridView.MultiSelect = false;
            this.AssemblyDetailsGridView.Name = "AssemblyDetailsGridView";
            this.AssemblyDetailsGridView.RowHeadersWidth = 51;
            this.AssemblyDetailsGridView.RowTemplate.Height = 40;
            this.AssemblyDetailsGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AssemblyDetailsGridView.Size = new System.Drawing.Size(810, 361);
            this.AssemblyDetailsGridView.TabIndex = 0;
            // 
            // ProductNumber
            // 
            this.ProductNumber.HeaderText = "P/N";
            this.ProductNumber.MinimumWidth = 6;
            this.ProductNumber.Name = "ProductNumber";
            this.ProductNumber.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.MinimumWidth = 6;
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // SNLotNum
            // 
            this.SNLotNum.HeaderText = "SN or Lot Number";
            this.SNLotNum.MinimumWidth = 6;
            this.SNLotNum.Name = "SNLotNum";
            // 
            // Notes
            // 
            this.Notes.HeaderText = "Notes";
            this.Notes.MinimumWidth = 6;
            this.Notes.Name = "Notes";
            // 
            // cancel_btn
            // 
            this.cancel_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancel_btn.AutoSize = true;
            this.cancel_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.cancel_btn.Location = new System.Drawing.Point(11, 365);
            this.cancel_btn.Margin = new System.Windows.Forms.Padding(2);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(313, 49);
            this.cancel_btn.TabIndex = 1;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // update_btn
            // 
            this.update_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.update_btn.AutoSize = true;
            this.update_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.update_btn.Location = new System.Drawing.Point(486, 365);
            this.update_btn.Margin = new System.Windows.Forms.Padding(2);
            this.update_btn.Name = "update_btn";
            this.update_btn.Size = new System.Drawing.Size(313, 49);
            this.update_btn.TabIndex = 2;
            this.update_btn.Text = "Update";
            this.update_btn.UseVisualStyleBackColor = true;
            this.update_btn.Click += new System.EventHandler(this.update_btn_Click);
            // 
            // AssemblyDetailsTableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 422);
            this.Controls.Add(this.update_btn);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.AssemblyDetailsGridView);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeBox = false;
            this.Name = "AssemblyDetailsTableForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AssemblyDetailsTableForm";
            ((System.ComponentModel.ISupportInitialize)(this.AssemblyDetailsGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView AssemblyDetailsGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNLotNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Notes;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Button update_btn;
    }
}