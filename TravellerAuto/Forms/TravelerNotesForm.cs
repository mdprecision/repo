﻿using System;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class TravelerNotesForm : Form
    {
        #region FormEvents
        public TravelerNotesForm()
        {
            InitializeComponent();
        }
        private void TravelerNotesForm_Load(object sender, EventArgs e)
        {
            notesText.Text = Traveler.TravelerObject.TravelerNotes;
        }
        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            Traveler.TravelerObject.TravelerNotes = notesText.Text;
            Traveler.TravelerObject.SaveTravelerData();

            HtmlMgr.ReloadNotes();
            Close();
        }
        #endregion
    }
}
