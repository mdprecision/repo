﻿namespace TravellerAuto
{
    partial class PromptLogicForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PromptLogicForm));
            this.PromptsLogicTable = new System.Windows.Forms.DataGridView();
            this.PromptsComboBox = new System.Windows.Forms.ComboBox();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PromptsLogicTable)).BeginInit();
            this.SuspendLayout();
            // 
            // PromptsLogicTable
            // 
            this.PromptsLogicTable.AllowUserToAddRows = false;
            this.PromptsLogicTable.AllowUserToDeleteRows = false;
            this.PromptsLogicTable.AllowUserToResizeRows = false;
            this.PromptsLogicTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PromptsLogicTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PromptsLogicTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PromptsLogicTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Description,
            this.Notes,
            this.Column1,
            this.Column3,
            this.Column2});
            this.PromptsLogicTable.Location = new System.Drawing.Point(0, 49);
            this.PromptsLogicTable.Margin = new System.Windows.Forms.Padding(2);
            this.PromptsLogicTable.MultiSelect = false;
            this.PromptsLogicTable.Name = "PromptsLogicTable";
            this.PromptsLogicTable.RowHeadersWidth = 51;
            this.PromptsLogicTable.RowTemplate.Height = 40;
            this.PromptsLogicTable.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PromptsLogicTable.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PromptsLogicTable.Size = new System.Drawing.Size(800, 392);
            this.PromptsLogicTable.TabIndex = 5;
            // 
            // PromptsComboBox
            // 
            this.PromptsComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.PromptsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PromptsComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PromptsComboBox.FormattingEnabled = true;
            this.PromptsComboBox.Location = new System.Drawing.Point(0, 0);
            this.PromptsComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.PromptsComboBox.Name = "PromptsComboBox";
            this.PromptsComboBox.Size = new System.Drawing.Size(800, 46);
            this.PromptsComboBox.TabIndex = 20;
            this.PromptsComboBox.SelectedIndexChanged += new System.EventHandler(this.PromptsComboBox_SelectedIndexChanged);
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Description.FillWeight = 71.61975F;
            this.Description.HeaderText = "Target Prompt";
            this.Description.MinimumWidth = 6;
            this.Description.Name = "Description";
            this.Description.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Description.Width = 113;
            // 
            // Notes
            // 
            this.Notes.FillWeight = 50F;
            this.Notes.HeaderText = "Operator";
            this.Notes.MinimumWidth = 6;
            this.Notes.Name = "Notes";
            this.Notes.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.FillWeight = 150F;
            this.Column1.HeaderText = "Compared Prompt/Value";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 300;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Offset";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column2.HeaderText = "Validity";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // PromptLogicForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PromptsComboBox);
            this.Controls.Add(this.PromptsLogicTable);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "PromptLogicForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PromptLogic";
            this.Load += new System.EventHandler(this.PromptLogicForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PromptsLogicTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView PromptsLogicTable;
        private System.Windows.Forms.ComboBox PromptsComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Notes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}