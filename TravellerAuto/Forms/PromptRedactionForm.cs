﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public partial class PromptRedactionForm : Form
    {
        #region FormEvents
        public PromptRedactionForm()
        {
            InitializeComponent();
        }
        private void PromptRedactionForm_Load(object sender, EventArgs e)
        {
            LoadTable();
        }
        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            //list of prompts changed for html update
            List<int> promptsToUpdate = new List<int>();

            for (int i = 0; i < Traveler.TravelerObject.ListOfUserPrompts.Count; i++)
            {   //check if the value was changed
                if (Traveler.TravelerObject.ListOfUserPrompts[i].CurrentValue != PromptRedactionTable.Rows[i].Cells[2].Value.ToString())
                {   //update the value if it was changed
                    Traveler.TravelerObject.ListOfUserPrompts[i].ValueCorrection(PromptRedactionTable.Rows[i].Cells[2].Value.ToString());
                    promptsToUpdate.Add(Traveler.TravelerObject.ListOfUserPrompts[i].Index);
                }
            }

            // check validity of group again
            int numberInvalid = Traveler.TravelerObject.FindNumberOfInvalidPrompts();
            if (numberInvalid == 0 && Traveler.ValidLogic)
            {
                if (Application.OpenForms["MainForm"] != null)
                {
                    (Application.OpenForms["MainForm"] as MainForm).LockTraveller(false);
                }
                //update current group details
                Traveler.TravelerObject.GroupDataList[Traveler.TravelerObject.CurrentGroupIndex].OperatorInitials = string.Join("\n", User.GetActiveUsers(Traveler.LoggedUsers));
                Traveler.TravelerObject.GroupDataList[Traveler.TravelerObject.CurrentGroupIndex].PassFail = "PASS";
                Traveler.TravelerObject.GroupDataList[Traveler.TravelerObject.CurrentGroupIndex].DateComplete = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                HtmlMgr.ReloadGroupDetails(Traveler.TravelerObject.CurrentGroupIndex);

                //update next group prompts
                Traveler.TravelerObject.SelectNextPrompt2(true);
                if (Application.OpenForms["MainForm"] is MainForm mainform)
                {
                    mainform.TabSwitch();
                }
                HtmlMgr.ReloadPrompts(Traveler.TravelerObject.PromptGroups[Traveler.TravelerObject.CurrentGroupIndex]);
            }

            //reload html for changed promtps
            using (Traveler.WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
            {
                HtmlMgr.ReloadPrompts(promptsToUpdate);
            }

            // save new prompt value and update the table with new values
            Traveler.TravelerObject.SaveTravelerData();
            LoadTable();
        }
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion
        #region RedactionFunctions
        private void LoadTable()
        {
            //loading all prompts into table
            PromptRedactionTable.Rows.Clear();
            foreach (PromptClasses.UserPrompt prompt in Traveler.TravelerObject.ListOfUserPrompts)
            {
                //validity string for converting integer value into string
                string[] validity = { "Unset", "Invalid", "Valid" };

                PromptRedactionTable.Rows.Add(prompt.PromptGroup, prompt.PromptName, prompt.CurrentValue, validity[prompt.Valid + 1]);
            }
        }
        #endregion
    }
}
