﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class RemoveUserForm : Form
    {
        #region Attributes
        public bool RemovedSuccess { get; set; } = false;

        #endregion
        #region FormEvents
        public RemoveUserForm()
        {
            InitializeComponent();
            RemoveButton.Text = RemoveButton.Text.Replace("????", Traveler.TravelerObject.TravelerSN.ToString().PadLeft(4, '0'));
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            RemoveUser();
        }
        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.RefreshUsers();
            }
        }
        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; RemoveUser(); }
        }
        private void UsernameBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; RemoveUser(); }
        }
        #endregion
        #region LoginFunctions
        private void RemoveUser()
        {
            if (!TravelerDatabase.UseDatabase || !TravelerDatabase.PingDB())
            {
                LoginStatusLbl.ForeColor = Color.Red;
                LoginStatusLbl.Text = "No Internet Connection";
                UsernameBox.Focus();
            }
            else
            {
                if (TravelerDatabase.GetNumberOfUsers(Traveler.TravelerObject.CustomerName, Traveler.TravelerObject.TemplateName,Traveler.TravelerObject.TravelerSN) == 1)
                {
                    LoginStatusLbl.ForeColor = Color.Red;
                    LoginStatusLbl.Text = "Cannot Remove Last User from Traveler";
                    UsernameBox.Focus();
                }
                else if (Traveler.TravelerObject.ActiveUsers
                    .Select(e => e.Username)
                    .Any(e => e.Equals(UsernameBox.Text)))
                {
                    int loginResult = TravelerDatabase.RemoveUser(UsernameBox.Text, PasswordBox.Text);
                    if (loginResult == 1)
                    {
                        LoginStatusLbl.ForeColor = Color.Green;
                        LoginStatusLbl.Text = "User Removed";
                        RemovedSuccess = true;
                        Close();
                    }
                    else if (loginResult == -2) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "Incorrect password, try again"; UsernameBox.Focus(); }
                    else if (loginResult == -3) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "Cannot Remove Admin Level Users"; UsernameBox.Focus(); }
                }
                else 
                { 
                    LoginStatusLbl.ForeColor = Color.Red;
                    LoginStatusLbl.Text = "User Not Active on Traveler, try again";
                    UsernameBox.Focus();
                }
            }
            UsernameBox.Text = PasswordBox.Text = string.Empty;
        }
        #endregion
    }
}
