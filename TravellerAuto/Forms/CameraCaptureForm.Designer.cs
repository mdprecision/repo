﻿namespace TravellerAuto
{
    partial class CameraCaptureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ActivateCamBtn = new System.Windows.Forms.Button();
            this.CameraDropDown = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(0, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(829, 702);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ActivateCamBtn
            // 
            this.ActivateCamBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ActivateCamBtn.BackColor = System.Drawing.SystemColors.Control;
            this.ActivateCamBtn.BackgroundImage = global::TravellerAuto.Properties.Resources.CamBtnIcon11;
            this.ActivateCamBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ActivateCamBtn.Location = new System.Drawing.Point(374, 690);
            this.ActivateCamBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ActivateCamBtn.Name = "ActivateCamBtn";
            this.ActivateCamBtn.Size = new System.Drawing.Size(82, 64);
            this.ActivateCamBtn.TabIndex = 8;
            this.ActivateCamBtn.UseVisualStyleBackColor = false;
            this.ActivateCamBtn.Click += new System.EventHandler(this.ActivateCamBtn_Click_1);
            // 
            // CameraDropDown
            // 
            this.CameraDropDown.Dock = System.Windows.Forms.DockStyle.Top;
            this.CameraDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CameraDropDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CameraDropDown.Location = new System.Drawing.Point(0, 0);
            this.CameraDropDown.Name = "CameraDropDown";
            this.CameraDropDown.Size = new System.Drawing.Size(829, 46);
            this.CameraDropDown.TabIndex = 0;
            this.CameraDropDown.SelectedIndexChanged += new System.EventHandler(this.CameraDropDown_SelectedIndexChanged);
            // 
            // CameraCaptureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 753);
            this.Controls.Add(this.CameraDropDown);
            this.Controls.Add(this.ActivateCamBtn);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimizeBox = false;
            this.Name = "CameraCaptureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraCaptureForm_FormClosing);
            this.Load += new System.EventHandler(this.CameraCaptureForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ActivateCamBtn;
        private System.Windows.Forms.ComboBox CameraDropDown;
    }
}