﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public partial class NewTravelerForm : Form
    {
        #region Attributes
        public string CustomerPath { get; set; } = PathMgr.CustomersDBPath;
        public string TravelerPath { get; set; } = string.Empty;
        public string Customer { get; set; } = string.Empty;
        public string TravelerName { get; set; } = string.Empty;
        public string SN { get; set; } = string.Empty;
        #endregion
        #region FormEvents
        public NewTravelerForm()
        {
            InitializeComponent();
        }
        private void NewTravelerForm_Load(object sender, EventArgs e)
        {
            //populating customer combo box with all customer folders
            List<string> customers = PathMgr.GetCustomerDirNames();
            foreach (string customer in customers)
            {
                string custName = Path.GetFileName(customer);
                CustomerCombBx.Items.Add(custName);
            }
        }
        private void CustomerCombBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            //loads templates combo box
            TemplatesListBox.Items.Clear();
            if (CustomerCombBx.SelectedItem != null || CustomerCombBx.Text != "Select Customer")
            {
                CustomerPath = Path.Combine(PathMgr.CustomersDBPath, CustomerCombBx.Text);

                // sorting templates by last edited
                DirectoryInfo di = new DirectoryInfo(CustomerPath);
                FileInfo[] files = di.GetFiles(@"*.docx*", SearchOption.AllDirectories);
                Array.Sort(files, (x, y) => Comparer<DateTime>.Default.Compare(x.LastWriteTime, y.LastWriteTime));
                Array.Reverse(files);

                Customer = CustomerCombBx.Text;
                foreach (FileInfo template in files)
                {
                    TemplatesListBox.Items.Add(template.ToString().Substring(0, template.ToString().Length - 5));
                }
                TemplatesListBox.Enabled = true;
            }
            else
            {
                TemplatesListBox.Enabled = false;
            }
        }
        private void SN_TxtBx_TextChanged(object sender, EventArgs e)
        {
            if (SN_TxtBx.Text != string.Empty)
            {
                open_btn.Enabled = true;
            }
            else
            {
                open_btn.Enabled = false;
            }
        }
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void open_btn_Click(object sender, EventArgs e)
        {
            Open();
        }
        private void TemplatesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            TravelerPath = Path.Combine(CustomerPath, TemplatesListBox.Text);
            if (TemplatesListBox.SelectedItem != null)
            {
                SN_TxtBx.Enabled = true;
                TravelerName = TemplatesListBox.Text;
            }
            else
            {
                SN_TxtBx.Enabled = false;
            }
        }
        #endregion
        #region NewTraveler Functions
        private void Open()
        {
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB() && TravelerDatabase.GetMaxAuthority(Customer,TravelerName) < 1)
            {
                MessageBox.Show($"Requires authority Trained(1) or greater for '{Customer} - {TravelerName}' to be created");
            }
            else
            {
                // rules for SN number Iteger <=4 digits
                if (!int.TryParse(SN_TxtBx.Text, out _) || SN_TxtBx.Text.Length > 4)
                {
                    MessageBox.Show("Invalid SN (Must be less than 4 integers)");
                }
                else
                {
                    string sn = SN_TxtBx.Text.PadLeft(4, '0');
                    TravelerPath = Path.Combine(TravelerPath, "Existing Travelers");
                    if (!File.Exists(Path.Combine(PathMgr.GetExistingTrvlrDir(Customer, TravelerName), sn + ".xml")))
                    {
                        SN = sn;
                        DialogResult = DialogResult.OK;
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("A traveler with this serial number already exists. \nPlease try another serial number");
                    }
                }
            }
        }
        #endregion
        #region Keybind FormEvents
        private void SN_TxtBx_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                Open();
            }
        }
        #endregion
    }
}
