﻿namespace TravellerAuto
{
    partial class PromptRedactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.PromptRedactionTable = new System.Windows.Forms.DataGridView();
            this.ProductNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNLotNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PromptRedactionTable)).BeginInit();
            this.SuspendLayout();
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateBtn.AutoSize = true;
            this.UpdateBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.UpdateBtn.Location = new System.Drawing.Point(476, 390);
            this.UpdateBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(313, 49);
            this.UpdateBtn.TabIndex = 3;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelBtn.AutoSize = true;
            this.CancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.CancelBtn.Location = new System.Drawing.Point(11, 390);
            this.CancelBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(313, 49);
            this.CancelBtn.TabIndex = 4;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // PromptRedactionTable
            // 
            this.PromptRedactionTable.AllowUserToAddRows = false;
            this.PromptRedactionTable.AllowUserToDeleteRows = false;
            this.PromptRedactionTable.AllowUserToResizeRows = false;
            this.PromptRedactionTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PromptRedactionTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PromptRedactionTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductNumber,
            this.Description,
            this.SNLotNum,
            this.Notes});
            this.PromptRedactionTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.PromptRedactionTable.Location = new System.Drawing.Point(0, 0);
            this.PromptRedactionTable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PromptRedactionTable.MultiSelect = false;
            this.PromptRedactionTable.Name = "PromptRedactionTable";
            this.PromptRedactionTable.RowHeadersWidth = 51;
            this.PromptRedactionTable.RowTemplate.Height = 40;
            this.PromptRedactionTable.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PromptRedactionTable.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PromptRedactionTable.Size = new System.Drawing.Size(800, 386);
            this.PromptRedactionTable.TabIndex = 5;
            // 
            // ProductNumber
            // 
            this.ProductNumber.FillWeight = 10.17495F;
            this.ProductNumber.HeaderText = "Group";
            this.ProductNumber.MinimumWidth = 6;
            this.ProductNumber.Name = "ProductNumber";
            this.ProductNumber.ReadOnly = true;
            this.ProductNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Description
            // 
            this.Description.FillWeight = 25F;
            this.Description.HeaderText = "Step Name";
            this.Description.MinimumWidth = 6;
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // SNLotNum
            // 
            this.SNLotNum.FillWeight = 50.87475F;
            this.SNLotNum.HeaderText = "Current Value";
            this.SNLotNum.MinimumWidth = 6;
            this.SNLotNum.Name = "SNLotNum";
            this.SNLotNum.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Notes
            // 
            this.Notes.FillWeight = 25F;
            this.Notes.HeaderText = "Status";
            this.Notes.MinimumWidth = 6;
            this.Notes.Name = "Notes";
            this.Notes.ReadOnly = true;
            this.Notes.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // PromptRedactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PromptRedactionTable);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.UpdateBtn);
            this.MinimizeBox = false;
            this.Name = "PromptRedactionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PromptRedacationForm";
            this.Load += new System.EventHandler(this.PromptRedactionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PromptRedactionTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.DataGridView PromptRedactionTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNLotNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Notes;
    }
}