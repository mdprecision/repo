﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public partial class CompletedTravelersForm : Form
    {
        #region Attributes
        Dictionary<int, string> CompletedTravelers { get; set; } = new Dictionary<int, string>();
        #endregion
        #region Form Events
        public CompletedTravelersForm()
        {
            InitializeComponent();
        }
        private void CompletedTravelersForm_Load(object sender, EventArgs e)
        {
            // getting completed travelers from the database, and adding them to the combo list
            CompletedTravelers = TravelerDatabase.GetCompletedTravelers();
            foreach (KeyValuePair<int, string> SN in CompletedTravelers)
            {
                CompletedTravelersList.Items.Add(SN.Key.ToString().PadLeft(4, '0'));
            }
            Text = $"{CompletedTravelers.Count} Completed Travelers Found For {Traveler.TravelerObject.CustomerName}-{Traveler.TravelerObject.TemplateName}";

            // saving scroll position of current traveler so we can scroll here on the completed traveler for references USER QOL/UX
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                _ = mainform.SaveScroll(false);
            }
        }

        private async void CompletedTravelersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CompletedTravelersList.SelectedItem != null)
            {
                // wait for webview to load
                await webView21.EnsureCoreWebView2Async();

                //path pointing to temporary html file(note that we do this in order to use local images)
                string cachePath = Path.Combine(Path.Combine(PathMgr.HtmlCachePth, "CompletedTravelers"), $"{CompletedTravelersList.SelectedItem}.html");

                //writing to cahce file and setting new source
                File.WriteAllText(cachePath, CompletedTravelers[int.Parse(CompletedTravelersList.SelectedItem.ToString())]);
                webView21.Source = new Uri(cachePath);
            }
        }

        private async void WebView21_NavigationCompleted(object sender, Microsoft.Web.WebView2.Core.CoreWebView2NavigationCompletedEventArgs e)
        {
            // scrolling to same position as traveler
            await webView21.ExecuteScriptAsync($"window.scrollTo({{top: {Traveler.TravelerObject.ScrollPosition}, behavior:'smooth'}});");
        }
        #endregion
    }
}
