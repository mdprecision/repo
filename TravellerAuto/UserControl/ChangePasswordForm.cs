﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class ChangePasswordForm: Form
    {
        #region Attributes
        public bool PasswordUpdated { get; set; } = false;
        User UserToUpdate { get; set; }
        #endregion
        #region FormEvents
        public ChangePasswordForm(User user)
        {
            InitializeComponent();
            UserToUpdate = user;
            Text = Text.Replace("????", UserToUpdate.Username);
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            ChangePassword();
        }
        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; ChangePassword(); }
        }
        private void NewPasswordBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; ChangePassword(); }
        }
        private void OldPasswordBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; ChangePassword(); }
        }
        #endregion
        #region LoginFunctions
        private void ChangePassword()
        {
            if (!TravelerDatabase.UseDatabase || !TravelerDatabase.PingDB())
            {
                LoginStatusLbl.ForeColor = Color.Red;
                LoginStatusLbl.Text = "No Internet Connection";
                OldPasswordBox.Focus();
            }

            if (User.ConfirmPassword(OldPasswordBox.Text, UserToUpdate.Salt, UserToUpdate.PasswordHash) 
                && NewPasswordBox.Text!= string.Empty 
                && NewPasswordBox.Text == NewPasswordBox2.Text)
            {
                TravelerDatabase.UpdatePassword(UserToUpdate, NewPasswordBox.Text);
                LoginStatusLbl.ForeColor = Color.Green;
                LoginStatusLbl.Text = "Password Changed";
                Close();
            }
            else
            {
                LoginStatusLbl.ForeColor = Color.Red;
                LoginStatusLbl.Text = "Incorrect password, try again";
                OldPasswordBox.Focus();
            }

            OldPasswordBox.Text = string.Empty;
            NewPasswordBox.Text = string.Empty;
            NewPasswordBox2.Text = string.Empty;
        }
        #endregion


    }
}
