﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class AdminConfirmForm : Form
    {
        #region Attributes
        public bool AdminConfirm { get; set; } = false;
        #endregion
        #region FormEvents
        public AdminConfirmForm()
        {
            InitializeComponent();
            LoginStatusLbl.ForeColor = Color.Blue;
            LoginStatusLbl.Text = "Enter Admin Password";
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            ConfirmAdmin();
        }
        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) { e.Handled = true; ConfirmAdmin(); }
        }
        #endregion
        #region LoginFunctions
        private void ConfirmAdmin()
        {
            int userExistsResult = User.UserExists("admin");
            if (userExistsResult == 1)
            {
                User admin = Traveler.LoggedUsers.First(e => e.Username.Equals("admin"));
                if (TravelerDatabase.ConfirmPasswordDB(PasswordBox.Text, admin))
                {
                    LoginStatusLbl.ForeColor = Color.Green;
                    LoginStatusLbl.Text = "Logged in successfully";
                    AdminConfirm = true;
                    Close();
                }
                else
                { 
                    LoginStatusLbl.ForeColor = Color.Red;
                    LoginStatusLbl.Text = "Incorrect password, try again";
                    PasswordBox.Focus();
                }
            }
            else if (userExistsResult == -1) { LoginStatusLbl.ForeColor = Color.Red; LoginStatusLbl.Text = "No Internet Connection"; PasswordBox.Focus(); }
            PasswordBox.Text = string.Empty;
        }
        #endregion
    }
}
