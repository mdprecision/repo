﻿namespace TravellerAuto
{
    partial class AdminUsersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsersListBox = new System.Windows.Forms.ListBox();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.AuthLabel = new System.Windows.Forms.Label();
            this.AuthList = new System.Windows.Forms.ComboBox();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.UsernameUser = new System.Windows.Forms.Label();
            this.EmailLabelUser = new System.Windows.Forms.Label();
            this.EmailLabel = new System.Windows.Forms.Label();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.CreateUserButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.UserIDLabel = new System.Windows.Forms.Label();
            this.UserIDUser = new System.Windows.Forms.Label();
            this.RevertButton = new System.Windows.Forms.Button();
            this.DeleteUserButton = new System.Windows.Forms.Button();
            this.TemplateAuthList = new System.Windows.Forms.ComboBox();
            this.TemplateAuthLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UsersListBox
            // 
            this.UsersListBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.UsersListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsersListBox.FormattingEnabled = true;
            this.UsersListBox.ItemHeight = 29;
            this.UsersListBox.Location = new System.Drawing.Point(0, 0);
            this.UsersListBox.Name = "UsersListBox";
            this.UsersListBox.Size = new System.Drawing.Size(201, 248);
            this.UsersListBox.TabIndex = 0;
            this.UsersListBox.TabStop = false;
            this.UsersListBox.SelectedIndexChanged += new System.EventHandler(this.UsersListBox_SelectedIndexChanged);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ApplyButton.Location = new System.Drawing.Point(593, 185);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(197, 51);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.TabStop = false;
            this.ApplyButton.Text = "Apply Changes";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // AuthLabel
            // 
            this.AuthLabel.AutoSize = true;
            this.AuthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AuthLabel.Location = new System.Drawing.Point(208, 13);
            this.AuthLabel.Name = "AuthLabel";
            this.AuthLabel.Size = new System.Drawing.Size(186, 29);
            this.AuthLabel.TabIndex = 3;
            this.AuthLabel.Text = "Default Authority";
            // 
            // AuthList
            // 
            this.AuthList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AuthList.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AuthList.FormattingEnabled = true;
            this.AuthList.Items.AddRange(new object[] {
            "Trainee (0)",
            "Trained (1)",
            "Admin (2)"});
            this.AuthList.Location = new System.Drawing.Point(400, 10);
            this.AuthList.Name = "AuthList";
            this.AuthList.Size = new System.Drawing.Size(187, 37);
            this.AuthList.TabIndex = 4;
            this.AuthList.TabStop = false;
            this.AuthList.DropDownClosed += new System.EventHandler(this.AuthList_DropDownClosed);
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameLabel.Location = new System.Drawing.Point(208, 90);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(130, 29);
            this.UsernameLabel.TabIndex = 5;
            this.UsernameLabel.Text = "Username:";
            // 
            // UsernameUser
            // 
            this.UsernameUser.AutoSize = true;
            this.UsernameUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameUser.Location = new System.Drawing.Point(338, 90);
            this.UsernameUser.Name = "UsernameUser";
            this.UsernameUser.Size = new System.Drawing.Size(0, 29);
            this.UsernameUser.TabIndex = 6;
            // 
            // EmailLabelUser
            // 
            this.EmailLabelUser.AutoSize = true;
            this.EmailLabelUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLabelUser.Location = new System.Drawing.Point(294, 119);
            this.EmailLabelUser.Name = "EmailLabelUser";
            this.EmailLabelUser.Size = new System.Drawing.Size(0, 29);
            this.EmailLabelUser.TabIndex = 8;
            // 
            // EmailLabel
            // 
            this.EmailLabel.AutoSize = true;
            this.EmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLabel.Location = new System.Drawing.Point(208, 119);
            this.EmailLabel.Name = "EmailLabel";
            this.EmailLabel.Size = new System.Drawing.Size(80, 29);
            this.EmailLabel.TabIndex = 7;
            this.EmailLabel.Text = "Email:";
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChangePasswordButton.Location = new System.Drawing.Point(207, 185);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(246, 51);
            this.ChangePasswordButton.TabIndex = 9;
            this.ChangePasswordButton.TabStop = false;
            this.ChangePasswordButton.Text = "Change Password";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePasswordButton_Click);
            // 
            // CreateUserButton
            // 
            this.CreateUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateUserButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateUserButton.Location = new System.Drawing.Point(593, 11);
            this.CreateUserButton.Name = "CreateUserButton";
            this.CreateUserButton.Size = new System.Drawing.Size(197, 51);
            this.CreateUserButton.TabIndex = 10;
            this.CreateUserButton.TabStop = false;
            this.CreateUserButton.Text = "Create User";
            this.CreateUserButton.UseVisualStyleBackColor = true;
            this.CreateUserButton.Click += new System.EventHandler(this.CreateUserButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(627, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 29);
            this.label1.TabIndex = 12;
            // 
            // UserIDLabel
            // 
            this.UserIDLabel.AutoSize = true;
            this.UserIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserIDLabel.Location = new System.Drawing.Point(208, 148);
            this.UserIDLabel.Name = "UserIDLabel";
            this.UserIDLabel.Size = new System.Drawing.Size(93, 29);
            this.UserIDLabel.TabIndex = 11;
            this.UserIDLabel.Text = "UserID:";
            // 
            // UserIDUser
            // 
            this.UserIDUser.AutoSize = true;
            this.UserIDUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserIDUser.Location = new System.Drawing.Point(307, 148);
            this.UserIDUser.Name = "UserIDUser";
            this.UserIDUser.Size = new System.Drawing.Size(0, 29);
            this.UserIDUser.TabIndex = 13;
            // 
            // RevertButton
            // 
            this.RevertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RevertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RevertButton.Location = new System.Drawing.Point(593, 128);
            this.RevertButton.Name = "RevertButton";
            this.RevertButton.Size = new System.Drawing.Size(197, 51);
            this.RevertButton.TabIndex = 14;
            this.RevertButton.TabStop = false;
            this.RevertButton.Text = "Revert Changes";
            this.RevertButton.UseVisualStyleBackColor = true;
            this.RevertButton.Click += new System.EventHandler(this.RevertButton_Click);
            // 
            // DeleteUserButton
            // 
            this.DeleteUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteUserButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteUserButton.Location = new System.Drawing.Point(593, 68);
            this.DeleteUserButton.Name = "DeleteUserButton";
            this.DeleteUserButton.Size = new System.Drawing.Size(197, 51);
            this.DeleteUserButton.TabIndex = 15;
            this.DeleteUserButton.TabStop = false;
            this.DeleteUserButton.Text = "Delete User";
            this.DeleteUserButton.UseVisualStyleBackColor = true;
            this.DeleteUserButton.Click += new System.EventHandler(this.DeleteUserButton_Click);
            // 
            // TemplateAuthList
            // 
            this.TemplateAuthList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TemplateAuthList.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TemplateAuthList.FormattingEnabled = true;
            this.TemplateAuthList.Items.AddRange(new object[] {
            "Trainee (0)",
            "Trained (1)",
            "Admin (2)"});
            this.TemplateAuthList.Location = new System.Drawing.Point(400, 48);
            this.TemplateAuthList.Name = "TemplateAuthList";
            this.TemplateAuthList.Size = new System.Drawing.Size(187, 37);
            this.TemplateAuthList.TabIndex = 17;
            this.TemplateAuthList.TabStop = false;
            this.TemplateAuthList.DropDownClosed += new System.EventHandler(this.TemplateAuthButton_DropDownClosed);
            // 
            // TemplateAuthLabel
            // 
            this.TemplateAuthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TemplateAuthLabel.Location = new System.Drawing.Point(209, 42);
            this.TemplateAuthLabel.Name = "TemplateAuthLabel";
            this.TemplateAuthLabel.Size = new System.Drawing.Size(185, 51);
            this.TemplateAuthLabel.TabIndex = 16;
            this.TemplateAuthLabel.Text = "???? Authority";
            // 
            // AdminUsersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 248);
            this.Controls.Add(this.TemplateAuthList);
            this.Controls.Add(this.TemplateAuthLabel);
            this.Controls.Add(this.DeleteUserButton);
            this.Controls.Add(this.RevertButton);
            this.Controls.Add(this.UserIDUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UserIDLabel);
            this.Controls.Add(this.CreateUserButton);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.EmailLabelUser);
            this.Controls.Add(this.EmailLabel);
            this.Controls.Add(this.UsernameUser);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.AuthList);
            this.Controls.Add(this.AuthLabel);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.UsersListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminUsersForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Users";
            this.Load += new System.EventHandler(this.AdminUsersForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox UsersListBox;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Label AuthLabel;
        private System.Windows.Forms.ComboBox AuthList;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label UsernameUser;
        private System.Windows.Forms.Label EmailLabelUser;
        private System.Windows.Forms.Label EmailLabel;
        private System.Windows.Forms.Button ChangePasswordButton;
        private System.Windows.Forms.Button CreateUserButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label UserIDLabel;
        private System.Windows.Forms.Label UserIDUser;
        private System.Windows.Forms.Button RevertButton;
        private System.Windows.Forms.Button DeleteUserButton;
        private System.Windows.Forms.ComboBox TemplateAuthList;
        private System.Windows.Forms.Label TemplateAuthLabel;
    }
}