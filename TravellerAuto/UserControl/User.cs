﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace TravellerAuto
{
    [Serializable]
    public class User
    {
        #region Attributes
        public static List<User> HardcodedUsers { get; set; } = null;
        public string Username { get; set; }
        public int ID { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] Salt { get; set; }
        public int DefaultAuthority { get; set; }
        public int TemplateAuthority { get; set; }
        #endregion
        #region Constructors
        public User() { }
        public User(string name, int id, string email, byte[] hash, byte[] salt, int auth, int templateauth)
        {
            Username = name;
            ID = id;
            Email = email;
            PasswordHash = hash;
            Salt = salt;
            DefaultAuthority = auth;
            TemplateAuthority = templateauth;
        }
        public User(string name, int id, string email, byte[] hash, byte[] salt, int auth)
        {
            Username = name;
            ID = id;
            Email = email;
            PasswordHash = hash;
            Salt = salt;
            DefaultAuthority = auth;
            TemplateAuthority = auth;
        }
        public User (User user)
        {
            Username = user.Username;
            ID = user.ID;
            Email = user.Email;
            PasswordHash = user.PasswordHash;
            Salt = user.Salt;
            DefaultAuthority = user.DefaultAuthority;
            TemplateAuthority = DefaultAuthority;
        }
        #endregion
        #region Login/Logout functions
        /// <summary>
        /// Login first uses GetUser(username), if no user is returned Login returns false.
        /// If GetUser(username) does return a user, Login checks that the input for password
        /// matches the users password attribute, if it does, the user is appended to ActiveUsers;
        /// if it doesn't Login returns false.
        /// </summary>
        public static int Login(string username, string password)
        {
            foreach (User existingUser in Traveler.LoggedUsers)
            {
                if (existingUser.Username == username)
                {
                    return -1;   // already exists
                }
            }
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
            {
                return TravelerDatabase.LoginDB(username, password);
            }
            else
            {
                User user = GetHardcodedUser(username);
                if (ConfirmPassword(password, user.Salt, user.PasswordHash))
                {
                    Traveler.LoggedUsers.Add(user);
                    if (Traveler.IsOpen)
                    {
                        Traveler.TravelerObject.ActiveUsers.Add(user);
                    }
                    return 1;
                }
                else
                {
                    // Log that password is not valid
                    return -2;
                }
            }
        }
        /// <summary>
        /// Logout checks if the user is in ActiveUsers, if it is, the user is removed from the list
        /// and Logout returns true. Else logout returns false.
        /// </summary>
        public static void Logout(string username)
        {
            Traveler.LoggedUsers.Remove(Traveler.LoggedUsers.First(u => u.Username == username));
            if (Traveler.IsOpen)
            {
                TravelerDatabase.UpdateUsersDB(Traveler.TravelerObject);
            }
        }
        #endregion
        #region Misc Functions
        /// <summary>
        /// returns a list of active user usernames in string format
        /// </summary>
        /// <returns></returns>
        public static List<string> GetActiveUsers(List<User> users)
        {
            if (users == null)
            {
                return new List<string>();
            }
            else
            {
                return users.Select(x => x.Username).ToList();
            }
        }
        /// <summary>
        /// GetUser checks the Users list for a matching username and returns that user
        /// </summary>
        private static User GetHardcodedUser(string username)
        {
            return HardcodedUsers.First(u => u.Username == username);
        }
        /// <summary>
        /// UserExists checks that the given username matches one of the users username in the
        /// Users list. If it does, returns true.
        /// </summary>
        public static int UserExists(string username)
        {
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
            {
                return TravelerDatabase.CheckUserExistsDB(username);
            }
            else
            {
                //checking hardcoded users for username
                foreach (User user in HardcodedUsers)
                {
                    if (username == user.Username)
                    {
                        return 1;
                    }
                }
                return -1;
            }
        }
        public static byte[] Hash(string value, string salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(salt));
        }
        public static byte[] Hash(string value, byte[] salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), salt);
        }
        public static byte[] Hash(byte[] value, byte[] salt)
        {
            byte[] saltedValue = value.Concat(salt).ToArray();
            return new SHA256Managed().ComputeHash(saltedValue);
        }
        /// <summary>
        /// Checks if the given password hash is equal to the string password and salt for the given user
        /// </summary>
        public static bool ConfirmPassword(string password, byte[] salt, byte[] _password)
        {
            byte[] passwordHash = Hash(password, salt);
            return _password.SequenceEqual(passwordHash);
        }
        /// <summary>
        /// Checks if there are users logged in, and if there are no users logged in(login required) opens the login form
        /// </summary>
        public static void CheckIfLoginRequired()
        {
            if (Traveler.LoggedUsers.Count == 0)
            {
                if (Traveler.IsOpen && Application.OpenForms["MainForm"] is MainForm mainform)
                {
                    mainform.CloseTraveler();
                }
                using (LoginForm loginForm = new LoginForm())
                {
                    loginForm.ShowDialog();
                }
            }
        }
        /// <summary>
        /// gets the maximum authority level of logged in users
        /// </summary>
        /// <returns></returns>
        public static int GetAuthority()
        {
            return Traveler.LoggedUsers.Max(x => x.DefaultAuthority);
        }
        /// <summary>
        /// creates a new random salt
        /// </summary>
        public static string GenerateSalt()
        {
            return new string(new char[] { (char)new Random((int)DateTime.Now.Ticks).Next('a', 'z'), (char)new Random((int)DateTime.Now.Ticks - 100).Next('a', 'z') });
        }
        #endregion
    }
}
