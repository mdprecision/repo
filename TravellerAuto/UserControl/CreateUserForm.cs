﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class CreateUserForm : Form
    {
        #region Attributes
        public User NewUser { get; set; }
        public bool UserCreated { get; set; } = false;
        #endregion
        #region FormEvents
        public CreateUserForm()
        {
            InitializeComponent();
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {
            CreateUser();
        }
        #endregion
        #region LoginFunctions
        private void CreateUser()
        {
            if (!TravelerDatabase.UseDatabase || !TravelerDatabase.PingDB())
            {
                LoginStatusLbl.ForeColor = Color.Red;
                LoginStatusLbl.Text = "No Internet Connection";
                UsernameBox.Focus();
            }
            else
            {
                DialogResult res = MessageBox.Show("Confirm the information is correct?",string.Empty,MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if (res == DialogResult.Yes && CheckInputs())
                {
                    using (AdminConfirmForm form = new AdminConfirmForm())
                    {
                        form.ShowDialog();
                        if (form.AdminConfirm)
                        {
                            UserCreated = true;
                            byte[] salt = Encoding.UTF8.GetBytes(User.GenerateSalt());
                            NewUser = new User(UsernameBox.Text, TravelerDatabase.NextUserID(), EmailBox.Text, User.Hash(NewPasswordBox.Text, salt), salt, AuthComboBox.SelectedIndex, AuthComboBox.SelectedIndex);
                            TravelerDatabase.CreateUser(
                                new User(UsernameBox.Text, TravelerDatabase.NextUserID(), EmailBox.Text, User.Hash(NewPasswordBox.Text, salt), salt, AuthComboBox.SelectedIndex, AuthComboBox.SelectedIndex));
                            MessageBox.Show("User Created");
                            Close();
                        }
                    }
                }
            }
        }
        private bool CheckInputs()
        {
            if (UsernameBox.Text == string.Empty)
            {
                MessageBox.Show("Username Empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (UsernameBox.Text.Length < 5)
            {
                MessageBox.Show("Username must be longer than 4 characters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!new Regex(@"^\w+$").IsMatch(UsernameBox.Text))
            {
                MessageBox.Show("Username must be numbers and/or Letters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (EmailBox.Text == string.Empty)
            {
                MessageBox.Show("Email Empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (EmailBox.Text.Length < 5)
            {
                MessageBox.Show("Email must be longer than 4 characters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (NewPasswordBox.Text != NewPasswordBox2.Text)
            {
                MessageBox.Show("Passwords Not Equal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (AuthComboBox.SelectedIndex== -1)
            {
                MessageBox.Show("Select Authority level", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        #endregion
    }
}
