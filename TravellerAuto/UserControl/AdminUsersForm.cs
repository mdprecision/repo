﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravellerAuto
{
    public partial class AdminUsersForm : Form
    {
        #region Attributes
        List<User> UnchangedUsers { get; set; } = new List<User>();
        List<User> Users { get; set; } = new List<User>();
        private string Customer { get; set; } = string.Empty;
        private string Template { get; set; } = string.Empty;
        #endregion
        #region Form Events
        public AdminUsersForm()
        {
            InitializeComponent();
        }
        private void AdminUsersForm_Load(object sender, EventArgs e)
        {
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                Tuple<string, string> travelerInfo = mainform.GetCustomerAndTemplate();
                if (travelerInfo == null)
                {
                    TemplateAuthLabel.Text = string.Empty;
                    TemplateAuthList.Enabled = false;
                }
                else
                {
                    Customer = travelerInfo.Item1;
                    Template = travelerInfo.Item2;
                    TemplateAuthLabel.Text = TemplateAuthLabel.Text.Replace("????", Template);
                }
            }
            Users = TravelerDatabase.GetAllUsers(Customer,Template);
            UnchangedUsers = new List<User>();
            Users.ForEach(user =>
            {
                UnchangedUsers.Add(new User(user));
            });
            UsersListBox.DisplayMember = "username";
            LoadUsers();
        }
        private void UsersListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            User selectedUser = (User)UsersListBox.SelectedItem;
            AuthList.SelectedIndex = selectedUser.DefaultAuthority;
            if (TemplateAuthList.Enabled)
            {
                TemplateAuthList.SelectedIndex = selectedUser.TemplateAuthority;
            }
            EmailLabelUser.Text = selectedUser.Email;
            UsernameUser.Text = selectedUser.Username;
            UserIDUser.Text = selectedUser.ID.ToString();
        }
        private void AuthList_DropDownClosed(object sender, EventArgs e)
        {
            User selectedUser = (User)UsersListBox.SelectedItem;
            selectedUser.DefaultAuthority = AuthList.SelectedIndex;
        }
        private void TemplateAuthButton_DropDownClosed(object sender, EventArgs e)
        {
            User selectedUser = (User)UsersListBox.SelectedItem;
            selectedUser.TemplateAuthority = TemplateAuthList.SelectedIndex;
        }
        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            using (ChangePasswordForm form = new ChangePasswordForm((User)UsersListBox.SelectedItem))
            {
                form.ShowDialog();
                if (form.PasswordUpdated)
                {
                    User updatedUser = (User)UsersListBox.SelectedItem;
                    MessageBox.Show($"Password Updated for User '{updatedUser.Username}'");
                }
            }
        }
        private void CreateUserButton_Click(object sender, EventArgs e)
        {
            using (CreateUserForm form = new CreateUserForm())
            {
                form.ShowDialog();
                if (form.UserCreated)
                {
                    Users.Add(form.NewUser);
                    UsersListBox.Items.Add(form.NewUser);
                }
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            using (AdminConfirmForm form = new AdminConfirmForm())
            {
                form.ShowDialog();
                if (form.AdminConfirm)
                {
                    TravelerDatabase.UpdateUser((User)UsersListBox.SelectedItem);
                    MessageBox.Show("Changes Applied");
                }
            }
        }
        private void RevertButton_Click(object sender, EventArgs e)
        {
            Users = new List<User>();
            UnchangedUsers.ForEach(user =>
            {
                Users.Add(new User(user));
            });
            LoadUsers(UsersListBox.SelectedIndex);
        }
        private void DeleteUserButton_Click(object sender, EventArgs e)
        {
            User user = (User)UsersListBox.SelectedItem;
            if (user.Username.Equals("admin"))
            {
                MessageBox.Show("Cannot Delete admin user");
                return;
            }
            DialogResult res = MessageBox.Show($"Are you sure you want to delete user '{user.Username}'","Delete User",MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                using (AdminConfirmForm form = new AdminConfirmForm())
                {
                    form.ShowDialog();
                    if (form.AdminConfirm)
                    {
                        TravelerDatabase.DeleteUser(user);
                        UsersListBox.SelectedIndex = 0;
                        Users.Remove(user);
                        UsersListBox.Items.Remove(user);
                    }
                }
            }
        }
        #endregion

        #region Form Functions
        private void LoadUsers(int index = 0)
        {
            UsersListBox.Items.Clear();
            Users.ForEach(e => UsersListBox.Items.Add(e));
            UsersListBox.SelectedIndex = index;
        }
        #endregion


    }
}
