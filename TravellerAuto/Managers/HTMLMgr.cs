﻿using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using TravellerAuto.Managers;
using TravellerAuto.PromptClasses;

namespace TravellerAuto
{
    public static class HtmlMgr
    {
        #region HTML Functions
        /// <summary>
        /// Loads HTML into TravelerHTML object from file, doesnt generate any values
        /// </summary>
        public static void ReloadFromFile()
        {
            string htmlPath = Path.Combine(PathMgr.GetCachePth(Traveler.TravelerObject.CustomerName, Traveler.TravelerObject.TemplateName), $"{Traveler.TravelerObject.TravelerSN}.html");

            Traveler.Html = XElement.Load(htmlPath);
            Traveler.HtmlUri = new Uri(htmlPath);
        }
        /// <summary>
        /// Reloads prompts on static HTML and live webview
        /// </summary>
        /// <param name="indexToUpdate">List of Prompt indexes to update</param>
        public static void ReloadPrompts(List<int> indexToUpdate)
        {
            //loop through each prompt to update.
            //we update both the static html file, and the webbrowser live with javascript so we dont have to reload the HTML file eveythim
            for (int i = 0; i < indexToUpdate.Count; i++)
            {
                XElement newNode;
                XElement elementToUpdate;
                if (Traveler.TravelerObject.ListOfUserPrompts[indexToUpdate[i]].PromptType == "CAM")
                {
                    // getting existing node (should always only get one)
                    elementToUpdate = FindElementByTagId(indexToUpdate[i], "img");
                    //getting new node
                    newNode = XElement.Parse(Traveler.TravelerObject.ListOfUserPrompts[indexToUpdate[i]].GetHtmlCode().Replace(@"\", "/"));
                }
                else if (Traveler.TravelerObject.ListOfUserPrompts[indexToUpdate[i]].PromptType == "YN")
                {
                    //getting existing node (should always only retunr one node)
                    elementToUpdate = FindElementByTagId(indexToUpdate[i], "span");
                    //getting new node
                    newNode = XElement.Parse(Traveler.TravelerObject.ListOfUserPrompts[indexToUpdate[i]].GetHtmlCode());
                }
                else
                {
                    // getting existing (should always return one xelement)
                    elementToUpdate = FindElementByTagId(indexToUpdate[i], "input");
                    //gettng new node
                    newNode = XElement.Parse(Traveler.TravelerObject.ListOfUserPrompts[indexToUpdate[i]].GetHtmlCode());
                }
                //replacing old node with new
                elementToUpdate.ReplaceWith(newNode);
                //updating live with javascript
                if (Application.OpenForms["MainForm"] is MainForm mainform)
                {
                    mainform.UpdateNode(indexToUpdate[i]);
                }
            }
        }
        /// <summary>
        /// Reloads all HTML elements, only called when cache is deleted or first time
        /// </summary>
        public static void LoadHTMLFirstTime(string customer, string template, string sn, WordprocessingDocument wordDoc)
        {
            // reloading ENTIRE html document, ideally only done on the first creation of a traveler, or when the cache is deleted
            Traveler.Html = CreateHtml(wordDoc);
            string htmlPath = Path.Combine(PathMgr.GetCachePth(customer, template), $"{sn}.html");

            // Main SN Number
            try
            {
                FindElementByValueTag("{[{SN}]}", "p").Value = Traveler.TravelerObject.TravelerSN;
            }
            catch (Exception)
            {
                MessageBox.Show("No Main SN Number Found in Template");
                // this is acceptable, some Travelers dont have an SN number on the template
            }

            // finds all prompts, and replaces their values
            int i = 0;
            FindElementByValueTagList("{{", "span")
                .ForEach(e => e.ReplaceWith(XElement.Parse(TextProcessor.ReplaceUserPrompt(e.ToString(), Traveler.TravelerObject.ListOfUserPrompts[i++].GetHtmlCode()))));

            // Traveler Notes
            try
            {
                XElement Notes = FindElementByValueTag("{[{Notes}]}", "p");
                Notes.Value = Traveler.TravelerObject.TravelerNotes;
                Notes.SetAttributeValue("id", "TravelerNotes");

            }
            catch (Exception)
            {
                // this is acceptable, some travelers dont have a notes section
                Traveler.TravelerObject.TravelerNotes = "-1";
            }

            // counting number of assembly details
            List<XElement> assemblyDetails = FindElementByValueTagList("{[{AD_SN", "p");
            Traveler.TravelerObject.NumberOfAssemblies = assemblyDetails.Count;
            //setting the number of assemblies if its unset
            if (Traveler.TravelerObject.AssemblyDetailsList.Count == 0)
            {
                Traveler.TravelerObject.AssemblyDetailsList = AssemblyDetails.CreateAssemblyDetails(Traveler.TravelerObject.NumberOfAssemblies);
            }

            //settings values
            //SN number
            i = 0;
            assemblyDetails
                .ForEach(e =>
                {
                    e.Value = Traveler.TravelerObject.AssemblyDetailsList[i].SN;
                    e.SetAttributeValue("id", "AD:" + i++.ToString());
                });
            //notes
            i = 0;
            FindElementByValueTagList("{[{AD_NOTES", "p")
                .ForEach(e =>
                {
                    e.Value = Traveler.TravelerObject.AssemblyDetailsList[i].AssemblyNotes;
                    e.SetAttributeValue("id", "ADN:" + i++.ToString());
                });
            // PN
            i = 0;
            FindElementByValueTagList("{[{AD_PN:", "p")
                .ForEach(e =>
                {
                    Traveler.TravelerObject.AssemblyDetailsList[i].PN = e.Value.Split(':')[1].Replace("}]}", string.Empty);
                    e.Value = Traveler.TravelerObject.AssemblyDetailsList[i].PN;
                    e.SetAttributeValue("id", "ADPN:" + i++.ToString());
                });
            // Description
            i = 0;
            FindElementByValueTagList("{[{AD_DESC:", "p")
                .ForEach(e =>
                {
                    Traveler.TravelerObject.AssemblyDetailsList[i].AssemblyDesc = e.Value.Split(':')[1].Replace("}]}", string.Empty);
                    e.Value = Traveler.TravelerObject.AssemblyDetailsList[i].AssemblyDesc;
                    e.SetAttributeValue("id", "ADD:" + i++.ToString());
                });
            i = 0;
            //Operator initials
            FindElementByValueTagList("{[{OI:", "p")
                .ForEach(e =>
                {
                    e.Value = Traveler.TravelerObject.GroupDataList[i].OperatorInitials;
                    e.SetAttributeValue("id", "OI:" + i++.ToString());
                });
            i = 0;
            //Operator initials
            FindElementByValueTagList("{[{ST:", "p")
                .ForEach(e =>
                {
                    if (Traveler.TravelerObject.GroupDataList[i].PassFail != string.Empty)
                    {
                        e.Value = Traveler.TravelerObject.GroupDataList[i].PassFail;
                    }
                    else
                    {
                        e.Value = string.Empty;
                    }
                    e.SetAttributeValue("id", "ST:" + i++.ToString());
                });
            i = 0;
            //Operator initials
            FindElementByValueTagList("{[{DoC:", "p")
                .ForEach(e =>
                {
                    e.Value = Traveler.TravelerObject.GroupDataList[i].DateComplete;
                    e.SetAttributeValue("id", "DoC:" + i++.ToString());
                });

            // getting pointer events and select to be none, so the user cannot click the webroser input fields, checkboxes etc.
            Traveler.Html.Descendants()
            .First(e => e.Name.LocalName.Equals("body"))
            .SetAttributeValue("style", "pointer-events:none;user-select:none;");

            // write to the html file and set the traveler URI
            File.WriteAllText(htmlPath, Traveler.Html.ToString());
            Traveler.HtmlUri = new Uri(htmlPath);
        }
        /// <summary>
        /// Reloads all assembly details
        /// </summary>
        public static void ReloadAssemblyDetails()
        {
            //getting assembly detail SN nodes
            int i = 0;
            FindElementByIdList("AD:").ForEach(e => e.Value = Traveler.TravelerObject.AssemblyDetailsList[i++].SN);
            i = 0;
            FindElementByIdList("ADN:").ForEach(e => e.Value = Traveler.TravelerObject.AssemblyDetailsList[i++].AssemblyNotes);

            //updating live webbrowser with javascript
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.UpdateNode(-1);
            }
        }
        /// <summary>
        /// Realods group values in the respective group
        /// </summary>
        /// <param name="group">Group index to refresh</param>
        public static void ReloadGroupDetails(int group)
        {
            try
            {
                //update static html
                FindElementById("OI:" + group).Value = Traveler.TravelerObject.GroupDataList[group].OperatorInitials;
                FindElementById("ST:" + group).Value = Traveler.TravelerObject.GroupDataList[group].PassFail;
                FindElementById("DoC:" + group).Value = Traveler.TravelerObject.GroupDataList[group].DateComplete;

                //update the webbrowser values live
                if (Application.OpenForms["MainForm"] is MainForm mainform)
                {
                    mainform.UpdateNode(-2, group);
                }
            }
            catch (Exception)
            {
                //providing input for group info is optional
                MessageBox.Show("No Group Info Found (Pass/Fail, Initials, Date of Completion");
            }
        }
        /// <summary>
        /// Reloads all prompt Notes
        /// </summary>
        public static void ReloadNotes()
        {
            // get notes node and update the static html
            FindElementById("TravelerNotes").Value = Traveler.TravelerObject.TravelerNotes;

            //update the webbrowser view live with javascript
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.UpdateNode(-3);
            }
        }
        /// <summary>
        /// Converts the template (docx) to html, stores it on disk and grabs the uri
        /// </summary>
        public static XElement CreateHtml(WordprocessingDocument wordDoc)
        {
            // DISPLAY ENTIRE DOC
            HtmlConverterSettings settings = new HtmlConverterSettings()
            {
                PageTitle = "SoneleTraveler"
            };
            XElement html = HtmlConverter.ConvertToHtml(wordDoc, settings);

            // sonele logo banner
            XElement header = XElement.Parse($"<div class=\"banner\"><img src=\"{Path.Combine(PathMgr.ResourceFolder, "sonelelogo.jpg")}\"></img></div>".Replace(@"\", "/"));
            html.Element("{http://www.w3.org/1999/xhtml}body").AddBeforeSelf(header);
            return html;
        }
        #endregion
        #region HTML Helpers
        private static XElement FindElementByTagId(int index, string tagName)
        {
            return Traveler.Html.Descendants()
                .Where(e => e.Name.LocalName.Equals(tagName))
                .Where(e => e.Attribute("id") != null)
                .First(e => e.Attribute("id").Value.Equals(index.ToString()));
        }
        private static XElement FindElementById(string id)
        {
            return Traveler.Html.Descendants()
                .Where(e => e.Attribute("id") != null)
                .First(e => e.Attribute("id").Value.Equals(id));
        }
        private static List<XElement> FindElementByIdList(string id)
        {
            return Traveler.Html.Descendants()
                .Where(e => e.Attribute("id") != null)
                .Where(e => e.Attribute("id").Value.Contains(id)).ToList();
        }
        private static XElement FindElementByValueTag(string value, string tagName)
        {
            return Traveler.Html.Descendants()
                    .Where(e => e.Name.LocalName.Equals(tagName))
                    .Single(e => e.Value.Contains(value));
        }
        private static List<XElement> FindElementByValueTagList(string value, string tagName)
        {
            return Traveler.Html.Descendants()
                .Where(e => e.Name.LocalName.Equals(tagName))
                .Where(e => e.Value.Contains(value))
                .ToList();
        }
        #endregion
    }
}
