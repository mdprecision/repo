﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TravellerAuto.Managers
{
    /// <summary>
    /// PathMgr manages and retrieves necessary directory paths for TravelerAuto
    /// </summary>
    public static class PathMgr
    {
        #region FolderNames
        private static string CustomersDBFolderName { set; get; } = "Customers";
        private static string ExistingTravelersDBDirName { set; get; } = "Existing Travelers";
        private static string CacheFolderName { set; get; } = "Cache";
        private static string HelpDocumentFolderName { set; get; } = "HelpDocument";
        private static string ImagesFolderName { set; get; } = "TravelerImages";
        private static string ConfigFileName { set; get; } = "Configuration.xml";
        private static string NetworkDriveName { set; get; }= $@"{Traveler.ConfigObject.DriveLetter}:\";
        //private static string NetworkDriveName { set; get; } = @"C:\TravelerAutoMockDB";
        private static string NetworkDriveFolderName { set; get; } = "TravelerAuto";
        public static string HelpDocumentName { set; get; } = "Help";
        #endregion
        #region PathNames
        public static string RootApplicationFolderPth { private set; get; } = AppDomain.CurrentDomain.BaseDirectory;
        public static string RootNetworkDrivePath { private set; get; } = Path.Combine(NetworkDriveName, NetworkDriveFolderName);
        public static string CustomersDBPath { private set; get; } = Path.Combine(RootNetworkDrivePath, CustomersDBFolderName);
        public static string HtmlCachePth { private set; get; } = Path.Combine(RootApplicationFolderPth, CacheFolderName);
        public static string HelpPath { private set; get; } = Path.Combine(RootNetworkDrivePath, HelpDocumentFolderName);
        public static string ImagesPath { private set; get; } = Path.Combine(RootNetworkDrivePath, ImagesFolderName);
        public static string ConfigPath { private set; get; } = Path.Combine(RootApplicationFolderPth, ConfigFileName);
        public static string TemplatePath { get; set; }
        public static string LogicPath { get; set; }
        public static string CompletedTravelersPath { get; set; }
        public static string XmlPath { get; set; }
        public static string ResourceFolder { private set; get; } = Path.Combine(Path.GetFullPath(@"..\..\"), "Resources");
        #endregion
        #region Path Creation Functions
        /// <summary>
        /// gets the folder path containing all the .xml files for the given template and customer
        /// </summary>
        public static string GetExistingTrvlrDir(string customer, string traveler)
        {
            return Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customer), traveler), ExistingTravelersDBDirName);
        }
        /// <summary>
        /// gets the folder path containing all the .html files in the cache for the given template and customer
        /// </summary>
        public static string GetCachePth(string customer, string traveler)
        {
            return Path.Combine(HtmlCachePth, Path.Combine(customer, traveler));
        }
        /// <summary>
        /// gets the folder path containing all the images for the traveler given by the customer, template, and sn
        /// </summary>
        public static string GetImagePth(string customer, string traveler, string sn)
        {
            return Path.Combine(ImagesPath, Path.Combine(customer, Path.Combine(traveler, sn)));
        }
        /// <summary>
        /// gets the folder path containing all the images for the templte and customer
        /// </summary>
        public static string GetImagePth(string customer, string traveler)
        {
            return Path.Combine(ImagesPath, Path.Combine(customer, traveler));
        }
        /// <summary>
        /// LoadDirectories creates necessary directories if they don't already exist
        /// </summary>
        public static void LoadDirectories()
        {
            Directory.CreateDirectory(RootApplicationFolderPth);
            Directory.CreateDirectory(RootNetworkDrivePath);
            Directory.CreateDirectory(CustomersDBPath);
            Directory.CreateDirectory(HtmlCachePth);
            Directory.CreateDirectory(Path.Combine(HtmlCachePth, "CompletedTravelers"));
            Directory.CreateDirectory(HelpPath);
            Directory.CreateDirectory(ImagesPath);
        }
        /// <summary>
        /// create the image and cache path for a template and the cooresponding customer
        /// </summary>
        public static void CreateCacheDirectory(string customer, string template, string sn)
        {
            Directory.CreateDirectory(Path.Combine(HtmlCachePth, Path.Combine(customer, template)));
            Directory.CreateDirectory(GetImagePth(customer, template, sn));
        }
        /// <summary>
        /// sets traveler filepaths based on customer and travller template name and revision
        /// </summary>
        public static void LoadFilePaths(string customer, string travellerName)
        {
            Directory.CreateDirectory(Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customer), travellerName), "Completed Travelers"));
            Directory.CreateDirectory(Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customer), travellerName), "Existing Travelers"));
            TemplatePath = Path.Combine(Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customer), travellerName), "Template"), $"{travellerName}.docx");
            LogicPath = Path.Combine(Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customer), travellerName), "Template"), $"{travellerName}-Logic.xlsx");
            CompletedTravelersPath = Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customer), travellerName), "Completed Travelers");
        }
        #endregion
        #region PathFunctions
        /// <summary>
        /// gets all the customers in the network drive travelerauto/customers path
        /// </summary>
        public static List<string> GetCustomerDirNames()
        {
            List<string> Customers = Directory.GetDirectories(CustomersDBPath, "*.*", SearchOption.TopDirectoryOnly)
                .Select(d => Path.GetFileName(d))
                .ToList();
            return Customers;
        }
        /// <summary>
        /// clears the cache and images for the template and customer if the traveler no longer exists or is deleted
        /// </summary>
        public static void ClearCache(string customerName, string template)
        {
            List<string> GetExistingTravelerSNs = PathMgr.GetExistingTravelerSNs(customerName, template);
            string cachePath = GetCachePth(customerName, template);
            if (Directory.Exists(cachePath))
            {
                foreach (string cacheFile in Directory.GetFiles(cachePath)
                .Where(f => f.EndsWith(".html"))
                .Select(f => new FileInfo(f).Name)
                .Select(n => n.Substring(0, n.Length - 5)).ToList())
                {
                    if (!GetExistingTravelerSNs.Contains(cacheFile))
                    {
                        File.Delete(Path.Combine(cachePath, cacheFile + ".html"));
                    }
                }
            }
            if (Directory.Exists(Path.Combine(HtmlCachePth, "CompletedTravelers")))
            {
                foreach (string cacheFile in Directory.GetFiles(Path.Combine(HtmlCachePth, "CompletedTravelers"))
                .Where(f => f.EndsWith(".html")))
                {
                    File.Delete(cacheFile);
                }
            }
            string imagePath = GetImagePth(customerName, template);
            if (Directory.Exists(imagePath))
            {
                foreach (string folder in Directory.GetDirectories(imagePath))
                {
                    if (!GetExistingTravelerSNs.Contains(new DirectoryInfo(folder).Name))
                    {
                        Directory.Delete(folder, true);
                    }
                }
            }
        }
        /// <summary>
        /// returns all the templates for the customer passed as a paramenter
        /// </summary>
        public static List<string> GetCustomerTrvlrNames(string customerName)
        {
            string customerDir = Path.Combine(CustomersDBPath, customerName);
            List<string> Templates = Directory.GetDirectories(customerDir, "*.*", SearchOption.TopDirectoryOnly)
                .Select(d => Path.GetFileName(d))
                .ToList();
            return Templates;
        }
        /// <summary>
        /// returns all the SN numbers for the customer and template passed as a parameter
        /// </summary>
        public static List<string> GetExistingTravelerSNs(string customerName, string template)
        {
            string travelerPath = Path.Combine(Path.Combine(Path.Combine(CustomersDBPath, customerName), template), "Existing Travelers");

            if (Directory.Exists(travelerPath))
            {
                List<string> existingTravelers = Directory.GetFiles(travelerPath, "*.*", SearchOption.TopDirectoryOnly)
                .Where(f => f.EndsWith(".xml"))
                .Select(f => new FileInfo(f).Name)
                .Select(n => n.Substring(0, n.Length - 4)).ToList();
                return existingTravelers;
            }
            else
            {
                return new List<string>();
            }


        }
        /// <summary>
        /// Checks if we can connect to the network drive
        /// </summary>
        /// <returns></returns>
        public static bool CheckNetworkDrive()
        {
            try
            {
                //throwaway function call, throws an error if we cannot connect to the path
                Directory.EnumerateDirectories(RootNetworkDrivePath);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// gets path to the header pdf to be merged for the given customer and template
        /// </summary>
        public static string GetSignOffPagePath(string customer, string template)
        {
            return Path.Combine(Path.Combine(Path.Combine(CustomersDBPath,customer),template),$"{template}-header.pdf");
        }
        #endregion
    }
}
