﻿using System.IO;
using System.Xml.Serialization;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public class ConfigManager
    {
        #region Attributes
        [XmlElement]
        public string Server { get; set; } = "MDP-SRV-APP03";
        [XmlElement]
        public string Port { get; set; } = "3306";
        [XmlElement]
        public string Password { get; set; } = "gr3enTiger16";
        [XmlElement]
        public string DatabaseName { get; set; } = "traveler";
        [XmlElement]
        public string User { get; set; } = "traveleruser";
        [XmlElement]
        public string DriveLetter { get; set; } = "S";    // the letter of the network drive
        [XmlIgnore]
        private static XmlSerializer XmlSerializer { get; set; } = new XmlSerializer(typeof(ConfigManager));
        #endregion
        #region Config Functions
        public ConfigManager() { }
        #endregion
        #region Serializing Functions
        /// <summary>
        /// Loads Config object from the configuration.xml file
        /// </summary>
        public static ConfigManager LoadConfig()
        {
            ConfigManager newConfig = new ConfigManager();
            using (FileStream stream = new FileStream(PathMgr.ConfigPath, FileMode.Open))
            {
                newConfig = (ConfigManager)XmlSerializer.Deserialize(stream);
            }
            return newConfig;
        }
        /// <summary>
        /// Saves config object to configuration.xml file
        /// </summary>
        public void SaveConfig()
        {
            using (FileStream stream = new FileStream(PathMgr.ConfigPath, FileMode.Create))
            {
                XmlSerializer.Serialize(stream, this);
            }
        }
        #endregion
    }

}
