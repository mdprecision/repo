﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TravellerAuto
{
    /// <summary>
    /// TextProcessor is a static utility class with methods used for processing texts
    /// </summary>
    public static class TextProcessor
    {
        #region TextProcessor Functions
        /// <summary>
        /// getUserPromptItems searches its input text for userprompts and returns a list of tuples
        /// with Item1: PromptType, Item2: PromptName
        /// </summary>
        public static List<Tuple<string, string, int>> GetUserPromptItems(string text)
        {
            Regex rx = new Regex(@"{{(.*?)}}");     // curly braces represent the tagging format defined in the traveler template doc file
            MatchCollection items = rx.Matches(text);
            List<Tuple<string, string, int>> itemsList = new List<Tuple<string, string, int>>(); // <type, name, group number> 

            foreach (Match item in items)
            {
                string[] t = item.Groups[1].Value.Split(':');
                // extract as UserPrompt object
                itemsList.Add(new Tuple<string, string, int>(t[0], t[1], int.Parse(t[2]) - 1));
            }
            return itemsList;

        }
        /// <summary>
        /// Takes a prompt tagged in '{{}}' format and returns the inner contents
        /// </summary>
        public static string ReplaceUserPrompt(string text, string replaceText)
        {
            string pattern = @"{{(.*?)}}";
            if (text.Split('{').Length - 2 > 1)
            {
                Exception exception = new Exception($"Template Error '{text}'");
                throw exception;
            }

            string newText = Regex.Replace(text, pattern, replaceText);
            return newText;
        }
        #endregion
    }
}