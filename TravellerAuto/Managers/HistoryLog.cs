﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TravellerAuto
{
    public class HistoryLog
    {
        #region Attributes
        [XmlAttribute("Users")]
        public List<string> HistoryLogUsers { get; set; } = User.GetActiveUsers(Traveler.LoggedUsers);
        [XmlAttribute("DateTime")]
        public long TimeinTicks { get; set; }
        [XmlAttribute("PreviousValue")]
        public string Previous { get; set; }
        [XmlAttribute("NewValue")]
        public string NewValue { get; set; }
        [XmlAttribute("ActionPerformed")]
        public string ActionPerformed { get; set; } // Can either be "assign value" or "user redaction" or "admin redaction" 
        #endregion
        #region Constructors
        public HistoryLog() { }
        public HistoryLog(long ticks, string previous, string current, string actionPerformed, int index)
        {
            TimeinTicks = ticks;
            Previous = previous;
            NewValue = current;
            ActionPerformed = actionPerformed;

            //contains the database function to populate the history log tables
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
            {
                TravelerDatabase.CreateHistoryLogDB(this, index);
            }
        }
        #endregion
    }
}
