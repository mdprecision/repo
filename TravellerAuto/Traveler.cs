﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using TravellerAuto.Managers;

namespace TravellerAuto
{
    public static class Traveler
    {
        #region Attributes
        public static Uri HtmlUri { get; set; }
        public static XElement Html { get; set; } = null;
        public static WordprocessingDocument WordDocFile { get; set; }
        public static TravelerData TravelerObject { get; set; } = new TravelerData();
        public static List<User> LoggedUsers { get; set; } = new List<User>();
        public static bool ValidLogic { get; set; } = true;
        public static bool IsOpen { get; set; } = false;
        public static bool SkipClear { get; set; } = false;
        public static bool QuitAfterLoginDialog { get; set; } = false;
        public static ConfigManager ConfigObject { set; get; } = new ConfigManager();
        #endregion
        #region Creating and Loading Traveler
        /// <summary>
        /// resets all travellerdata fields and creates a new traveller based on the templates provided
        /// </summary>
        public static void CreateNewTraveler(string customer, string template, string SN)
        {
            //create new travelerdata object and assign the basic members
            TravelerObject = new TravelerData
            {
                CustomerName = customer,
                TemplateName = template,
                TravelerSN = SN
            };
            PathMgr.XmlPath = Path.Combine(PathMgr.GetExistingTrvlrDir(customer, template), $"{SN}.xml");

            if (!IsOpen) // clear cache on first launch of program
            {
                PathMgr.ClearCache(customer, template);
            }

            //create path and cache folders for new traveler
            PathMgr.CreateCacheDirectory(customer, template, SN);
            PathMgr.LoadFilePaths(customer, template);

            //create the list of user prompts and load the html from the template word doc
            using (WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
            {
                List<Tuple<string, string, int>> parsedPrompts = TextProcessor.GetUserPromptItems(WordDocFile.MainDocumentPart.Document.InnerText).ToList();
                TravelerObject.CreateUserPrompts(parsedPrompts);
                HtmlMgr.LoadHTMLFirstTime(customer,template,SN, WordDocFile);
            }

            //creating traveler in database
            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
            {
                TravelerObject = TravelerDatabase.CreateTravelerDB(TravelerObject);
            }

            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.RefreshProgressBar();
                mainform.RefreshUsers();
            }

            // indicates that we have loaded at least one travelr within the runtime of the program
            IsOpen = true;
        }
        /// <summary>
        /// loads a traveller from XML to the current traveler being displayed
        /// </summary>
        public static void LoadExistingTraveler(string customer, string template, string SN)
        {
            TravelerObject.CustomerName = customer;
            TravelerObject.TemplateName = template;
            TravelerObject.TravelerSN = SN;
            PathMgr.XmlPath = Path.Combine(PathMgr.GetExistingTrvlrDir(customer, template), $"{SN}.xml");

            //clear the cache of this template 
            if (!IsOpen)
            {
                PathMgr.ClearCache(customer, template);
            }

            //create and load paths for traveler
            PathMgr.CreateCacheDirectory(customer, template, SN);
            PathMgr.LoadFilePaths(customer, template);

            if (TravelerDatabase.UseDatabase && TravelerDatabase.PingDB())
            {
                //database loading of traveler
                TravelerObject = TravelerDatabase.LoadTravelerDB(customer, template, SN);
                //loading html file
                using (WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
                {
                    HtmlMgr.LoadHTMLFirstTime(customer, template, SN, WordDocFile);
                }
            }
            else
            {
                //xml loading of traveler
                TravelerObject = TravelerData.LoadTravellerData(PathMgr.XmlPath);
            }
            TravelerObject.LoadPromptGroups();

            //get users logged into traveler
            List<string> ActiveUsernames = User.GetActiveUsers(TravelerObject.ActiveUsers);
            foreach (User user in LoggedUsers)
            {
                if (!ActiveUsernames.Contains(user.Username))
                {
                    TravelerObject.ActiveUsers.Add(user);
                }
            }

            // load html from word doc template
            using (WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
            {
                HtmlMgr.ReloadFromFile();
            }
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.ReloadBrowser();
                mainform.LockTraveller(TravelerObject.TravellerIsLocked);
                mainform.ClearAllInputFields();
                mainform.RefreshProgressBar();
                mainform.RefreshUsers();
            }

            IsOpen = true;
        }
        #endregion
        #region Navigating Traveler
        /// <summary>
        /// tries to set value for the current selected prompt and reloads the HTML if it succeeds
        /// </summary>
        public static bool SetValueToSelectedPrompt(string val)
        {
            if (TravelerObject.SetValueForSelectedPrompt(val))
            {
                //select next prompt, boolean true tells the function it was sent by the submit button to avoid certain code
                int returnStatus = TravelerObject.SelectNextPrompt2(true);
                using (WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
                {
                    if (returnStatus == 2) //indicates we completed an entire group, and there is another group after
                    {
                        //reload before and after group PROMPTS
                        List<int> promptsToUpdate = TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex]
                            .Concat(TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex - 1]).ToList();
                        HtmlMgr.ReloadPrompts(promptsToUpdate);

                        //reloads before group details (pass/fail, date of completion, operator initials)
                        HtmlMgr.ReloadGroupDetails(TravelerObject.CurrentGroupIndex - 1);
                    }
                    else if (returnStatus == 3) // indicates we completed the entire traveler, only reload current group
                    {
                        // reloading group PROMPTS
                        List<int> promptsToUpdate = TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex];
                        HtmlMgr.ReloadPrompts(promptsToUpdate);

                        HtmlMgr.ReloadGroupDetails(TravelerObject.CurrentGroupIndex);

                        //saving and copmleting traveler
                        TravelerObject.SaveTravelerToPDF();
                        TravelerDatabase.CreateCompleteTraveler(TravelerObject);
                    }
                    else if (returnStatus == 4) // indicates reloading current group
                    {
                        HtmlMgr.ReloadGroupDetails(TravelerObject.CurrentGroupIndex);
                        HtmlMgr.ReloadPrompts(TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex]);
                    }
                    else
                    {
                        //reloads current group PROPMTS (usually when going next or previous prompt)
                        HtmlMgr.ReloadPrompts(TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex]);
                    }
                    return true;
                }
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// selects next prompt and reloads HTML
        /// </summary>
        public static int SelectNextPrompt()
        {
            int returnStatus = TravelerObject.SelectNextPrompt2();   // return status tells us which prompts to reload in HTML
            using (WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
            {
                if (returnStatus == 0)//reload current prompt, and one before it
                {
                    HtmlMgr.ReloadPrompts(new List<int> { TravelerObject.SelectedPromptIndex - 1, TravelerObject.SelectedPromptIndex });
                }
                else if (returnStatus == 1)//reloads current prompt, and last one in group (wrap around from going next prompt)
                {
                    HtmlMgr.ReloadPrompts(new List<int> { TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex].Last(), TravelerObject.SelectedPromptIndex });
                }
                else if (returnStatus == -1)// reload entire first group (when starting traveler)
                {
                    HtmlMgr.ReloadPrompts(TravelerObject.PromptGroups[0]);
                }
            }
            return TravelerObject.SelectedPromptIndex;
        }
        /// <summary>
        /// selects previous prompt and reloads HTML
        /// </summary>
        public static int SelectPreviousPrompt()
        {
            int returnStatus = TravelerObject.SelectPreviousPrompt2();
            using (WordDocFile = WordprocessingDocument.Open(PathMgr.TemplatePath, true))
            {
                if (returnStatus == 0)// reload current prompt, and next prompt 
                {
                    HtmlMgr.ReloadPrompts(new List<int> { TravelerObject.SelectedPromptIndex + 1, TravelerObject.SelectedPromptIndex });
                }
                else if (returnStatus == 1)// reload current prompt, and first one in group (wrap around from going backward)
                {
                    HtmlMgr.ReloadPrompts(new List<int> { TravelerObject.PromptGroups[TravelerObject.CurrentGroupIndex].First(), TravelerObject.SelectedPromptIndex });
                }
            }
            return TravelerObject.SelectedPromptIndex;
        }
        #endregion
    }
}
