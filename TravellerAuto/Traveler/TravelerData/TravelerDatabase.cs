﻿using DocumentFormat.OpenXml.Packaging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Windows.Forms;
using TravellerAuto.Managers;
using TravellerAuto.PromptClasses;

namespace TravellerAuto
{
    public static class TravelerDatabase
    {
        #region Attributes
        public static bool UseDatabase { get; private set; } = true;
        public static string ConnectionString { get; private set; } = $"server={Traveler.ConfigObject.Server};port={Traveler.ConfigObject.Port};userid={Traveler.ConfigObject.User};password={Traveler.ConfigObject.Password};database={Traveler.ConfigObject.DatabaseName}";
        #endregion

        #region Creating Database Functions
        /// <summary>
        /// Main database function that creates a traveler entry in the database 
        /// </summary>
        public static TravelerData CreateTravelerDB(TravelerData newTraveler)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand(string.Empty, Connection))
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT * FROM existingtravelers WHERE travelerSN={newTraveler.TravelerSN} AND customer='{newTraveler.CustomerName}' AND template='{newTraveler.TemplateName}'", Connection))
                {

                    bool travelerExistsInDatabase;
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        travelerExistsInDatabase = rdr.Read();
                    }

                    if (!travelerExistsInDatabase)
                    {
                        cmdr.CommandText = $"SELECT * FROM templates WHERE customerName='{newTraveler.CustomerName}' AND templateName='{newTraveler.TemplateName}'";
                        using (MySqlDataReader rdr = cmdr.ExecuteReader())
                        {
                            if (!rdr.Read())
                            {
                                // template not created yet
                                // create template row
                                rdr.Close();
                                cmd.CommandText = $"INSERT INTO templates(templateName,customername,datecreated) VALUES ('{newTraveler.TemplateName}','{newTraveler.CustomerName}','{DateTime.Now:yyy-MM-dd H:mm:ss}')";
                                cmd.ExecuteNonQuery();
                                // create user authority
                                cmdr.CommandText = $"SELECT authority,userid FROM users";
                                cmd.CommandText = String.Empty;
                                using (MySqlDataReader rdr2 = cmdr.ExecuteReader())
                                {
                                    while (rdr2.Read())
                                    {
                                        cmd.CommandText += $"INSERT INTO userauthority(userid,authority,template,customer) VALUES ('{rdr2.GetInt32(1)}','{rdr2.GetInt32(0)}','{newTraveler.TemplateName}','{newTraveler.CustomerName}');";
                                    }
                                }
                                cmd.ExecuteNonQuery();
                            }
                            else
                            {
                                //template already exists
                            }
                        }

                        cmdr.CommandText = $"SELECT promptindex FROM promptlogic WHERE customer='{newTraveler.CustomerName}' AND template='{newTraveler.TemplateName}'";
                        bool travelerHasLogic;
                        using (MySqlDataReader rdr = cmdr.ExecuteReader())
                        {
                            travelerHasLogic = rdr.Read();
                        }

                        if (!travelerHasLogic)
                        {
                            //create prompt logic rows
                            newTraveler.LoadLogic();
                            foreach (int key in newTraveler.PromptConstraints.Keys)
                            {
                                for (int i = 0; i < Traveler.TravelerObject.PromptConstraints[key].Count; i++)
                                {
                                    PromptLogic logic = Traveler.TravelerObject.PromptConstraints[key][i];
                                    cmd.CommandText = $"INSERT INTO promptlogic(promptindex,targetPrompt,operator,comparedPrompt,offset,dateCreated,template,customer,comparedvalue) Values (" +
                                        $"{key},'{logic.TargetPrompt}','{logic.Operator_}','{logic.ComparedPrompt}','{logic.Offset}'" +
                                        $",'{DateTime.Now:yyy-MM-dd H:mm:ss}','{newTraveler.TemplateName}','{newTraveler.CustomerName}','{logic.ComparedValue}')";
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        else
                        {
                            //load logic
                            newTraveler.PromptConstraints.Clear();
                            cmdr.CommandText = $"SELECT targetprompt,operator,comparedprompt,offset,promptindex,comparedvalue FROM promptlogic WHERE customer='{newTraveler.CustomerName}' AND template='{newTraveler.TemplateName}'";
                            using (MySqlDataReader rdr = cmdr.ExecuteReader())
                            {
                                while (rdr.Read())
                                {
                                    PromptLogic logic = new PromptLogic();
                                    int length = 0;

                                    int index = rdr.GetInt32(4);

                                    try
                                    {
                                        logic.TargetPrompt = rdr.GetString(0);
                                        length++;
                                        logic.Operator_ = rdr.GetString(1);
                                        length++;
                                        logic.ComparedPrompt = rdr.GetString(2);
                                        if (!logic.ComparedPrompt.Equals(string.Empty))
                                        {
                                            length++;
                                        }
                                        logic.Offset = rdr.GetString(3);
                                        if (!logic.Offset.Equals(string.Empty))
                                        {
                                            length++;
                                        }
                                        logic.ComparedValue = rdr.GetString(5);
                                    }
                                    catch (SqlNullValueException)
                                    {
                                        /*when this is thrown is means we have reaced the end of the logic command (can be variable lengths)*/
                                    }
                                    logic.Length = length;

                                    if (newTraveler.PromptConstraints.ContainsKey(index))    // if the prompt indexed already has logic, just append to the dictionary, if not add a new dictionary entry
                                    {
                                        newTraveler.PromptConstraints[index].Add(logic);
                                    }
                                    else
                                    {
                                        newTraveler.PromptConstraints.Add(index, new List<PromptLogic> { logic });
                                    }
                                    newTraveler.LogicCount++;
                                }
                            }
                        }

                        // create existing travelers row
                        cmd.CommandText = $"INSERT INTO existingtravelers(template,customer,travelerSN,selectedPromptIndex,currentGroupIndex,scrollPosition,percentProgress,dateCreated,isLocked,isComplete,TravelerNotes,lastDateSynced) " +
                            $"VALUES ('{newTraveler.TemplateName}','{newTraveler.CustomerName}',{newTraveler.TravelerSN},{newTraveler.SelectedPromptIndex},{newTraveler.CurrentGroupIndex},{newTraveler.ScrollPosition},{newTraveler.PercentProgress}," +
                            $"'{DateTime.Now:yyy-MM-dd H:mm:ss}',{newTraveler.TravellerIsLocked},{newTraveler.IsComplete},'{newTraveler.TravelerNotes}','{DateTime.Now:yyy-MM-dd H:mm:ss}')";
                        cmd.ExecuteNonQuery();

                        // create active user rows
                        foreach (User user in Traveler.LoggedUsers)
                        {
                            if (user.DefaultAuthority > 1)
                            {
                                continue;
                            }
                            cmd.CommandText = $"INSERT INTO ActiveUsers(travelersn,UserID,dateCreated,template,customer,username) " +
                                $"VALUES ({newTraveler.TravelerSN},{user.ID},'{DateTime.Now:yyy-MM-dd H:mm:ss}','{newTraveler.TemplateName}','{newTraveler.CustomerName}','{user.Username}')";
                            cmd.ExecuteNonQuery();
                        }

                        //create assemblydetails rows
                        for (int i = 0; i < newTraveler.NumberOfAssemblies; ++i)
                        {
                            cmd.CommandText = $"INSERT INTO assemblydetails(TravelerSN,PN,DateCreated,description,ADindex,template,customer) " +
                                    $"VALUES ({newTraveler.TravelerSN},'{newTraveler.AssemblyDetailsList[i].PN}','{DateTime.Now:yyy-MM-dd H:mm:ss}'" +
                                    $",'{newTraveler.AssemblyDetailsList[i].AssemblyDesc}',{i},'{newTraveler.TemplateName}','{newTraveler.CustomerName}')";
                            cmd.ExecuteNonQuery();
                        }

                        // create groupdata rows
                        for (int i = 0; i < newTraveler.NumberOfGroups; ++i)
                        {
                            cmd.CommandText = $"INSERT INTO groupdata(travelerSN,datecreated,groupnumber,template,customer) " +
                                    $"VALUES ({newTraveler.TravelerSN},'{DateTime.Now:yyy-MM-dd H:mm:ss}',{i},'{newTraveler.TemplateName}','{newTraveler.CustomerName}')";
                            cmd.ExecuteNonQuery();
                        }

                        //create prompt rows
                        for (int i = 0; i < newTraveler.ListOfUserPrompts.Count; ++i)
                        {
                            UserPrompt prompt = newTraveler.ListOfUserPrompts[i];
                            string type = string.Empty;
                            if (prompt.PromptType == "DateTime")
                            {
                                DateTimePrompt dt = (DateTimePrompt)prompt;
                                type = dt.DT_Type;
                            }
                            else
                            {
                                type = prompt.PromptType;
                            }

                            cmd.CommandText = $"INSERT INTO prompts(promptIndex,type,promptName,promptGroup,dateCreated," +
                                $"TravelerSN,IsSet,groupLocked,selected,valid,template,customer) VALUES ({prompt.Index},'{type}','{prompt.PromptName}'," +
                                $"'{prompt.PromptGroup}','{DateTime.Now:yyy-MM-dd H:mm:ss}',{newTraveler.TravelerSN},{prompt.IsSet},{prompt.GroupLocked}," +
                                $"{prompt.Selected},{prompt.Valid},'{newTraveler.TemplateName}','{newTraveler.CustomerName}')";
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Exception exception = new Exception("Traveler Not Found In Database");
                        throw exception;
                    }
                    return newTraveler;
                }
            }
        }
        /// <summary>
        /// creates a history log row for the given history log
        /// </summary>
        public static void CreateHistoryLogDB(HistoryLog log, int index)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand(string.Empty, Connection))
                {
                    //inserting the history log event
                    cmd.CommandText = $"INSERT INTO historylog(previousValue,newValue,actionPerformed,promptIndex,dateCreated,TravellerSN,template,customer)" +
                        $" VALUES ('{log.Previous}','{log.NewValue}','{log.ActionPerformed}',{index},'{DateTime.Now:yyy-MM-dd H:mm:ss}'" +
                        $",{Traveler.TravelerObject.TravelerSN},'{Traveler.TravelerObject.TemplateName}','{Traveler.TravelerObject.CustomerName}')";
                    cmd.ExecuteNonQuery();

                    //get the unique ID for the history log we just inserted
                    int latestID;
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT LAST_INSERT_ID()", Connection))
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        rdr.Read();
                        latestID = rdr.GetInt32(0);
                    }
                    // insert the users that were active for the cooresponding history log referenced by the ID above
                    foreach (string user in User.GetActiveUsers(Traveler.LoggedUsers))
                    {
                        int userID;
                        using (MySqlCommand cmdr = new MySqlCommand($"SELECT userID from users WHERE username='{user}'", Connection))
                        using (MySqlDataReader rdr = cmdr.ExecuteReader())
                        {
                            rdr.Read();
                            userID = rdr.GetInt32(0);
                        }
                        cmd.CommandText = $"INSERT INTO historylogusers(ID,userID,dateCreated,username) VALUES ({latestID},{userID},'{DateTime.Now:yyy-MM-dd H:mm:ss}', '{user}')";
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        /// <summary>
        /// creates completed traveler row in the database
        /// </summary>
        public static void CreateCompleteTraveler(TravelerData traveler)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"INSERT INTO completedtravelers(travelersn, template, customer, html) " +
                    $"VALUES ({traveler.TravelerSN},'{traveler.TemplateName}','{traveler.CustomerName}','{Convert.ToBase64String(Encoding.UTF8.GetBytes(Traveler.Html.ToString()))}')", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region Updating Database Functions
        /// <summary>
        /// adds any new users logged in to the active users for the traveler that is open
        /// </summary>
        public static void UpdateUsersDB(TravelerData traveler)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                // update users
                foreach (User user in Traveler.LoggedUsers)
                {
                    if (user.DefaultAuthority > 1)
                    {
                        // skip adding admin users to active users
                        continue;
                    }
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT userID FROM ActiveUsers WHERE userID='{user.ID}' AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'", Connection))
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    using (MySqlCommand cmd = new MySqlCommand($"INSERT INTO ActiveUsers(travelersn,UserID,dateCreated,username,customer,template) " +
                        $"VALUES ({traveler.TravelerSN},{user.ID},'{DateTime.Now:yyy-MM-dd H:mm:ss}', '{user.Username}','{traveler.CustomerName}','{traveler.TemplateName}')", Connection))
                    {
                        // inserts user if not there already
                        if (!rdr.Read())
                        {
                            rdr.Close();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// updates the timout time for each traveler to be 12 hours from now
        /// </summary>
        public static void UpdateTravelerBusyTimeDB(TravelerData traveler)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"UPDATE existingtravelers set busyDate='{DateTime.Now.AddHours(12.0):yyy-MM-dd H:mm:ss}'" +
                    $"WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// sets the traveler busy so that no other users can open the traveler
        /// </summary>
        public static void SetTravelerBusyDB(TravelerData traveler)
        {
            // using win32 serial number, which should be unique between machines. we use this to differentiate different machines
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_OperatingSystem");
            ManagementObject searchObject = searcher.Get().Cast<ManagementObject>().First();
            string serialNumber = searchObject["SerialNumber"].ToString();

            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"UPDATE existingtravelers set isBusy=true,busyDate='{DateTime.Now.AddHours(12.0):yyy-MM-dd H:mm:ss}',busyMachine='{serialNumber}' " +
                    $"WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// releases the current traveler so that it is no longer busy
        /// </summary>
        public static void ReleaseTravelerDB(TravelerData traveler)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"UPDATE existingtravelers set isBusy=false,busyDate= NULL,busyMachine=NULL " +
                        $"WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// updates the current traveler data to the database
        /// </summary>
        public static void SaveTravelerDB(TravelerData traveler, bool skipSavingUsers = false)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand(string.Empty, Connection))
                {
                    //check for traveler in DB
                    MySqlCommand cmdr = new MySqlCommand($"SELECT travelerSn FROM existingtravelers WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'", Connection);
                    MySqlDataReader rdr = cmdr.ExecuteReader();
                    if (!rdr.Read())
                    {
                        CreateTravelerDB(traveler);
                    }
                    rdr.Close();

                    // updateexistingtraveler
                    var command = new StringBuilder();
                    command.Append($"selectedPromptIndex = { traveler.SelectedPromptIndex},");
                    cmd.CommandText = $"UPDATE existingtravelers SET selectedPromptIndex = {traveler.SelectedPromptIndex},currentGroupIndex = {traveler.CurrentGroupIndex},scrollPosition = {traveler.ScrollPosition}" +
                        $",percentProgress = {traveler.PercentProgress},isLocked = {traveler.TravellerIsLocked},isComplete = {traveler.IsComplete},TravelerNotes='{Convert.ToBase64String(Encoding.UTF8.GetBytes(traveler.TravelerNotes))}'" +
                        $", lastDateSynced='{DateTime.Now:yyy-MM-dd H:mm:ss}' WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    cmd.ExecuteNonQuery();

                    if (!skipSavingUsers)
                    {
                        UpdateUsersDB(traveler);
                    }

                    //upate assembly details
                    for (int i = 0; i < traveler.NumberOfAssemblies; ++i)
                    {
                        if (traveler.AssemblyDetailsList[i].SN != string.Empty)
                        {
                            cmd.CommandText = $"UPDATE assemblydetails SET SN='{traveler.AssemblyDetailsList[i].SN}' WHERE ADindex={i} AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                            cmd.ExecuteNonQuery();
                        }
                        if (traveler.AssemblyDetailsList[i].AssemblyDesc != string.Empty)
                        {
                            cmd.CommandText = $"UPDATE assemblydetails SET description='{traveler.AssemblyDetailsList[i].AssemblyDesc}' WHERE ADindex={i} AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // update group data
                    for (int i = 0; i < traveler.NumberOfGroups; ++i)
                    {
                        var groupList = traveler.GroupDataList[i];
                        if (groupList.PassFail.Equals("PASS"))
                        {
                            cmd.CommandText = $"UPDATE groupdata SET pass=1 WHERE groupNumber={i} AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                            cmd.ExecuteNonQuery();
                        }
                        else if (groupList.PassFail.Equals("FAIL"))
                        {
                            cmd.CommandText = $"UPDATE groupdata SET pass=0 WHERE groupNumber={i} AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                            cmd.ExecuteNonQuery();
                        }
                        if (!groupList.DateComplete.Equals(string.Empty))
                        {
                            string dt = DateTime.Parse(traveler.GroupDataList[i].DateComplete).ToString("yyy-MM-dd H:mm:ss");
                            cmd.CommandText = $"UPDATE groupdata SET dateOfCompletion='{dt}' WHERE groupNumber={i} AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                            cmd.ExecuteNonQuery();
                        }
                        if (!groupList.OperatorInitials.Equals(string.Empty))
                        {
                            cmd.CommandText = $"UPDATE groupdata SET operatorInitials='{traveler.GroupDataList[i].OperatorInitials}' " +
                                $"WHERE groupNumber={i} AND travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                            cmd.ExecuteNonQuery();
                        }
                    }

                    //update prompt values
                    for (int i = 0; i < traveler.ListOfUserPrompts.Count; ++i)
                    {
                        UserPrompt prompt = traveler.ListOfUserPrompts[i];
                        string promptValue = prompt.CurrentValue;

                        //replacing illegal ssql backslash character, this is fixed when reloading the prompt value
                        if (prompt.PromptType.Equals("CAM"))
                        {
                            promptValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(prompt.CurrentValue));
                        }

                        cmd.CommandText = $"UPDATE prompts SET IsSet={prompt.IsSet},groupLocked={prompt.GroupLocked},selected={prompt.Selected}" +
                            $",valid={prompt.Valid} , currentValue = '{promptValue}' WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}' AND promptIndex={prompt.Index}";
                        cmd.ExecuteNonQuery();
                    }
                    UpdateTravelerBusyTimeDB(traveler);
                }
            }
        }
        #endregion
        #region Loading Database Functions
        /// <summary>
        /// Loads traveler form the database to the current traveler data object
        /// </summary>
        public static TravelerData LoadTravelerDB(string customer, string template, string SN)
        {
            TravelerData traveler = new TravelerData
            {
                CustomerName = customer,
                TemplateName = template,
                TravelerSN = SN
            };

            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand(string.Empty, Connection))
                {

                    // load existingtraveler
                    cmdr.CommandText = $"SELECT selectedPromptIndex, currentGroupIndex,scrollPosition,percentProgress,isLocked,isComplete,TravelerNotes FROM existingtravelers WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        if (!rdr.Read())
                        {
                            MessageBox.Show($"Couldnt Find Traveler : {traveler.TravelerSN} On Server. Loading From Local Instead.");
                            // if the traveler isnt in existring travelers, load from XML path instead
                            traveler = TravelerData.LoadTravellerData(PathMgr.XmlPath);
                            traveler.LoadLogic();
                            return traveler;
                        }
                        else
                        {
                            // getting all the travelerobject members from the query above
                            traveler.SelectedPromptIndex = rdr.GetInt32(0);
                            traveler.CurrentGroupIndex = rdr.GetInt32(1);
                            traveler.ScrollPosition = rdr.GetInt32(2);
                            traveler.PercentProgress = rdr.GetInt32(3);
                            traveler.TravellerIsLocked = rdr.GetBoolean(4);
                            traveler.IsComplete = rdr.GetBoolean(5);
                            traveler.TravelerNotes = Encoding.UTF8.GetString(Convert.FromBase64String(rdr.GetString(6)));
                        }
                    }

                    //load assembly details
                    traveler.AssemblyDetailsList.Clear();
                    //getting number of assemblydetails
                    cmdr.CommandText = $"SELECT MAX(adindex) FROM assemblydetails WHERE customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    int numassemblies = -1;
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            numassemblies = rdr.GetInt32(0) + 1;
                        }
                        // creating empty list of assemblies
                        traveler.AssemblyDetailsList = AssemblyDetails.CreateAssemblyDetails(numassemblies);
                        traveler.NumberOfAssemblies = numassemblies;
                    }

                    cmdr.CommandText = $"SELECT SN,notes,PN,description,adindex FROM assemblydetails WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        // null values mean that the details are not set yet, we ommit them if there is no value
                        while (rdr.Read())
                        {
                            try
                            {
                                traveler.AssemblyDetailsList[rdr.GetInt32(4)].SN = rdr.GetString(0);
                            }
                            catch (SqlNullValueException) { /*This is fine, just means it is not filled out yet*/ }
                            try
                            {
                                traveler.AssemblyDetailsList[rdr.GetInt32(4)].AssemblyNotes = rdr.GetString(1);
                            }
                            catch (SqlNullValueException) { /*This is fine, just means it is not filled out yet*/}

                            // these should always be filled out so arent error checked
                            traveler.AssemblyDetailsList[rdr.GetInt32(4)].PN = rdr.GetString(2);
                            traveler.AssemblyDetailsList[rdr.GetInt32(4)].AssemblyDesc = rdr.GetString(3);
                        }
                    }

                    // load group data
                    traveler.GroupDataList.Clear();
                    //getting number of groups
                    int numgroups = -1;
                    cmdr.CommandText = "SELECT MAX(groupnumber) FROM groupdata";
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            numgroups = rdr.GetInt32(0) + 1;
                        }
                        // creating empty list of groupdata
                        traveler.GroupDataList = GroupData.CreateGroupData(numgroups);
                        traveler.NumberOfGroups = numgroups;
                    }

                    cmdr.CommandText = $"SELECT operatorInitials,dateOfCompletion,pass,groupnumber FROM groupdata WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        // null values mean the groups data is not set yet, we ommit setting those values if they are null
                        while (rdr.Read())
                        {
                            string[] boolStrings = { "FAIL", "PASS" };
                            try
                            {
                                traveler.GroupDataList[rdr.GetInt32(3)].OperatorInitials = rdr.GetString(0);
                            }
                            catch (SqlNullValueException) { continue; }
                            try
                            {
                                traveler.GroupDataList[rdr.GetInt32(3)].DateComplete = rdr.GetDateTime(1).ToString("dd/MMM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            catch (SqlNullValueException) { continue; }
                            try
                            {
                                traveler.GroupDataList[rdr.GetInt32(3)].PassFail = boolStrings[rdr.GetInt32(2)];
                            }
                            catch (SqlNullValueException) { continue; }
                        }
                    }

                    //load prompt values
                    traveler.ListOfUserPrompts.Clear();
                    cmdr.CommandText = $"SELECT IsSet,groupLocked,selected,valid,currentValue,promptGroup,promptName,type, promptindex FROM prompts WHERE travelerSN={traveler.TravelerSN} AND customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            string type = rdr.GetString(7);
                            if (type.Equals("D") || type.Equals("T") || type.Equals("DT"))
                            {
                                //casting to datetime so that we can access the DT_TYPE member
                                DateTimePrompt prompt = (DateTimePrompt)UserPrompt.CreatePrompt(type, rdr.GetString(6), rdr.GetInt32(5));
                                prompt.DT_Type = type;

                                // setting the prompt values
                                prompt.Index = rdr.GetInt32(8);
                                prompt.IsSet = rdr.GetBoolean(0);
                                prompt.GroupLocked = rdr.GetBoolean(1);
                                prompt.Selected = rdr.GetBoolean(2);
                                //getting the prompt validity
                                if (rdr.GetInt32(3) == 1)
                                {
                                    prompt.Valid = 1;
                                }
                                else if (rdr.GetInt32(3) == 0)
                                {
                                    prompt.Valid = 0;
                                }
                                else
                                {
                                    prompt.Valid = -1;
                                }

                                try
                                {
                                    prompt.CurrentValue = rdr.GetString(4);
                                }
                                catch (SqlNullValueException) {/*This is OK, it just means the datetime value is not set yet*/ }
                                //adding the new prompt to the list
                                traveler.ListOfUserPrompts.Add(prompt);
                            }
                            else
                            {
                                UserPrompt prompt = UserPrompt.CreatePrompt(type, rdr.GetString(6), rdr.GetInt32(5));
                                prompt.Index = rdr.GetInt32(8);
                                prompt.IsSet = rdr.GetBoolean(0);
                                prompt.GroupLocked = rdr.GetBoolean(1);
                                prompt.Selected = rdr.GetBoolean(2);
                                if (rdr.GetInt32(3) == 1)
                                {
                                    prompt.Valid = 1;
                                }
                                else if (rdr.GetInt32(3) == 0)
                                {
                                    prompt.Valid = 0;
                                }
                                else
                                {
                                    prompt.Valid = -1;
                                }

                                try
                                {
                                    if (type.Equals("CAM"))
                                    {
                                        prompt.CurrentValue = Encoding.UTF8.GetString(Convert.FromBase64String(rdr.GetString(4)));
                                    }
                                    else
                                    {
                                        prompt.CurrentValue = rdr.GetString(4);
                                    }
                                }
                                catch (SqlNullValueException) {/*This is OK, it just means the value is not set yet*/ }
                                traveler.ListOfUserPrompts.Add(prompt);
                            }
                        }
                    }

                    //load logic
                    traveler.PromptConstraints.Clear();
                    cmdr.CommandText = $"SELECT targetprompt,operator,comparedprompt,offset,promptindex,comparedvalue FROM promptlogic WHERE customer='{traveler.CustomerName}' AND template='{traveler.TemplateName}'";
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        // we use the nullvalue exception catch again here, since some logic commands dont contain all 4 parameters (2 length logic commands have a null offset value)
                        while (rdr.Read())
                        {
                            PromptLogic logic = new PromptLogic();
                            int length = 0;

                            int index = rdr.GetInt32(4);

                            try
                            {
                                logic.TargetPrompt = rdr.GetString(0);
                                length++;
                                logic.Operator_ = rdr.GetString(1);
                                length++;
                                logic.ComparedPrompt = rdr.GetString(2);
                                if (!logic.ComparedPrompt.Equals(string.Empty))
                                {
                                    length++;
                                }
                                logic.Offset = rdr.GetString(3);
                                if (!logic.Offset.Equals(string.Empty))
                                {
                                    length++;
                                }
                                logic.ComparedValue = rdr.GetString(5);
                            }
                            catch (SqlNullValueException)
                            {
                                /*when this is thrown is means we have reached the end of the logic command (can be variable lengths so this is OK)*/
                            }
                            logic.Length = length;

                            if (traveler.PromptConstraints.ContainsKey(index))    // if the prompt indexed already has logic, just append to the dictionary, if not add a new dictionary entry
                            {
                                traveler.PromptConstraints[index].Add(logic);
                            }
                            else
                            {
                                traveler.PromptConstraints.Add(index, new List<PromptLogic> { logic });
                            }
                            traveler.LogicCount++;
                        }
                    }
                    return traveler;
                }
            }
        }
        #endregion
        #region Misc Database Functions
        /// <summary>
        /// checks if the given traveler is busy
        /// </summary>
        public static bool CheckTravelerBusyDB(string sn, string customer, string template)
        {
            //using win32 os serial number, unique to each install of windows
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_OperatingSystem");
            ManagementObject searchObject = searcher.Get().Cast<ManagementObject>().First();
            string serialNumber = searchObject["SerialNumber"].ToString();

            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT isBusy,busyDate,busyMachine FROM existingtravelers WHERE travelerSN='{sn}' AND customer='{customer}' AND template='{template}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    if (!rdr.Read())
                    {
                        Exception exception = new Exception("ERROR : Existing traveler busy info not found");
                        throw exception;
                    }
                    else
                    {
                        if (!rdr.GetBoolean(0) || (rdr.GetString(2) == serialNumber) || (rdr.GetDateTime(1) < DateTime.Now))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// get the minimum authority level of the given traveler
        /// </summary>
        public static int GetMaxAuthority (string customer, string template)
        {
            int minAuth = 0;
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                // loop through all the logged in users
                Traveler.LoggedUsers
                    .Select(e => e.ID)
                    .ToList()
                    .ForEach(id =>
                    {
                        using (MySqlCommand cmdr = new MySqlCommand($"SELECT authority FROM userauthority WHERE template='{template}' AND customer='{customer}' AND userid='{id}'", Connection))
                        {
                            object rdr = cmdr.ExecuteScalar();
                            if (rdr == null)
                            {
                                //indicates the authority does not exist for this user yet, so we check default authoeity instead
                                minAuth = -1;
                                //note: return exists lambda function not the getmaxauthority function
                                return;
                            }
                            int auth = int.Parse(rdr.ToString());
                            if (auth == 3)
                            {
                                //quitting foreach loop early, 3 is maximum authority
                                minAuth = 3;
                                return;
                            }
                            // update maximum value
                            if (auth > minAuth)
                            {
                                minAuth = auth;
                            }
                        }
                    });
                if (minAuth == -1)
                {
                    // indicates that template authority isnt found, so we check default authority instead
                    Traveler.LoggedUsers
                    .Select(e => e.ID)
                    .ToList()
                    .ForEach(id =>
                    {
                        using (MySqlCommand cmdr = new MySqlCommand($"SELECT authority FROM users WHERE userid='{id}'", Connection))
                        {
                            Object rdr = cmdr.ExecuteScalar();
                            if (rdr == null)
                            {
                                minAuth = -1;
                                return;
                            }
                            int auth = int.Parse(rdr.ToString());
                            if (auth == 3)
                            {
                                //quitting foreach loop early, 3 is maximum authority
                                minAuth = 3;
                                return;
                            }
                            if (auth > minAuth)
                            {
                                minAuth = auth;
                            }
                        }
                    });
                }
                return minAuth;
            }
        }
        /// <summary>
        /// gets a list of traveler SNs that are available to the current users
        /// </summary>
        public static List<int> GetTravelersDB(string customer, string template)
        {
            if (GetMaxAuthority(customer,template) > 0)
            {
                // auth levels of 1+ required to open templates
                using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
                {
                    Connection.Open();
                    List<int> returnlist = new List<int>();
                    if (User.GetAuthority() > 0)
                    {
                        using (MySqlCommand cmdr = new MySqlCommand($"SELECT travelerSN FROM existingtravelers WHERE customer='{customer}' AND template='{template}'", Connection))
                        using (MySqlDataReader rdr = cmdr.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                int sn = rdr.GetInt32(0);
                                if (!returnlist.Contains(sn))
                                {
                                    returnlist.Add(sn);
                                }
                            }
                        }
                    }
                    return returnlist;
                }
            }
            else
            {
                return new List<int>();
            }
        }       
        /// <summary>
        /// Returns the first SN number that the users have access to that is not busy
        /// </summary>
        public static int GetNextTraveler(string customer, string template)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                List<int> UsersTravelers = new List<int>();
                Connection.Open();
                foreach (User user in Traveler.LoggedUsers)
                {
                    // this SQL block gets all the possible SN numbers from the current logged in users
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT travelerSN FROM activeusers WHERE customer='{customer}' AND template='{template}' AND  userid='{user.ID}'", Connection))
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        while(rdr.Read())
                        {
                            int sn = rdr.GetInt32(0);
                            if (!UsersTravelers.Contains(sn))
                            {
                                UsersTravelers.Add(sn);
                            }
                        }
                    }
                }

                // then, we search all the SN numbers found above and return the first one that is not busy
                foreach(int SN in UsersTravelers)
                {
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT isbusy FROM existingtravelers WHERE customer='{customer}' AND template='{template}' AND travelerSN='{SN}'", Connection))
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            if (!rdr.GetBoolean(0)) return SN;
                        }
                        else
                        {
                            Exception exception = new Exception("ERROR : TRAVELER SN USER MISMATCH");
                            throw exception;
                        }
                    }
                }
                return -1;
            }
        }
        /// <summary>
        /// gets number of traveler on the current traveler, logged in or not
        /// </summary>
        public static int GetNumberOfUsers(string customer ,string template,string sn)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT COUNT(*) FROM activeusers WHERE customer='{customer}' AND template='{template}' AND travelerSN='{sn}'", Connection))
                {
                    return Convert.ToInt32(cmdr.ExecuteScalar());
                }
            }
        }        
        /// <summary>
        /// Returns number of logged in users that are signed in to the current traveler
        /// </summary>
        public static int GetNumberOfUsersLeft(string customer ,string template, string sn)
        {
            int numberOfUsersLeft = 0;
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                foreach(User user in Traveler.LoggedUsers)
                {
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT EXISTS (SELECT * FROM activeusers WHERE customer='{customer}' AND template='{template}' AND travelerSn='{sn}' AND username='{user.Username}')", Connection))
                    {
                        numberOfUsersLeft += Convert.ToInt32(cmdr.ExecuteScalar());
                    }
                }
            }
            return numberOfUsersLeft;
        }
        /// <summary>
        /// gets the user from the database based on the given username
        /// </summary>
        public static User GetUserDB(string username)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT passwordHash,salt,username,email,authority,userID FROM Users WHERE username='{username}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    return new User(rdr.GetString(2), rdr.GetInt32(5), rdr.GetString(3), Convert.FromBase64String(rdr.GetString(0)), Encoding.UTF8.GetBytes(rdr.GetString(1)), rdr.GetInt32(4));
                }
            }
        }
        /// <summary>
        /// Gets all users in databse
        /// </summary>
        public static List<User> GetAllUsers(string customer, string template)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                List<User> returnList = new List<User>();
                List<int> userIDs = new List<int>();
                int auth;
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT userid FROM Users", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        userIDs.Add(rdr.GetInt32(0));
                    }
                }
                userIDs.ForEach(id =>
                {
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT authority FROM userauthority WHERE userid='{id}' AND template='{template}' AND customer='{customer}'", Connection))
                    {
                        object authobj = cmdr.ExecuteScalar();
                        if (authobj == null)
                        {
                            auth = -1;
                        }
                        else
                        {
                            auth = Convert.ToInt32(authobj);
                        }
                    }
                    using (MySqlCommand cmdr = new MySqlCommand($"SELECT passwordHash,salt,username,email,authority FROM Users WHERE userid='{id}'", Connection))
                    using (MySqlDataReader rdr = cmdr.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            returnList.Add(new User(rdr.GetString(2), id, rdr.GetString(3), Convert.FromBase64String(rdr.GetString(0)), Encoding.UTF8.GetBytes(rdr.GetString(1)), rdr.GetInt32(4),auth));
                        }
                    }
                });
                return returnList;
            }
        }        
        /// <summary>
        /// Updates User in Database
        /// </summary>
        public static void UpdateUser(User user)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"UPDATE users SET passwordHash='{Convert.ToBase64String(user.PasswordHash)}',salt='{Encoding.UTF8.GetString(user.Salt)}',username='{user.Username}',email='{user.Email}',authority='{user.DefaultAuthority}' " +
                    $"WHERE userid='{user.ID}'; UPDATE userauthority SET authority='{user.TemplateAuthority}' WHERE userid='{user.ID}' AND template='{Traveler.TravelerObject.TemplateName}'" +
                    $"AND customer='{Traveler.TravelerObject.CustomerName}'", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }        
        /// <summary>
        /// Deletes user from users table
        /// </summary>
        public static void DeleteUser (User user)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"DELETE from users WHERE userid='{user.ID}'", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// Creates user in database
        /// </summary>
        public static void CreateUser(User user)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"INSERT into users (passwordHash, salt, username, email, authority, userID) " +
                    $"VALUES ('{Convert.ToBase64String(user.PasswordHash)}','{Encoding.UTF8.GetString(user.Salt)}','{user.Username}'," +
                    $"'{user.Email}','{user.DefaultAuthority}','{user.ID}')", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// returns the next id to use for users
        /// </summary>
        public static int NextUserID()
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT MAX(userID) from users", Connection))
                {
                    return Convert.ToInt32(cmdr.ExecuteScalar()) + 1;
                }
            }
        }
        /// <summary>
        /// Updates User password in Database
        /// </summary>
        public static void UpdatePassword(User user, string password)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmd = new MySqlCommand($"UPDATE users SET passwordHash='{Convert.ToBase64String(User.Hash(password,user.Salt))}' WHERE userid='{user.ID}'", Connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// tries to login using the given username and password string
        /// </summary>
        public static int LoginDB(string username, string password)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT passwordHash,salt,username,email,authority,userID FROM Users WHERE username='{username}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    if (!rdr.Read())
                    {
                        Exception exception = new Exception("ERROR User not found, already checked if this exists");
                        throw exception;  // we already checked if user exists
                    }
                    else
                    {
                        byte[] salt = Encoding.UTF8.GetBytes(rdr.GetString(1));
                        byte[] hash = Convert.FromBase64String(rdr.GetString(0));
                        if (User.ConfirmPassword(password, salt, hash))
                        {
                            Traveler.LoggedUsers.Add(new User(rdr.GetString(2), rdr.GetInt32(5), rdr.GetString(3), hash, salt, rdr.GetInt32(4)));
                            return 1;
                        }
                        else
                        {
                            // Log that password is not valid
                            return -2;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// deletes user from the current travelers active users
        /// </summary>
        public static void LogoutDB(string username)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT userID FROM users WHERE username='{username}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        int userid = rdr.GetInt32(0);
                        rdr.Close();
                        cmdr.CommandText = $"DELETE FROM activeusers WHERE userID='{userid}' AND travelerSN='{Traveler.TravelerObject.TravelerSN}' AND customer='{Traveler.TravelerObject.CustomerName}' AND template='{Traveler.TravelerObject.TemplateName}'";
                        cmdr.ExecuteNonQuery();
                    }
                }
            }

        }
        /// <summary>
        /// checks if the given username exists in users
        /// </summary>
        public static int CheckUserExistsDB(string username)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT userID FROM Users WHERE username='{username}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {

                    if (!rdr.Read())
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
        }
        /// <summary>
        /// Checks if the database connection is valid
        /// </summary>
        public static bool PingDB()
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                try
                {
                    Connection.Open();
                    //disabling the usedatabase boolean so that we dont check this again
                    //program hangs for a few seconds before timeout
                    UseDatabase = true;
                    return true;
                }
                catch
                {
                    //enabling the usedatabase boolean if we reconnect to database
                    //program hangs for a few seconds before timeout
                    UseDatabase = false;
                    return false;
                }
            }
        }
        /// <summary>
        /// returns a dicionary of completed travelers keyed by SN
        /// </summary>
        public static Dictionary<int, string> GetCompletedTravelers()
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT travelersn, html FROM completedtravelers WHERE customer='{Traveler.TravelerObject.CustomerName}' AND template='{Traveler.TravelerObject.TemplateName}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    //ditionary <travelersn, htmlstring>. htmlstring to be displayed on the webview
                    Dictionary<int, string> returnlist = new Dictionary<int, string>();
                    while (rdr.Read())
                    {
                        // skipping duplicates (should never happen)
                        if (returnlist.Keys.Contains(rdr.GetInt32(0)))
                        {
                            continue;
                        }
                        // add to list
                        returnlist.Add(rdr.GetInt32(0), Encoding.UTF8.GetString(Convert.FromBase64String(rdr.GetString(1))));
                    }
                    return returnlist;
                }
            }
        }
        /// <summary>
        /// remove user from the active users and log out the user (unUsed function)
        /// </summary>
        public static int RemoveUser(string username, string password)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT passwordHash,salt,username,email,authority,userID FROM Users WHERE username='{username}'", Connection))
                using (MySqlDataReader rdr = cmdr.ExecuteReader())
                {
                    if (!rdr.Read())
                    {
                        Exception exception = new Exception("ERROR User not found, already checked if this exists");
                        throw exception;  // we already checked if user exists
                    }
                    else
                    {
                        if (rdr.GetInt32(4) > 1)
                        {
                            return -3;
                        }
                        byte[] salt = Encoding.UTF8.GetBytes(rdr.GetString(1));
                        byte[] hash = Convert.FromBase64String(rdr.GetString(0));
                        if (User.ConfirmPassword(password, salt, hash))
                        {
                            Traveler.TravelerObject.ActiveUsers.Remove(Traveler.TravelerObject.ActiveUsers.First(u => u.Username == username));
                            LogoutDB(username);
                            return 1;
                        }
                        else
                        {
                            // Log that password is not valid
                            return -2;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// confirms is the password is correct for the given user
        /// </summary>
        public static bool ConfirmPasswordDB(string password, User user)
        {
            using (MySqlConnection Connection = new MySqlConnection(ConnectionString))
            {
                Connection.Open();
                using (MySqlCommand cmdr = new MySqlCommand($"SELECT EXISTS (SELECT * from users WHERE userid='{user.ID}' AND passwordHash='{Convert.ToBase64String(User.Hash(password,user.Salt))}')", Connection))
                {
                    // 1 if password is correct, zero otherwise
                    return Convert.ToBoolean(cmdr.ExecuteScalar());
                }
            }
        }
        #endregion
    }
}
