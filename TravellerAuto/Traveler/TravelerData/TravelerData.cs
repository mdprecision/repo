﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Serialization;
using TravellerAuto.Managers;
using TravellerAuto.PromptClasses;
using Excel = Microsoft.Office.Interop.Excel;

namespace TravellerAuto
{
    [Serializable]
    [XmlInclude(typeof(AssemblyDetails))]
    [XmlInclude(typeof(UserPrompt))]
    [XmlInclude(typeof(TextBoxPrompt))]
    [XmlInclude(typeof(DualCheckBoxPrompt))]
    [XmlInclude(typeof(DateTimePrompt))]
    [XmlInclude(typeof(CamRequestPrompt))]
    public class TravelerData
    {
        #region Attributes

        [XmlAttribute]
        public string TravelerSN { get; set; } = String.Empty;
        [XmlAttribute]
        public string TemplateName { get; set; } = String.Empty;
        [XmlAttribute]
        public string CustomerName { get; set; } = String.Empty;
        [XmlAttribute]
        public int ScrollPosition { get; set; } = 0;
        [XmlAttribute]
        public int PercentProgress { get; set; } = 0;
        [XmlAttribute]
        public bool TravellerIsLocked { get; set; } = false;
        [XmlAttribute]
        public bool IsComplete { get; set; } = false;

        [XmlArrayItem]
        public List<User> ActiveUsers { get; private set; } = new List<User>();
        [XmlArrayItem]
        public List<AssemblyDetails> AssemblyDetailsList { get; set; } = new List<AssemblyDetails>();
        [XmlArrayItem]
        public List<GroupData> GroupDataList { get; set; } = new List<GroupData>();
        [XmlArrayItem]
        public List<UserPrompt> ListOfUserPrompts { get; set; } = new List<UserPrompt>();
        [XmlIgnore]
        public int LogicCount { get; set; } = 0;
        [XmlIgnore]
        public IDictionary<int, List<int>> PromptGroups { get; set; } = new Dictionary<int, List<int>>();
        [XmlIgnore]
        public IDictionary<int, List<PromptLogic>> PromptConstraints { get; set; } = new Dictionary<int, List<PromptLogic>>();
        [XmlAttribute]
        public int SelectedPromptIndex { get; set; } = -1;
        [XmlAttribute]
        public int CurrentGroupIndex { get; set; } = -1;
        [XmlAttribute]
        public int NumberOfAssemblies { get; set; } = 0;
        [XmlAttribute]
        public int NumberOfGroups { get; set; } = 0;
        [XmlElement]
        public string TravelerNotes { get; set; } = String.Empty;

        #endregion
        #region TravelerData Loading Functions
        /// <summary>
        /// Overwrites current traveller data to the new traveler being loaded provided by the input xmlfilepath
        /// </summary>
        public static TravelerData LoadTravellerData(string xmlFilePath)
        {
            using (FileStream stream = new FileStream(xmlFilePath, FileMode.Open))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(TravelerData));
                return (TravelerData)xmlSerializer.Deserialize(stream);
            }
        }
        /// <summary>
        /// serializes current traveler to XMl
        /// </summary>
        public void SaveTravelerData()
        {
            using (FileStream stream = new FileStream(PathMgr.XmlPath, FileMode.Create))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(TravelerData));
                xmlSerializer.Serialize(stream, this);
            }
        }
        /// <summary>
        /// creates prompt objects based on the input list of prompts, populates the list and saves to XML.
        /// </summary>
        public void CreateUserPrompts(List<Tuple<string, string, int>> listOfPrompts)
        {
            UserPrompt.CurrentIndex = 0;

            listOfPrompts.ForEach(e => ListOfUserPrompts.Add(UserPrompt.CreatePrompt(e.Item1, e.Item2, e.Item3)));
            LoadPromptGroups();
            GroupDataList = GroupData.CreateGroupData(Traveler.TravelerObject.NumberOfGroups);
            SaveTravelerData();
        }
        /// <summary>
        /// Loads prompt gropu dictionary from list of user prompts
        /// </summary>
        public void LoadPromptGroups()
        {
            PromptGroups.Clear();
            Traveler.TravelerObject.NumberOfGroups = 0;

            // loops through prompts and populated the dictionary of prompt groups
            foreach (UserPrompt prompt in ListOfUserPrompts)
            {
                if (PromptGroups.ContainsKey(prompt.PromptGroup))
                {
                    PromptGroups[prompt.PromptGroup].Add(prompt.Index);
                }
                else
                {
                    PromptGroups.Add(prompt.PromptGroup, new List<int> { prompt.Index });
                    Traveler.TravelerObject.NumberOfGroups++;
                }
            }
        }
        /// <summary>
        /// loads the prompt logic from the template excel file
        /// </summary>
        public void LoadLogic()
        {
            PromptConstraints.Clear();

            int promptCount = Traveler.TravelerObject.ListOfUserPrompts.Count;
            if (!File.Exists(PathMgr.LogicPath))
            {
                //if there is no template, we create the template and format it correctly
                Excel.Application app_ = new Excel.Application();
                if (app_ == null)
                {
                    MessageBox.Show("Error, Excel not installed");
                    return;
                }

                Excel.Workbook workbook_ = app_.Workbooks.Add(Type.Missing);
                Excel.Worksheet worksheet_ = (Excel.Worksheet)workbook_.ActiveSheet;
                worksheet_.Name = TemplateName + " Logic";


                worksheet_.Cells[1, 1] = "Group";
                ((Excel.Range)worksheet_.Columns[1]).EntireColumn.ColumnWidth = 10;
                worksheet_.Cells[1, 2] = "Type";
                ((Excel.Range)worksheet_.Columns[2]).EntireColumn.ColumnWidth = 10;
                worksheet_.Cells[1, 3] = "Target Prompt";
                ((Excel.Range)worksheet_.Columns[3]).EntireColumn.ColumnWidth = 15;

                for (int i = 0; i < 15; i++)
                {
                    worksheet_.Cells[1, i + 4] = $"Logic {i + 1}";
                    ((Excel.Range)worksheet_.Columns[i + 4]).EntireColumn.ColumnWidth = 20;
                }

                for (int i = 0; i < promptCount; i++)
                {
                    worksheet_.Cells[i + 2, 1] = Traveler.TravelerObject.ListOfUserPrompts[i].PromptGroup + 1;
                    worksheet_.Cells[i + 2, 3] = Traveler.TravelerObject.ListOfUserPrompts[i].PromptName;
                    if (Traveler.TravelerObject.ListOfUserPrompts[i].PromptType == "DateTime")
                    {
                        DateTimePrompt dt = (DateTimePrompt)Traveler.TravelerObject.ListOfUserPrompts[i];
                        worksheet_.Cells[i + 2, 2] = dt.DT_Type;
                    }
                    else
                    {
                        worksheet_.Cells[i + 2, 2] = Traveler.TravelerObject.ListOfUserPrompts[i].PromptType;
                    }
                }

                workbook_.SaveAs(PathMgr.LogicPath);
                workbook_.Close();
                app_.Quit();
            }
            //reading excel file
            Excel.Application app = new Excel.Application();
            if (app == null)
            {
                MessageBox.Show("Error, Excel not installed");
                return;
            }

            Excel.Workbooks workbook = app.Workbooks;
            Excel.Workbook worksheet = workbook.Open(PathMgr.LogicPath);
            Excel.Worksheet sheet = (Excel.Worksheet)worksheet.Sheets[1];


            List<string> promptLogicList = new List<string>();
            for (int i = 0; i < promptCount; ++i)
            {
                int j = 0;
                while (((Excel.Range)sheet.Cells[i + 2, j + 4]).Text.ToString() != string.Empty)
                {
                    string cleanedString = ((Excel.Range)sheet.Cells[i + 2, j + 4]).Text.ToString().Trim().ToLower();
                    cleanedString = Regex.Replace(cleanedString, @"\s+", " ");
                    promptLogicList.Add(((Excel.Range)sheet.Cells[i + 2, 3]).Text.ToString().ToLower() + " " + cleanedString);
                    j++;
                }
            }

            worksheet.Close(false, PathMgr.LogicPath, null);
            workbook.Close();
            app.Quit();

            //parsing each prompt in the list of prompts
            foreach (string constraint in promptLogicList)
            {
                try
                {
                    string[] constraintContents = constraint.ToLower().Split(' ');
                    if (constraintContents.Length == 2) // 2 length constraints
                    {
                        int promptIndex1 = Traveler.TravelerObject.ListOfUserPrompts.Where(e => e.PromptName.ToLower().Equals(constraintContents[0])).ToList()[0].Index;    // gets the index of the prompt mentioned in the logic statement
                        if (PromptConstraints.ContainsKey(promptIndex1))    // if the prompt indexed already has logic, just append to the dictionary, if not add a new dictionary entry
                        {
                            PromptConstraints[promptIndex1].Add(new PromptLogic(constraintContents[0], constraintContents[1]));
                        }
                        else
                        {
                            PromptConstraints.Add(promptIndex1, new List<PromptLogic> { new PromptLogic(constraintContents[0], constraintContents[1]) });
                        }
                        LogicCount++;
                    }
                    else if (constraintContents.Length == 3)    // 3 length constraints
                    {
                        int promptIndex1 = Traveler.TravelerObject.ListOfUserPrompts.Where(e => e.PromptName.ToLower().Equals(constraintContents[0])).ToList()[0].Index;
                        if (PromptConstraints.ContainsKey(promptIndex1))    // if the prompt indexed already has logic, just append to the dictionary, if not add a new dictionary entry
                        {
                            PromptConstraints[promptIndex1].Add(new PromptLogic(constraintContents[0], constraintContents[1], constraintContents[2]));
                        }
                        else
                        {
                            PromptConstraints.Add(promptIndex1, new List<PromptLogic> { new PromptLogic(constraintContents[0], constraintContents[1], constraintContents[2]) });
                        }
                        LogicCount++;
                    }
                    else if (constraintContents.Length == 4)    // 4 length constraints
                    {
                        int promptIndex1 = Traveler.TravelerObject.ListOfUserPrompts.Where(e => e.PromptName.ToLower().Equals(constraintContents[0])).ToList()[0].Index;

                        if (PromptConstraints.ContainsKey(promptIndex1))    // if the prompt indexed already has logic, just append to the dictionary, if not add a new dictionary entry
                        {
                            PromptConstraints[promptIndex1].Add(new PromptLogic(constraintContents[0], constraintContents[1], constraintContents[2], constraintContents[3]));
                        }
                        else
                        {
                            PromptConstraints.Add(promptIndex1, new List<PromptLogic> { new PromptLogic(constraintContents[0], constraintContents[1], constraintContents[2], constraintContents[3]) });
                        }
                        LogicCount++;
                    }
                    else
                    {
                        Exception exception = new Exception("Logic length > 4");
                        throw exception;
                    }
                }
                catch (Exception e)
                {
                    // indicates invalid prompt logic input, not allowing continuation of traveler
                    if (Application.OpenForms["MainForm"] is MainForm mainform)
                    {
                        mainform.LockTraveller(true);
                        mainform.ErrorMessage($"Invalid Prompt Logic '{constraint}' | {e.Message}");
                    }
                    Traveler.ValidLogic = false;
                    return;
                }
                //prompt logic good
                Traveler.ValidLogic = true;
                if (Application.OpenForms["MainForm"] is MainForm mainform1)
                {
                    mainform1.LockTraveller(false);
                }
            }
        }
        #endregion
        #region Navigating Prompts Functions
        /// <summary>
        /// sets value of prompt, selects next promp and saves to XMl
        /// </summary>
        public bool SetValueForSelectedPrompt(string val)
        {
            ListOfUserPrompts[SelectedPromptIndex].SetValue(val);
            SaveTravelerData();
            return true;
        }
        private bool IsGroupComplete()
        {
            //special case when starting traveler
            if (CurrentGroupIndex == -1)
            {
                return true;
            }
            else
            {
                return !PromptGroups[CurrentGroupIndex].Any(e => ListOfUserPrompts[e].CurrentValue == string.Empty);
            }
        }
        /// <summary>
        /// Checks if the current prompt group has invalid promtps or not, and reloads each prompt in the groups.
        /// This is checked when a traveler is resume to ensure that no previous inputs within the group are now invalid
        /// </summary>
        public void CheckGroupValid()
        {
            List<int> promptsToUpdate = new List<int>();
            PromptGroups[CurrentGroupIndex]
                .Where(e => ListOfUserPrompts[e].CurrentValue != string.Empty)
                .ToList()
                .ForEach(e =>
                {
                    ListOfUserPrompts[e].Valid = ListOfUserPrompts[e].CheckValidity();
                    promptsToUpdate.Add(e);
                });
            HtmlMgr.ReloadPrompts(promptsToUpdate);
        }
        private bool LoadNextGroup()
        {
            bool loaded = false;
            if (CurrentGroupIndex == -1)
            {
                //special case for first group
                CurrentGroupIndex += 1;
                //unlocking next group
                PromptGroups[0]
                    .ForEach(e => ListOfUserPrompts[e].GroupLocked = false);
                loaded = true;
            }
            else if (IsGroupComplete())
            {
                //lock current group prompts
                PromptGroups[CurrentGroupIndex]
                    .Where(e => e != SelectedPromptIndex)
                    .ToList()
                    .ForEach(e => ListOfUserPrompts[e].GroupLocked = true);
                if (CurrentGroupIndex == (PromptGroups.Count - 1))
                {
                    // Finished Doc
                    loaded = false;
                }
                else
                {
                    //increment grooup, and unlock next group
                    CurrentGroupIndex++;
                    PromptGroups[CurrentGroupIndex]
                        .Where(e => e != SelectedPromptIndex)
                        .ToList()
                        .ForEach(e => ListOfUserPrompts[e].GroupLocked = false);
                    loaded = true;
                }
            }
            return loaded;
        }
        /// <summary>
        /// returns the list of prompts that current have a value that has been set by the user
        /// </summary>
        public List<UserPrompt> GetUserPromptsWithValues()
        {
            return ListOfUserPrompts.Where(p => p.CurrentValue != string.Empty).ToList();
        }
        /// <summary>
        /// selectes next prompt, checks if the group is complete, if it is valid or invalid and triggers the traveller lock if invalid.
        /// </summary>
        public int SelectNextPrompt2(bool sentBySubmitButton = false)
        {
            int returnStatus = 0; // indicates wrap around (reached end of prompts lists and needed to go back to the first one)
            if (CurrentGroupIndex == -1)    // when starting a new traveler
            {
                LoadNextGroup();
                SelectedPromptIndex = PromptGroups[0][0];
                ListOfUserPrompts[0].Selected = true;
                return -1;  // return code to reload only the first prompt group
            }
            int numberInvalid = FindNumberOfInvalidPrompts();
            if (IsGroupComplete() && sentBySubmitButton)    // if the group is done
            {
                if (Traveler.LoggedUsers.Where(e => e.DefaultAuthority > 0).ToList().Count == 0)
                {
                    MessageBox.Show("No Active Users with authority to complete group");
                    if (Application.OpenForms["MainForm"] is MainForm mainform)
                    {
                        mainform.LoadPreviousFieldValue(ListOfUserPrompts[SelectedPromptIndex]);
                    }
                    Traveler.SkipClear = true;
                    return -2;  //return code to not reload any prompts
                }
                if (!TravelerDatabase.UseDatabase || !TravelerDatabase.PingDB())
                {
                    MessageBox.Show("No Internet Connection to Complete Group");
                    if (Application.OpenForms["MainForm"] is MainForm mainform)
                    {
                        mainform.LoadPreviousFieldValue(ListOfUserPrompts[SelectedPromptIndex]);
                    }
                    Traveler.SkipClear = true;
                    return -2;  //return code to not reload any prompts
                }
                if (numberInvalid > 0)
                {
                    DialogResult warningResult = MessageBox.Show("Warning : Invalid Data submitted in Current Group, Would you like to Continue? Traveller Will be locked if you continue", "Error",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (warningResult == DialogResult.No)
                    {
                        if (Application.OpenForms["MainForm"] is MainForm mainform)
                        {
                            mainform.LoadPreviousFieldValue(ListOfUserPrompts[SelectedPromptIndex]);
                        }

                        return -2;  // dont reload any prompts
                    }
                    else
                    {
                        if (Application.OpenForms["MainForm"] is MainForm mainform)
                        {
                            mainform.LockTraveller(true);
                        }
                        //set group data values
                        Traveler.TravelerObject.GroupDataList[CurrentGroupIndex].OperatorInitials = string.Join("\n", User.GetActiveUsers(Traveler.LoggedUsers));
                        Traveler.TravelerObject.GroupDataList[CurrentGroupIndex].PassFail = "FAIL";
                        Traveler.TravelerObject.GroupDataList[CurrentGroupIndex].DateComplete = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

                        //lock prompts
                        PromptGroups[CurrentGroupIndex].ForEach(e => ListOfUserPrompts[e].GroupLocked = true);

                        //save traveler
                        TravelerDatabase.SaveTravelerDB(this);
                        return 4;
                    }
                }
                else if (TravellerIsLocked)
                {
                    if (Application.OpenForms["MainForm"] is MainForm mainform)
                    {
                        mainform.LockTraveller(false);
                    }
                }
                else
                {
                    ListOfUserPrompts[SelectedPromptIndex].Selected = false;    // unselect current prompt
                    ListOfUserPrompts[SelectedPromptIndex].GroupLocked = true;  // lock the group

                    //set group data values
                    GroupDataList[CurrentGroupIndex].OperatorInitials = string.Join("\n", User.GetActiveUsers(Traveler.LoggedUsers));
                    if (numberInvalid != 0)
                    {
                        GroupDataList[CurrentGroupIndex].PassFail = "FAIL";
                    }
                    else
                    {
                        GroupDataList[CurrentGroupIndex].PassFail = "PASS";
                    }
                    GroupDataList[CurrentGroupIndex].DateComplete = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

                    //save to database before loading next group
                    TravelerDatabase.SaveTravelerDB(this);

                    if (LoadNextGroup())    // if we successfully load the next group (form is still incomplete)
                    {
                        SelectedPromptIndex = PromptGroups[CurrentGroupIndex][0];
                        ListOfUserPrompts[SelectedPromptIndex].Selected = true;
                        returnStatus = 2;     // status indicating that we completed a group handled for HTML refreshing
                    }
                    else
                    {
                        //check if assembly details are filled out
                        if (AssemblyDetailsList.Any(e => e.AssemblyNotes.Equals(string.Empty) || e.SN.Equals(string.Empty)))
                        {
                            MessageBox.Show("Cannot Complete Traveler. Assembly Details not finished");
                            return -2;
                        }
                        // check if notes are filled out
                        if (TravelerNotes != "-1" && TravelerNotes == string.Empty)
                        {
                            MessageBox.Show("Cannot Complete Traveler. Traveller Notes not finished");
                            return -2;
                        }
                        // check if we can connect to database
                        if (!TravelerDatabase.UseDatabase || !TravelerDatabase.PingDB())
                        {
                            MessageBox.Show("No Internet connection to complete traveler");
                            return -2;
                        }
                        // prompt to finish traveler
                        DialogResult res = MessageBox.Show("All Data filled out.Would you like to Complete the Traveler?", "Traveler Complete", MessageBoxButtons.YesNo);
                        if (res == DialogResult.Yes)
                        {
                            //lock last group
                            PromptGroups[CurrentGroupIndex].ForEach(e => ListOfUserPrompts[e].GroupLocked = true);
                            // lock traveler
                            if (Application.OpenForms["MainForm"] is MainForm mainform)
                            {
                                mainform.LockTraveller(true);
                                // set complete
                                mainform.ErrorMessage("Traveler Complete", true);
                            }
                            IsComplete = true;
                            TravellerIsLocked = true;
                            TravelerDatabase.SaveTravelerDB(this);
                            return 3;   // traveler complete
                        }
                        else
                        {
                            //aborting completeing traveler
                            if (Application.OpenForms["MainForm"] is MainForm mainform)
                            {
                                mainform.LoadPreviousFieldValue(ListOfUserPrompts[SelectedPromptIndex]);
                            }

                            return -2;
                        }
                        //form complete
                    }
                }
            }
            else
            {
                // Group not complete, Move onto next prompt
                ListOfUserPrompts[SelectedPromptIndex].Selected = false;

                // this makes us to goto the next prompt if navigating with arrows, OR the next EMPTY prompt if navigating with the submit button
                do
                {
                    int indexInGroup = PromptGroups[CurrentGroupIndex].IndexOf(SelectedPromptIndex);
                    if (indexInGroup == PromptGroups[CurrentGroupIndex].Count - 1)  // wrap around
                    {
                        SelectedPromptIndex = PromptGroups[CurrentGroupIndex][0];
                        returnStatus = 1;
                    }
                    else
                    {
                        SelectedPromptIndex++;
                    }
                } while (ListOfUserPrompts[SelectedPromptIndex].CurrentValue != string.Empty && sentBySubmitButton);

                ListOfUserPrompts[SelectedPromptIndex].Selected = true;
            }
            return returnStatus;
        }
        /// <summary>
        /// selects the previous prompt, similar to selecting next prompt
        /// </summary>
        public int SelectPreviousPrompt2()
        {
            int wrapAround = 0;   // indicates wrap around (meaning we needed to go to the end of the prompts)

            ListOfUserPrompts[SelectedPromptIndex].Selected = false;

            int indexInGroup = PromptGroups[CurrentGroupIndex].IndexOf(SelectedPromptIndex);
            if (indexInGroup == 0) { SelectedPromptIndex += PromptGroups[CurrentGroupIndex].Count - 1; wrapAround = 1; }   // wrap around backwards
            else
            {
                SelectedPromptIndex--;
            }

            ListOfUserPrompts[SelectedPromptIndex].Selected = true;
            SaveTravelerData();
            return wrapAround;
        }
        #endregion
        #region Misc Functions
        public TravelerData() { }
        /// <summary>
        /// returns number of prompts that are invalid
        /// </summary>
        public int FindNumberOfInvalidPrompts()
        {
            return ListOfUserPrompts.Count(e => e.Valid.Equals(0));
        }
        /// <summary>
        /// calls the mainform print to pdf function
        /// </summary>
        public void SaveTravelerToPDF()
        {
            string outputPath = Path.Combine(PathMgr.CompletedTravelersPath, $"{TravelerSN}.pdf");
            if (Application.OpenForms["MainForm"] is MainForm mainform)
            {
                mainform.PrintToPdf(outputPath);
            }
        }
        #endregion
    }
}
