﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TravellerAuto
{
    [Serializable]
    [XmlInclude(typeof(GroupData))]
    public class GroupData
    {
        #region Attributes
        [XmlAttribute]
        public string OperatorInitials { get; set; } = string.Empty;
        [XmlAttribute]
        public string PassFail { get; set; } = string.Empty;
        [XmlAttribute]
        public string DateComplete { get; set; } = string.Empty;
        [XmlAttribute]
        public int GroupNumber { get; set; } = -1;
        #endregion
        #region Constructors
        public GroupData() { }
        public GroupData(int index, string oi = "", string pf = "", string doc = "")
        {
            OperatorInitials = oi;
            PassFail = pf;
            DateComplete = doc;
            GroupNumber = index;
        }
        #endregion
        #region GroupDataFunctions
        /// <summary>
        /// Populates the list of assembly details based on the input count
        /// </summary>
        public static List<GroupData> CreateGroupData(int count)
        {
            List<GroupData> returnList = new List<GroupData>();
            for (int i = 0; i < count; ++i)
            {
                returnList.Add(new GroupData(i));
            }
            return returnList;    // assembly list is stored in TravellerData
        }
        #endregion
    }
}
