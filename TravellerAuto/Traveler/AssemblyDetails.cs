﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TravellerAuto.PromptClasses
{
    [Serializable]
    [XmlInclude(typeof(AssemblyDetails))]
    public class AssemblyDetails
    {
        #region Attributes
        [XmlAttribute("SN")]
        public string SN { get; set; } = string.Empty;
        [XmlAttribute("PN")]
        public string PN { get; set; } = string.Empty;
        [XmlAttribute("Description")]
        public string AssemblyDesc { get; set; } = string.Empty;

        [XmlElement("Notes")]
        public string AssemblyNotes { get; set; } = string.Empty;
        #endregion
        #region Constructors
        public AssemblyDetails() { }
        public AssemblyDetails(string sn, string notes)
        {
            SN = sn;
            AssemblyNotes = notes;
        }
        #endregion
        #region AssemblyDetail Functions
        /// <summary>
        /// Populates the list of assembly details based on the input count
        /// </summary>
        public static List<AssemblyDetails> CreateAssemblyDetails(int count)
        {
            List<AssemblyDetails> returnList = new List<AssemblyDetails>();
            for (int i = 0; i < count; ++i)
            {
                returnList.Add(new AssemblyDetails() { AssemblyNotes = "N/A" });
            }
            return returnList;    // assembly list is stored in TravellerData
        }
        #endregion
    }
}
